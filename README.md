# HacerGestoV3

Gesture Library for the Android Platform (Supports API 14+)

**About HacerGestoV3**

HacerGestoV3 is a gesture library for the Android Platform
This library comes with a comprehensive demo app complete with source code to help developers get a head start. Though the library is protected by an open source licence, the code for the demo app is free source.
You can download the Demo app directly from the Google Play Store: https://play.google.com/store/apps/details?id=com.WarwickWestonWright.HacerGestoV3Demo&hl=en_GB

You can view the demo of the library here: https://youtu.be/hCLdoOShkpw

***You can make a donation the HG Widgets for Android here: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DWT2TT7X83PE8***

This Gesture Library allows for the simultaneous use of 4 Gestures: Rotate, Pinch/Scale Move and Fling.

Each Gesture has numerous configurable behaviours.

**Developer Note: The last update could break your code**
The last update changed the way the touchpoint return the values in the callback. The origin is now the center (was upper left). This change was for consistency as all texture positions are center centric.

**Rotate**
The Rotate behaviour is based upon the HGDial libraries and has the following behaviours:

1. The ability to record the direction of rotation.
1. Allows precision rotation settings causing the dial to rotate at a different rate than the gesture (including the ability to rotate in the opposite direction of the gesture).
1. It records the number of gesture rotations.
1. It records the number of image rotations.
1. It has a cumulative dial setting. When enabled the rotation will occur relative to the touch; and disabled the rotation will start from the point where the gesture starts.
1. It has an advanced angle snap feature with an angle snap tolerance setting. The tolerance causes the dial to rotate freely until the snap tolerance is met.
1. The dial can operate in single or dual finger mode.
1. With this dial it is possible to set a minimum/maximum rotation constraint.
1. It has a variable dial behaviour causing the rotation rate to change depending on how close the gesture is the centre of the dial.
1. This gesture comes with a 'fling-to-spin' behaviour; having configurable fling tolerance, spin start/end speed and spin animation duration.
1. A key feature is that the dial controls are designed to interact with each other and any other widgets/layouts that implement touch listeners.
1. All of the above features play together in perfect harmony.

**Scale**
The Scale Gesture has the following behaviours:

1. Minimum and maximum scale constraint.
1. Scale Snapping.

**Move**
The Move gesture has the following:

1. Cumulative Move (moves image relative to touch)
1. Non Cumulative (position starts from the position of the touch)
1. Move Snapping: Can set multiple snap points with snap distance tolerance.
1. Move constraints. Set movement to constrain to left, top, right and bottom. Also sas dynamic constraints for edge and off edge of view.

**Fling**
The Fling Gesture has the following behaviours:

1. Fling off of edge or fling to edge.
1. Fling to nearest corner.
1. Fling to nearest edge.

**Pan Zoom Mode:**

Pan Zoom has the following behaviours:

1. Calling a simple method will run the library in pan zoom mode.
1. Minimum pan zoom scale setting: centre crop and best fit. This scaling also works with device rotation.
1. Scales and constrains movement perfectly.
1. Works with fling gesture.

**All gestures and behavioural flags work together in any combination and each gesture can be turned on or off**


All of the gestures can be used simultaneously and behave intuitively while doing so.

**ToDos:**

1. Get 90 degree rotation working with pan zoom behaviour.
1. Set touch zones callbacks for pan zome mode.
1. Alter rotate snap flag to work when there is no free rotation between snaps
1. Add snap flag to callback object for scale gesture.
1. Write demo app demonstrating how to use the library as a gesture detector.

**Improvements on Version 2**

1. It is now possible to use rotate, scale and move simultaneously.
1. The move snapping now works with the fling behaviour.
1. Now uses canvas; making it perfect for use as a custom control.

**Important Note:**

Slow spin on curve behaviour has been removed and replaced with a 'Slow Factor' behaviour. See Commit Note (https://bitbucket.org/warwick/hacergestov3/commits/1e6a25c10d876bdfe37e40c5483da597) for potential beaks in your code when updating an existing application that uses this library.

**Only need a rotate/dial library?**
**Try one of these.**

https://bitbucket.org/warwick/hgdialrepo or https://bitbucket.org/warwick/hg_dial_v2

**LICENSE.**
This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License

In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".

Copyright (c) 2015, Warwick Weston Wright All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1.     Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
1.     Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.