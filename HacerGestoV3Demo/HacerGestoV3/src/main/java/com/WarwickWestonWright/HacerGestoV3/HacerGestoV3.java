/*
LICENSE. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License

In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".

Copyright (c) 2015, Warwick Weston Wright All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HacerGestoV3;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

public class HacerGestoV3 extends View implements View.OnTouchListener {

	IHacerGestoV3 iHacerGestoV3;
	private Drawable drawable;
	private final AngleWrapper angleWrapper = new AngleWrapper(new Point(0, 0));
	private final HGV3Info hgv3Info = new HGV3Info();

	/*
	Developer note about separation of concerns strategy. This class only ever reads package local fields from the metrics classes (rotateMetrics, scaleMetrics, moveMetrics and flingMetrics)
	The metrics class only ever read from this class. The metrics classes read from each other.
	Addendum: This is a new library and the separation of concerns has not been fully checked so their may be some mixed concerns that need fixing.
	*/

	/* Top of block separation of concerns */
	private final HGGeometry hgGeometry = new HGGeometry();//Used internally public for convenience.
	private final RotateMetrics rotateMetrics = new RotateMetrics(this, hgv3Info, hgGeometry, angleWrapper);
	private final ScaleMetrics scaleMetrics = new ScaleMetrics(this, hgv3Info, hgGeometry);
	final MoveMetrics moveMetrics = new MoveMetrics(this, hgv3Info, hgGeometry);
	final FlingMetrics flingMetrics = new FlingMetrics(this, hgv3Info, hgGeometry, moveMetrics);
	private final Point rotationCenterPoint = new Point(0, 0);
	/* Bottom of block separation of concerns */
	private boolean viewIsSetup;
	private int paddingLeft;
	private int paddingTop;
	private int paddingRight;
	private int paddingBottom;
	int contentWidth;
	int contentHeight;
	int viewWidth;//Added for processor efficiency
	int viewHeight;//Added for processor efficiency
	int viewWidthOver2;//Added for processor efficiency
	int viewHeightOver2;//Added for processor efficiency
	private double viewWidthToHeightRatio;
	private double drawableWidthToHeightRatio;
	private float deviceDensity;
	private int leftMargin;//Open end
	private int topMargin;//Open end
	private int rightMargin;//Open end
	private int bottomMargin;//Open end
	private int scaleOffsetL;
	private int scaleOffsetT;
	float firstTouchX;
	float firstTouchY;
	float secondTouchX;
	float secondTouchY;
	private float touchOffsetX;
	private float touchOffsetY;
	private int touchPointerCount;
	private long quickTapTime;
	volatile boolean quickTap;
	volatile boolean secondFingerWasDown;
	private long gestureDownTime;
	private int useGestures;
	public final int ROTATE = 2;
	public final int SCALE = 4;
	public final int MOVE = 8;
	public final int FLING = 16;

	public interface IHacerGestoV3 {
		void onDown(HGV3Info hgv3Info);
		void onPointerDown(HGV3Info hgv3Info);
		void onMove(HGV3Info hgv3Info);
		void onPointerUp(HGV3Info hgv3Info);
		void onUp(HGV3Info hgv3Info);
	}

	//Standard View constructors
	public HacerGestoV3(Context context) {super(context);init(null, 0);}
	public HacerGestoV3(Context context, AttributeSet attrs) {super(context, attrs);init(attrs, 0); setOnTouchListener(this);}
	public HacerGestoV3(Context context, AttributeSet attrs, int defStyle) {super(context, attrs, defStyle);init(attrs, defStyle);}

	private void init(AttributeSet attrs, int defStyle) {

		setFields();
		setFocusable(true);
		setFocusableInTouchMode(true);
		final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.HacerGestoV3, defStyle, 0);

		if(a.hasValue(R.styleable.HacerGestoV3_hgV3Drawable)) {
			drawable = a.getDrawable(R.styleable.HacerGestoV3_hgV3Drawable);
			drawable.setCallback(this);
		}

		a.recycle();

	}//End private void init(AttributeSet attrs, int defStyle)


	@Override
	protected void onDraw(final Canvas canvas) {
		super.onDraw(canvas);

		//viewIsSetup is set to false for view events that cause a redraw such as orientation changes etc.
		if(viewIsSetup == true) {

			if(viewWidth < viewHeight) {
				contentWidth = (int) ((float) viewWidth * scaleMetrics.scale) - paddingLeft - paddingRight;
				contentHeight = (int) ((float) viewWidth / drawableWidthToHeightRatio * scaleMetrics.scale) - paddingTop - paddingBottom;
			}
			else {
				contentWidth = (int) ((float) viewHeight* scaleMetrics.scale) - paddingLeft - paddingRight;
				contentHeight = (int) ((float) viewHeight / drawableWidthToHeightRatio * scaleMetrics.scale) - paddingTop - paddingBottom;
			}//End if(viewWidth < viewHeight)

			scaleOffsetL = ((viewWidth - contentWidth) / 2);
			scaleOffsetT = ((viewHeight - contentHeight) / 2);
			rotateMetrics.imageRadiusX2 = contentWidth;
			rotateMetrics.imageRadius = contentWidth / 2;

			if(moveMetrics.cumulativeMove == false && (useGestures & MOVE) == MOVE) {
				rotationCenterPoint.x = (int) firstTouchX;
				rotationCenterPoint.y = (int) firstTouchY;
			}
			else {
				rotationCenterPoint.x = (contentWidth / 2) + scaleOffsetL + (int) moveMetrics.xMove;
				rotationCenterPoint.y = (contentHeight / 2) + scaleOffsetT + (int) moveMetrics.yMove;
			}

			//Rotate section
			angleWrapper.setCenterPoint(rotationCenterPoint);

			if(rotateMetrics.angleSnap == 0) {
				canvas.rotate((float) (angleWrapper.getTextureAngle() % 1 * 360), rotationCenterPoint.x, rotationCenterPoint.y);
			}
			else /* if(rotateMetrics.angleSnap != 0) */ {
				canvas.rotate((float) (rotateMetrics.angleSnapNext * 360), rotationCenterPoint.x, rotationCenterPoint.y);
			}

			//Scale section
			if(contentWidth < contentHeight) {
				drawable.setBounds(paddingLeft, paddingTop, paddingLeft + contentWidth, paddingTop + contentHeight);
			}
			else if(contentWidth >= contentHeight) {
				drawable.setBounds(paddingLeft, paddingTop, paddingLeft + contentWidth, paddingTop + contentHeight);
			}//End if(contentWidth < contentHeight)

			//Move/Fling section
			if(moveMetrics.hasSnapped == false) {

				if(moveMetrics.cumulativeMove == true) {
					canvas.translate(scaleOffsetL + moveMetrics.xMove, scaleOffsetT + moveMetrics.yMove);
				}
				else {

					if(!((useGestures & MOVE) == MOVE)) {
						canvas.translate((moveMetrics.xMove + (contentWidth / 2)), (moveMetrics.yMove + (contentHeight / 2)));
					}
					else {
						canvas.translate((moveMetrics.xMove - (contentWidth / 2)), (moveMetrics.yMove - (contentHeight / 2)));
					}

				}
			}
			else /* if(moveMetrics.hasSnapped == true) */ {

				canvas.translate(scaleOffsetL + moveMetrics.moveSnapping[moveMetrics.snapIdx].x, scaleOffsetT + moveMetrics.moveSnapping[moveMetrics.snapIdx].y);

			}//End if(moveMetrics.moveSnapping == null)

			drawable.draw(canvas);

		}
		else /* if(viewIsSetup == false) */ {

			//Condition only gets called once for every layout touch events only execute in the viewIsSetup true condition.
			deviceDensity = getResources().getDisplayMetrics().density;
			viewWidth = getWidth();
			viewHeight = getHeight();
			viewWidthToHeightRatio = (double) viewWidth / (double) viewHeight;
			drawableWidthToHeightRatio = (double) drawable.getIntrinsicWidth() / (double) drawable.getIntrinsicHeight();

			if(moveMetrics.usePanZoomMode == true) {
				setupPanZoom();
			}

			viewWidthOver2 = viewWidth / 2;
			viewHeightOver2 = viewHeight / 2;
			paddingLeft = getPaddingLeft();
			paddingTop = getPaddingTop();
			paddingRight = getPaddingRight();
			paddingBottom = getPaddingBottom();

			leftMargin = ((FrameLayout.LayoutParams) getLayoutParams()).leftMargin;
			topMargin = ((FrameLayout.LayoutParams) getLayoutParams()).topMargin;
			rightMargin = ((FrameLayout.LayoutParams) getLayoutParams()).rightMargin;
			bottomMargin = ((FrameLayout.LayoutParams) getLayoutParams()).bottomMargin;

			if(viewWidth < viewHeight) {
				contentWidth = (int) ((float) viewWidth * scaleMetrics.scale) - paddingLeft - paddingRight;
				contentHeight = (int) ((float) viewWidth / drawableWidthToHeightRatio * scaleMetrics.scale) - paddingTop - paddingBottom;
			}
			else if(viewWidth >= viewHeight) {
				contentWidth = (int) ((float) viewHeight* scaleMetrics.scale) - paddingLeft - paddingRight;
				contentHeight = (int) ((float) viewHeight / drawableWidthToHeightRatio * scaleMetrics.scale) - paddingTop - paddingBottom;
			}//End if(viewWidth < viewHeight)

			scaleOffsetL = ((viewWidth - contentWidth) / 2);
			scaleOffsetT = ((viewHeight - contentHeight) / 2);
			rotateMetrics.imageRadiusX2 = contentWidth;
			rotateMetrics.imageRadius = contentWidth / 2;
			rotationCenterPoint.x = (contentWidth / 2) + scaleOffsetL + (int) moveMetrics.xMove;
			rotationCenterPoint.y = (contentHeight / 2) + scaleOffsetT + (int) moveMetrics.yMove;
			scaleMetrics.setOriginalDimensions(new Point((int) (float) viewWidth - paddingLeft - paddingRight, (int) (float) viewHeight - paddingTop - paddingBottom));

			if(drawable != null) {

				if(contentWidth < contentHeight) {
					drawable.setBounds(paddingLeft, paddingTop, paddingLeft + contentWidth, paddingTop + contentHeight);
				}
				else if(contentWidth >= contentHeight) {
					drawable.setBounds(paddingLeft, paddingTop, paddingLeft + contentWidth, paddingTop + contentHeight);
				}//End if(contentWidth < contentHeight)

				angleWrapper.setCenterPoint(rotationCenterPoint);

				if(rotateMetrics.angleSnap == 0) {
					canvas.rotate((float) (angleWrapper.getTextureAngle() % 1 * 360), rotationCenterPoint.x, rotationCenterPoint.y);
				}
				else /* if(rotateMetrics.angleSnap != 0) */ {
					canvas.rotate((float) (rotateMetrics.angleSnapNext * 360), rotationCenterPoint.x, rotationCenterPoint.y);
				}

				canvas.translate(scaleOffsetL + moveMetrics.xMove, scaleOffsetT + moveMetrics.yMove);

				drawable.draw(canvas);

			}//End if(drawable != null)

			viewIsSetup = true;

		}//End if(viewIsSetup == true)

	}//End protected void onDraw(Canvas canvas)


	private void setupPanZoom() {

		if(moveMetrics.zoomCenterCrop == true) {

			if(viewWidth < viewHeight) {

				if(viewWidthToHeightRatio < drawableWidthToHeightRatio) {
					final double minScale = (1d / viewWidthToHeightRatio) * drawableWidthToHeightRatio;
					scaleMetrics.setUseMinMaxScale((float) minScale, Float.MAX_VALUE, true);
					scaleMetrics.doManualScale((float) minScale);
				}
				else if(viewWidthToHeightRatio >= drawableWidthToHeightRatio) {
					final double minScale = (drawableWidthToHeightRatio / viewWidthToHeightRatio) * (viewWidthToHeightRatio / drawableWidthToHeightRatio);
					scaleMetrics.setUseMinMaxScale((float) minScale, Float.MAX_VALUE, true);
					scaleMetrics.doManualScale((float) minScale);
				}//End if(viewWidthToHeightRatio < drawableWidthToHeightRatio)

			}
			else if(viewWidth >= viewHeight) {

				if(viewWidthToHeightRatio < drawableWidthToHeightRatio) {
					final double minScale = 1d / ((1d / drawableWidthToHeightRatio) * viewWidthToHeightRatio) *  viewWidthToHeightRatio;
					scaleMetrics.setUseMinMaxScale((float) minScale, Float.MAX_VALUE, true);
					scaleMetrics.doManualScale((float) minScale);
				}
				else if(viewWidthToHeightRatio >= drawableWidthToHeightRatio) {
					final double minScale = ((1d / drawableWidthToHeightRatio) * viewWidthToHeightRatio) *  drawableWidthToHeightRatio;
					scaleMetrics.setUseMinMaxScale((float) minScale, Float.MAX_VALUE, true);
					scaleMetrics.doManualScale((float) minScale);
				}//End if(viewWidthToHeightRatio < drawableWidthToHeightRatio)

			}//End if(viewWidth < viewHeight)

		}
		else /* if(moveMetrics.zoomCenterCrop == false) */ {

			if(viewWidth < viewHeight) {

				if(viewWidthToHeightRatio < drawableWidthToHeightRatio) {
					final double minScale = (drawableWidthToHeightRatio / viewWidthToHeightRatio) * (viewWidthToHeightRatio / drawableWidthToHeightRatio);
					scaleMetrics.setUseMinMaxScale((float) minScale, Float.MAX_VALUE, true);
					scaleMetrics.doManualScale((float) minScale);
				}
				else if(viewWidthToHeightRatio >= drawableWidthToHeightRatio) {
					final double minScale = drawableWidthToHeightRatio / viewWidthToHeightRatio;
					scaleMetrics.setUseMinMaxScale((float) minScale, Float.MAX_VALUE, true);
					scaleMetrics.doManualScale((float) minScale);
				}//End if(viewWidthToHeightRatio < drawableWidthToHeightRatio)

			}
			else if(viewWidth >= viewHeight) {

				if(viewWidthToHeightRatio < drawableWidthToHeightRatio) {
					final double minScale = ((1d / drawableWidthToHeightRatio) * viewWidthToHeightRatio) *  drawableWidthToHeightRatio;
					scaleMetrics.setUseMinMaxScale((float) minScale, Float.MAX_VALUE, true);
					scaleMetrics.doManualScale((float) minScale);
				}
				else if(viewWidthToHeightRatio >= drawableWidthToHeightRatio) {

					final double minScale = 1d / ((1d / drawableWidthToHeightRatio) * viewWidthToHeightRatio) *  viewWidthToHeightRatio;
					scaleMetrics.setUseMinMaxScale((float) minScale, Float.MAX_VALUE, true);
					scaleMetrics.doManualScale((float) minScale);

				}//End if(viewWidthToHeightRatio < drawableWidthToHeightRatio)

			}//End if(viewWidth < viewHeight)

		}//End if(moveMetrics.zoomCenterCrop == true)

	}//End private void setupPanZoom()


	//Register main callback
	public void registerCallback(IHacerGestoV3 IHacerGestoV3) {this.iHacerGestoV3 = IHacerGestoV3;}
	public void unRegisterCallback() {this.iHacerGestoV3 = null;}


	private void setFields() {

		if(this.rotateMetrics.spinAnimationHandler == null) {rotateMetrics.spinAnimationHandler = new SpinAnimationHandler(rotateMetrics, this);}
		this.viewIsSetup = false;
		this.paddingLeft = 0;
		this.paddingTop = 0;
		this.paddingRight = 0;
		this.paddingBottom = 0;
		this.contentWidth = 0;
		this.contentHeight = 0;
		this.leftMargin = 0;
		this.topMargin = 0;
		this.rightMargin = 0;
		this.bottomMargin = 0;
		this.scaleOffsetL = 0;
		this.scaleOffsetT = 0;
		this.firstTouchX = 0f;
		this.firstTouchY = 0f;
		this.secondTouchX = 0f;
		this.secondTouchY = 0f;
		this.touchOffsetX = 0f;
		this.touchOffsetY = 0f;
		this.touchPointerCount = 0;
		this.quickTapTime = 100L;
		this.quickTap = false;
		this.gestureDownTime = 0;
		this.useGestures = 0;
		this.quickTap = false;

	}//End private void setFields()


	/* Top of block touch methods */
	private void setDownTouch(final MotionEvent event) {

		try {

			try {

				//touchOffsetXY vars are an open end so that in the future it will be possible to offset the touches
				touchPointerCount = event.getPointerCount();
				firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
				firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
				secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
				secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
				hgv3Info.setFirstTouchX(firstTouchX - viewWidthOver2);
				hgv3Info.setFirstTouchY(firstTouchY - viewHeightOver2);
				hgv3Info.setSecondTouchX(secondTouchX - viewWidthOver2);
				hgv3Info.setSecondTouchY(secondTouchY - viewHeightOver2);

			}
			catch(IndexOutOfBoundsException e) {}

		}
		catch(IllegalArgumentException e) {}

	}//End private void setDownTouch(final MotionEvent event)


	private void setMoveTouch(final MotionEvent event) {

		try {

			try {

				//touchOffsetXY vars are an open end so that in the future it will be possible to offset the touches
				touchPointerCount = event.getPointerCount();
				firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
				firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
				secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
				secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
				hgv3Info.setFirstTouchX(firstTouchX - viewWidthOver2);
				hgv3Info.setFirstTouchY(firstTouchY - viewHeightOver2);
				hgv3Info.setSecondTouchX(secondTouchX - viewWidthOver2);
				hgv3Info.setSecondTouchY(secondTouchY - viewHeightOver2);

			}
			catch(IndexOutOfBoundsException e) {}

		}
		catch(IllegalArgumentException e) {}

	}//End private void setMoveTouch(final MotionEvent event)


	private void setUpTouch(final MotionEvent event) {

		try {

			try {

				//touchOffsetXY vars are an open end so that in the future it will be possible to offset the touches
				touchPointerCount = event.getPointerCount();
				firstTouchX = event.getX(event.findPointerIndex(event.getPointerId(0))) + touchOffsetX;
				firstTouchY = event.getY(event.findPointerIndex(event.getPointerId(0))) + touchOffsetY;
				secondTouchX = event.getX(event.findPointerIndex(event.getPointerId(1))) + touchOffsetX;
				secondTouchY = event.getY(event.findPointerIndex(event.getPointerId(1))) + touchOffsetY;
				hgv3Info.setFirstTouchX(firstTouchX - viewWidthOver2);
				hgv3Info.setFirstTouchY(firstTouchY - viewHeightOver2);
				hgv3Info.setSecondTouchX(secondTouchX - viewWidthOver2);
				hgv3Info.setSecondTouchY(secondTouchY - viewHeightOver2);

			}
			catch(IndexOutOfBoundsException e) {}

		}
		catch(IllegalArgumentException e) {}

	}//End private void setUpTouch(final MotionEvent event)
    /* Bottom of block touch methods */


	/* Top of block rotate functions */
	private HGV3Info doDownTouch(final MotionEvent event) {

		if(touchPointerCount > 2) {return hgv3Info;}
		if(touchPointerCount > 1) {secondFingerWasDown = true;}//To prevent quick tap from functioning when more than one finger is down.

		if(flingMetrics.moveAnimationHandler.flingTriggered == true && flingMetrics.moveAnimationHandler.bounceBack == true) {

			final boolean endReached = (flingMetrics.moveAnimationHandler.positionUpdate.x == flingMetrics.moveAnimationHandler.endPosition.x &&
				flingMetrics.moveAnimationHandler.positionUpdate.y == flingMetrics.moveAnimationHandler.endPosition.y);

			if(endReached == true && flingMetrics.moveAnimationHandler.bounceBack == true) {
				moveMetrics.synchroniseFlingMove(new Point(0, 0));

			}
			else if(endReached == true) {
				flingMetrics.moveAnimationHandler.checkOutOfBounds();
			}

		}//End if(flingMetrics.moveAnimationHandler.flingTriggered == true && flingMetrics.moveAnimationHandler.bounceBack == true)

		flingMetrics.moveAnimationHandler.flingInterrupted = true;
		flingMetrics.moveAnimationHandler.flingTriggered = false;

		quickTap = false;
		gestureDownTime = System.currentTimeMillis();
		hgv3Info.setQuickTap(quickTap);
		hgv3Info.setSpinTriggered(false);

		//Check what gestures are active and execute metrics calculations accordingly
		if((useGestures & ROTATE) == ROTATE) {
			if((useGestures & MOVE) != MOVE) {
				rotateMetrics.setHgv3Info(event);
			}
			else if((useGestures & MOVE) == MOVE && touchPointerCount == 2) {
				rotateMetrics.setHgv3Info(event);
			}
		}
		if((useGestures & SCALE) == SCALE) {scaleMetrics.setHgv3Info(event);}
		if((useGestures & MOVE) == MOVE) {moveMetrics.setHgv3Info(event);}
		if((useGestures & FLING) == FLING) {flingMetrics.setHgv3Info(event);}
		invalidate();

		return hgv3Info;

	}//End private HGV3Info doDownTouch(final int action)


	private HGV3Info doMoveTouch(final MotionEvent event) {

		if(touchPointerCount > 2) {return hgv3Info;}
		//Check what gestures are active and execute metrics calculations accordingly
		if((useGestures & ROTATE) == ROTATE) {rotateMetrics.setHgv3Info(event);}
		if((useGestures & SCALE) == SCALE) {scaleMetrics.setHgv3Info(event);}
		if((useGestures & MOVE) == MOVE) {moveMetrics.setHgv3Info(event);}
		//if((useGestures & FLING) == FLING) {}//Not Used In Context
		invalidate();

		return hgv3Info;

	}//End private HGV3Info doMoveTouch(final int action)


	private HGV3Info doUpTouch(final MotionEvent event) {

		if(touchPointerCount < 1) {return hgv3Info;}
		if(quickTap == false) {

			//Check what gestures are active and execute metrics calculations accordingly
			if((useGestures & ROTATE) == ROTATE) {
				if((useGestures & MOVE) != MOVE) {
					rotateMetrics.setHgv3Info(event);
				}
				else if((useGestures & MOVE) == MOVE && touchPointerCount == 2) {
					rotateMetrics.setHgv3Info(event);
				}
			}
			if((useGestures & SCALE) == SCALE) {scaleMetrics.setHgv3Info(event);}
			if((useGestures & MOVE) == MOVE) {moveMetrics.setHgv3Info(event);}
			if((useGestures & FLING) == FLING) {flingMetrics.setHgv3Info(event);}

			invalidate();

		}//End if(quickTap == false)

		//Condition for quick tap
		if(secondFingerWasDown == false && System.currentTimeMillis() < gestureDownTime + quickTapTime) {

			if(flingMetrics.moveAnimationHandler.flingTriggered == false && rotateMetrics.spinAnimationHandler.spinTriggered == false) {
				quickTap = true;
				hgv3Info.setQuickTap(true);
			}

		}//End if(System.currentTimeMillis() < gestureDownTime + quickTapTime)

		if(touchPointerCount == 1) {secondFingerWasDown = false;}//To prevent quick tap from functioning when more than one finger is down.

		return hgv3Info;

	}//End private HGV3Info doUpTouch(final MotionEvent event)
	/* Bottom of block rotate functions */


	@Override
	public boolean onTouch(final View v, final MotionEvent event) {
		hgTouchEvent(event);
		return true;
	}//End public boolean onTouch(final View v, final MotionEvent event)


	//Convenience method allowing to parse a touch event into the library causing the library to behave as if its' view was touched directly. (Warning not thoroughly tested)
	public void sendTouchEvent(final MotionEvent event) {hgTouchEvent(event);}


	//Main touch handler
	private void hgTouchEvent(final MotionEvent event) {

		final int action = event.getAction() & MotionEvent.ACTION_MASK;

		if(action == MotionEvent.ACTION_MOVE) {

			setMoveTouch(event);
			iHacerGestoV3.onMove(doMoveTouch(event));

		}
		else if(action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_DOWN) {

			setDownTouch(event);
			iHacerGestoV3.onDown(doDownTouch(event));

		}
		else if(action == MotionEvent.ACTION_POINTER_UP || action == MotionEvent.ACTION_UP) {

			setUpTouch(event);
			iHacerGestoV3.onUp(doUpTouch(event));

		}//End if(action == MotionEvent.ACTION_MOVE)

	}//End private void hgTouchEvent(final MotionEvent event)


	//Accessors
	//Rotation
	public double getPrecisionRotation() {return rotateMetrics.getPrecisionRotation();}
	public boolean getCumulativeRotate() {return rotateMetrics.cumulativeRotate;}
	public boolean getUseSingleFinger() {return rotateMetrics.useSingleFinger;}
	public double getVariableDialInner() {return this.rotateMetrics.variableDialInner;}
	public double getVariableDialOuter() {return rotateMetrics.variableDialOuter;}
	public boolean getUseVariableDial() {return rotateMetrics.useVariableDial;}
	public boolean getPositiveCurve() {return rotateMetrics.positiveCurve;}
	public boolean getSpinTriggered() {return rotateMetrics.spinTriggered;}
	public int getLastTextureDirection() {return rotateMetrics.getLastTextureDirection();}
	public Point getCenterPoint() {return this.rotateMetrics.angleWrapper.getCenterPoint();}
	public double getMinimumRotation() {return rotateMetrics.minimumRotation;}
	public double getMaximumRotation() {return rotateMetrics.maximumRotation;}
	public boolean getUseMinMaxRotation() {return rotateMetrics.useMinMaxRotation;}
	public int getFlingDistanceThreshold() {return rotateMetrics.flingDistanceThreshold;}
	public long getFlingTimeThreshold() {return rotateMetrics.flingTimeThreshold;}
	public float getSpinStartSpeed() {return rotateMetrics.spinStartSpeed;}
	public float getSpinEndSpeed() {return rotateMetrics.spinEndSpeed;}
	public long getSpinDuration() {return rotateMetrics.spinDuration;}
	public float getSlowFactor() {return rotateMetrics.slowFactor;}
	public boolean getSlowDown() {return rotateMetrics.slowDown;}
	public float getFlingAngleTolerance() {return rotateMetrics.flingAngleTolerance;}

	//Scaling
	public boolean getUseMinMaxScale() {return scaleMetrics.getUseMinMaxScale();}
	float getMinScale() {return this.scaleMetrics.getMinScale();}
	float getMaxScale() {return this.scaleMetrics.getMaxScale();}
	public int getScaleSnapping() {return scaleMetrics.getScaleSnapping();}


	//Moving
	public boolean getCumulativeMove() {return moveMetrics.getCumulativeMove();}
	public Point[] getMoveSnapping() {return moveMetrics.getMoveSnapping();}
	public int[] getMoveSnappingTolerance() {return moveMetrics.getMoveSnappingTolerance();}
	public MoveMetrics.MoveConstraintObj getMoveConstraintObj() {return moveMetrics.moveConstraintObj;}
	public boolean getZoomCenterCrop() {return moveMetrics.getZoomCenterCrop();}
	public boolean getZoomWithRotation() {return moveMetrics.getZoomWithRotation();}
	public boolean getUsePanZoomMode() {return moveMetrics.getUsePanZoomMode();}


	//Flinging
	public boolean getBounceBack() {return flingMetrics.getBounceBack();}
	public int getFlingDistance() {return flingMetrics.getFlingDistance();}
	public long getFlingToMoveTime() {return flingMetrics.getFlingToMoveTime();}
	public long getFlingToMoveAnimation() {return flingMetrics.getFlingToMoveAnimation();}
	public boolean getFlingOffEdge() {return flingMetrics.getFlingOffEdge();}
	public boolean getFlingToCorner() {return flingMetrics.getFlingToCorner();}
	public boolean getFlingToEdge() {return flingMetrics.getFlingToEdge();}
	public boolean getDoManualFling() {return flingMetrics.getDoManualFling();}
	public boolean getUseDynamicAnimation() {return flingMetrics.getUseDynamicAnimation();}
	public Point getFlingStartPosition() {return flingMetrics.getFlingStartPosition();}
	public Point getFlingEndPosition() {return flingMetrics.getFlingEndPosition();}
	boolean getFlingSlideToCorner() {return flingMetrics.getFlingSlideToCorner();}


	//Mutators
	//Rotation
	public void setPrecisionRotation(final double precisionRotation) {rotateMetrics.setPrecisionRotation(precisionRotation);}
	public void setCumulativeRotate(final boolean cumulativeRotate) {rotateMetrics.cumulativeRotate = cumulativeRotate;}
	public void setUseSingleFinger(final boolean useSingleFinger) {rotateMetrics.useSingleFinger = useSingleFinger;}
	public void setMinMaxDial(final double minimumRotation, final double maximumRotation, final boolean useMinMaxRotation) {rotateMetrics.setMinMaxDial(minimumRotation, maximumRotation, useMinMaxRotation);}
	public void setVariableDial(final double variableDialInner, final double variableDialOuter, final Boolean useVariableDial) {rotateMetrics.setVariableDial(variableDialInner, variableDialOuter, useVariableDial);}
	public void setUseVariableDialCurve(final boolean useVariableDialCurve, final boolean positiveCurve) {rotateMetrics.setUseVariableDialCurve(useVariableDialCurve, positiveCurve);}
	public void setAngleSnap(final double angleSnap, final double angleSnapProximity) {rotateMetrics.setAngleSnap(angleSnap, angleSnapProximity);}
	public void setSlowFactor(final float slowFactor) {rotateMetrics.slowFactor = slowFactor;}
	public void setSlowDown(final boolean slowDown) {rotateMetrics.slowDown = slowDown;}
	public void setFlingAngleTolerance(final float flingAngleTolerance) {rotateMetrics.flingAngleTolerance = flingAngleTolerance;}

	public void setSpinTolerance(final int flingDistanceThreshold, final long flingTimeThreshold) {
		rotateMetrics.flingDistanceThreshold = flingDistanceThreshold;
		rotateMetrics.flingTimeThreshold = flingTimeThreshold;
	}//End public void setSpinTolerance(final int flingDistanceThreshold, final long flingTimeThreshold)


	public void setSpinAnimation(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration) {
		rotateMetrics.spinStartSpeedState = rotateMetrics.spinStartSpeed = spinStartSpeed;
		rotateMetrics.spinEndSpeedState = rotateMetrics.spinEndSpeed = spinEndSpeed;
		rotateMetrics.spinDurationState = rotateMetrics.spinDuration = spinDuration;
	}//End public void setSpinAnimation(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration)


	//Scaling
	public void setScaleSnapping(final int scaleSnapping) {scaleMetrics.setScaleSnapping(scaleSnapping);}
	public void setUseMinMaxScale(final float minScale, final float maxScale, final boolean useMinMaxScale) {scaleMetrics.setUseMinMaxScale(minScale, maxScale, useMinMaxScale);}

	//Moving
	public void setCumulativeMove(final boolean cumulativeMove) {moveMetrics.setCumulativeMove(cumulativeMove);}
	public void setMoveSnapping(final Point[] moveSnapping, final int[] moveSnappingTolerance) {moveMetrics.setMoveSnapping(moveSnapping, moveSnappingTolerance);}
	public void setDoManualFling(final boolean doManualFling) {flingMetrics.setDoManualFling(doManualFling);}
	public void setMoveConstraintObj(final int l, final int t, final int r, final int b, final boolean moveOffEdge, final boolean moveToEdge, final boolean useMoveConstraint) {
		moveMetrics.setMoveConstraintObj(l, t, r, b, moveOffEdge, moveToEdge, useMoveConstraint);//moveOffEdge//moveToEdge
	}

	public void setPanZoomMode(final boolean zoomCenterCrop, final boolean zoomWithRotation, final boolean usePanZoomMode) {
		moveMetrics.setPanZoomMode(zoomCenterCrop, zoomWithRotation, usePanZoomMode);
	}

	//Flinging
	public void setBounceBack(final boolean bounceBack) {flingMetrics.setBounceBack(bounceBack);}
	public void setUseDynamicAnimation(final boolean useDynamicAnimation) {flingMetrics.setUseDynamicAnimation(useDynamicAnimation);}
	public void setFlingSlideToCorner(final boolean flingSlideToCorner) {flingMetrics.setFlingSlideToCorner(flingSlideToCorner);}

	public void setMoveFlingTolerance(final int flingDistance, final long flingToMoveTime, final long flingToMoveAnimation) {
		flingMetrics.setMoveFlingTolerance(flingDistance, flingToMoveTime, flingToMoveAnimation);
	}

	public void setMoveFlingBehaviours(final boolean flingOffEdge, final boolean flingToCorner, final boolean flingToEdge) {
		flingMetrics.setMoveFlingBehaviours(flingOffEdge, flingToCorner, flingToEdge);
	}

	//Common Methods
	public int getUseGestures() {return this.useGestures;}
	public void setUseGestures(final int useGestures) {this.useGestures = useGestures;}
	public long getQuickTapTime() {return this.quickTapTime;}
	public void setQuickTapTime(final long quickTapTime) {this.quickTapTime = quickTapTime;}
	public Drawable getDrawable() {return this.drawable;}
	public void setDrawable(final Drawable drawable) {this.drawable = drawable; viewIsSetup = false; invalidate();}
	public HGV3Info getHgv3Info() {return this.hgv3Info;}


	/* Top of block convenience methods */
	public void doManualTextureDial(final double manualDial) {rotateMetrics.doManualTextureDial(manualDial); invalidate();}
	public void doManualGestureDial(final double manualDial) {rotateMetrics.doManualGestureDial(manualDial); invalidate();}
	public void setFullTextureAngle(final double fullTextureAngle) {this.rotateMetrics.setFullGestureAngle(fullTextureAngle);}
	public double getFullTextureAngle() {return this.rotateMetrics.angleWrapper.getTextureAngle();}
	public void cancelSpin() {rotateMetrics.cancelSpin();}
	public void doManualMove(final Point newPosition) {moveMetrics.doManualMove(newPosition);}
	public void doRapidDial(final double textureAngle) {angleWrapper.setTextureAngle(textureAngle); invalidate();}
	public void doManualScale(final float scale) {scaleMetrics.doManualScale(scale);}


	public void triggerMoveAnimation(final Point startPosition, final Point endPosition, final long animationDuration) {
		flingMetrics.triggerMoveAnimation(startPosition, endPosition, animationDuration);
	}

	public void triggerSpin(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration, final int lastTextureDirection) {
		rotateMetrics.triggerSpin(spinStartSpeed, spinEndSpeed, spinDuration, lastTextureDirection);
	}

	public void performQuickTap() {
		hgv3Info.setQuickTap(true);
		iHacerGestoV3.onUp(hgv3Info);
	}
	/* Bottom of block convenience methods */


	/* Top of block View overrides */
	@Override
	public void requestLayout() {
		viewIsSetup = false;
		super.requestLayout();
	}


	@Override
	public void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
		viewIsSetup = false;
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}


	@Override
	protected void onConfigurationChanged(final Configuration newConfig) {
		viewIsSetup = false;
		super.onConfigurationChanged(newConfig);
	}


	@Override
	public void onDetachedFromWindow() {

		if(this.rotateMetrics.spinAnimationHandler != null) {
			//Fault Tolerance
			this.rotateMetrics.spinAnimationHandler.spinTriggered = false;
		}

		this.viewIsSetup = false;
		this.paddingLeft = 0;
		this.paddingTop = 0;
		this.paddingRight = 0;
		this.paddingBottom = 0;
		this.contentWidth = 0;
		this.contentHeight = 0;
		this.leftMargin = 0;
		this.topMargin = 0;
		this.rightMargin = 0;
		this.bottomMargin = 0;
		this.firstTouchX = 0f;
		this.firstTouchY = 0f;
		this.secondTouchX = 0f;
		this.secondTouchY = 0f;
		this.touchOffsetX = 0f;
		this.touchOffsetY = 0f;
		this.touchPointerCount = 0;
		super.onDetachedFromWindow();
	}
	/* Bottom of block View overrides */

}