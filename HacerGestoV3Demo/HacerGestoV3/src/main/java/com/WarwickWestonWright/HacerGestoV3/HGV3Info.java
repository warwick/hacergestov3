/*
LICENSE. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License

In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".

Copyright (c) 2015, Warwick Weston Wright All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HacerGestoV3;

public final class HGV3Info {

	//Rotate Vars
	private double variablePrecision;
	private int textureDirection;
	private int gestureDirection;
	private double gestureAngle;
	private double textureAngle;
	private int minMaxReached;
	private double touchAngle;
	private boolean rotateSnapped;
	private boolean spinTriggered;
	private float spinCurrentSpeed;

	//Scale Vars
	private float scale;
	private boolean scaleSnapped;
	private int scaleDirection;

	//Move Vars
	private float xMove;
	private float yMove;
	private boolean moveSnapped;
	private int moveSnappedIdx;
	private int moveDirection;

	//Fling Vars
	private boolean flingTriggered;

	//Common Properties
	private float firstTouchX;
	private float firstTouchY;
	private float secondTouchX;
	private float secondTouchY;
	private boolean quickTap;
	private double touchDistance;

	HGV3Info() {

		//Rotate Vars
		this.variablePrecision = 0d;
		this.textureDirection = 0;
		this.gestureDirection = 0;
		this.gestureAngle = 0d;
		this.textureAngle = 0d;
		this.minMaxReached = 0;
		this.touchAngle = 0d;
		this.rotateSnapped = false;
		this.spinTriggered = false;
		this.spinCurrentSpeed = 0f;

		//Scale Vars
		this.scale = 1.0f;
		this.scaleSnapped = false;
		this.scaleDirection = 0;

		//Move Vars
		this.xMove = 0f;
		this.yMove = 0f;
		this.moveSnapped = false;
		this.moveSnappedIdx = -1;
		this.moveDirection = 0;

		//Fling Vars
		this.flingTriggered = false;

		//Common Properties
		this.firstTouchX = 0f;
		this.firstTouchY = 0f;
		this.secondTouchX = 0f;
		this.secondTouchY = 0f;
		this.quickTap = false;
		this.touchDistance = 0d;

	}

	//Accessors
	//Rotate Properties
	public double getVariablePrecision() {return this.variablePrecision;}
	public int getTextureDirection() {return this.textureDirection;}
	public int getGestureDirection() {return this.gestureDirection;}
	public double getGestureAngle() {return this.gestureAngle;}
	public double getTextureAngle() {return this.textureAngle;}
	public int getMinMaxReached() {return this.minMaxReached;}
	public double getTouchAngle() {return this.touchAngle;}
	public boolean getRotateSnapped() {return this.rotateSnapped;}
	public boolean isSpinTriggered() {return this.spinTriggered;}
	public float getSpinCurrentSpeed() {return this.spinCurrentSpeed;}

	//Scale Properties
	public float getScale() {return this.scale;}
	public boolean getScaleSnapped() {return this.scaleSnapped;}
	public int getScaleDirection() {return this.scaleDirection;}

	//Move Properties
	public float[] getCurrentPosition() {return new float[] {xMove, yMove};}
	public boolean getMoveSnapped() {return this.moveSnapped;}
	public int getMoveSnappedIdx() {return this.moveSnappedIdx;}
	public int getMoveDirection() {return this.moveDirection;}

	//Fling Properties
	public boolean getFlingTriggered() {return this.flingTriggered;}

	//Common Properties
	public float getFirstTouchX() {return this.firstTouchX;}
	public float getFirstTouchY() {return this.firstTouchY;}
	public float getSecondTouchX() {return this.secondTouchX;}
	public float getSecondTouchY() {return this.secondTouchY;}
	public boolean getQuickTap() {return this.quickTap;}
	public double getTouchDistance() {return this.touchDistance;}

	//Mutators
	//Rotate Properties
	void setVariablePrecision(final double variablePrecision) {this.variablePrecision = variablePrecision;}
	void setTextureDirection(final int textureDirection) {this.textureDirection = textureDirection;}
	void setGestureDirection(final int gestureDirection) {this.gestureDirection = gestureDirection;}
	void setGestureAngle(final double gestureAngle) {this.gestureAngle = gestureAngle;}
	void setTextureAngle(final double textureAngle) {this.textureAngle = textureAngle;}
	void setMinMaxReached(final int minMaxReached) {this.minMaxReached = minMaxReached;}
	void setTouchAngle(final double touchAngle) {this.touchAngle = touchAngle;}
	void setRotateSnapped(final boolean rotateSnapped) {this.rotateSnapped = rotateSnapped;}
	void setSpinTriggered(final boolean spinTriggered) {this.spinTriggered = spinTriggered;}
	void setSpinCurrentSpeed(final float spinCurrentSpeed) {this.spinCurrentSpeed = spinCurrentSpeed;}

	//Scale Properties
	void setScale(final float scale) {this.scale = scale;}
	void setScaleSnapped(final boolean scaleSnapped) {this.scaleSnapped = scaleSnapped;}
	void setScaleDirection(final int scaleDirection) {this.scaleDirection = scaleDirection;}

	//Move Properties
	void setCurrentPosition(final float xMove, final float yMove) {this.xMove = xMove; this.yMove = yMove;}
	void setMoveSnapped(final boolean moveSnapped, final int moveSnappedIdx) {this.moveSnapped = moveSnapped; this.moveSnappedIdx = moveSnappedIdx;}
	void setMoveDirection(final int moveDirection) {this.moveDirection = moveDirection;}

	//Fling Properties
	void setFlingTriggered(final boolean flingTriggered) {this.flingTriggered = flingTriggered;}

	//Common Properties
	void setFirstTouchX(final float firstTouchX) {this.firstTouchX = firstTouchX;}
	void setFirstTouchY(final float firstTouchY) {this.firstTouchY = firstTouchY;}
	void setSecondTouchX(final float secondTouchX) {this.secondTouchX = secondTouchX;}
	void setSecondTouchY(final float secondTouchY) {this.secondTouchY = secondTouchY;}
	void setQuickTap(final boolean quickTap) {this.quickTap = quickTap;}
	void setTouchDistance(double touchDistance) {this.touchDistance = touchDistance;}

}