package com.WarwickWestonWright.HacerGestoV3;

import android.graphics.Point;
import android.os.Handler;
import android.os.Message;

import java.lang.ref.WeakReference;

class MoveAnimationHandler implements Runnable {

	private final MoveHandler moveHandler = new MoveHandler(this);
	private final FlingMetrics flingMetrics;
	private final MoveMetrics moveMetrics;
	private final HacerGestoV3 hacerGestoV3;
	final Point startPosition = new Point(0, 0);
	final Point endPosition = new Point(0, 0);
	final Point positionUpdate = new Point(0, 0);
	private long flingToMoveAnimation;
	boolean flingTriggered;
	boolean flingInterrupted;
	boolean doManualFling;
	boolean bounceBack;
	long moveEndTime;
	boolean flingOutOfBounds;

	private Thread moveAnimationThread;

	public MoveAnimationHandler(final FlingMetrics flingMetrics, final MoveMetrics moveMetrics, final HacerGestoV3 hacerGestoV3) {
		this.flingMetrics = flingMetrics;
		this.moveMetrics = moveMetrics;
		this.hacerGestoV3 = hacerGestoV3;
		this.flingInterrupted = false;
	}//End public MoveAnimationHandler(final FlingMetrics flingMetrics, final MoveMetrics moveMetrics, final HacerGestoV3 hacerGestoV3)

	void startMoveAnim() {

		if(doManualFling == true) {

			hacerGestoV3.post(new Runnable() {
				@Override
				public void run() {
					if(hacerGestoV3.iHacerGestoV3 != null) {

						moveMetrics.hgv3Info.setFlingTriggered(false);
						hacerGestoV3.iHacerGestoV3.onUp(moveMetrics.hgv3Info);
						hacerGestoV3.invalidate();

					}
				}
			});

			doManualFling = false;
			return;
		}

		this.flingToMoveAnimation = flingMetrics.flingToMoveAnimation;
		if(moveAnimationThread == null) {moveAnimationThread = new Thread(this);}

		moveAnimationThread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread t, Throwable e) {
				flingTriggered = false;
				moveAnimationThread = null;
			}
		});

		try {
			flingTriggered = true;
			flingInterrupted = false;
			moveAnimationThread.start();
		}
		catch(IllegalThreadStateException e) {
			moveAnimationThread = null;
			flingTriggered = false;
		}

	}//End void startMoveAnim()


	@Override
	public void run() {
		doAnimation();
		moveHandler.sendEmptyMessage(0);
	}//End public void run()


	private void doAnimation() {

		long currentTime = System.currentTimeMillis();

			while(currentTime < moveEndTime) {

				if(flingInterrupted == true) {break;}

				if(currentTime + 2L < System.currentTimeMillis()) {

					currentTime = System.currentTimeMillis();
					final long oneRatio = System.currentTimeMillis() - (moveEndTime - flingToMoveAnimation);
					final double realRatio = ((double) oneRatio / (double) flingMetrics.flingToMoveAnimation);

					positionUpdate.x = (int)(startPosition.x + (realRatio * (endPosition.x - startPosition.x)));
					positionUpdate.y = (int) (startPosition.y + (realRatio * (endPosition.y - startPosition.y)));
					if((flingOutOfBounds = checkOutOfBounds()) == false) {
						moveMetrics.synchroniseFlingMove(positionUpdate);
					}
					else {

						if(moveMetrics.usePanZoomMode == false) {
							positionUpdate.x = endPosition.x;
							positionUpdate.y = endPosition.y;
							moveMetrics.synchroniseFlingMove(positionUpdate);
							break;
						}

					}

					hacerGestoV3.post(new Runnable() {
						@Override
						public void run() {
							if(hacerGestoV3.iHacerGestoV3 != null) {
								hacerGestoV3.iHacerGestoV3.onMove(moveMetrics.hgv3Info);
								hacerGestoV3.invalidate();
							}
						}
					});

				}//End if(currentTime + 5L < System.currentTimeMillis())

			}//End while(currentTime < moveEndTime)

	}//End private void doAnimation()


	boolean checkOutOfBounds() {

		boolean returnValue = false;

		if(moveMetrics.usePanZoomMode == false) {

			if(endPosition.x < startPosition.x && positionUpdate.x < endPosition.x) {moveMetrics.synchroniseFlingMove(positionUpdate); return true;}
			if(endPosition.y < startPosition.y && positionUpdate.y < endPosition.y) {moveMetrics.synchroniseFlingMove(positionUpdate); return true;}
			if(endPosition.x > startPosition.x && positionUpdate.x > endPosition.x) {moveMetrics.synchroniseFlingMove(positionUpdate); return true;}
			if(endPosition.y > startPosition.y && positionUpdate.y > endPosition.y) {moveMetrics.synchroniseFlingMove(positionUpdate); return true;}

		}
		else /* if(moveMetrics.usePanZoomMode == true) */ {

			if(flingMetrics.flingSlideToCorner == true) {

				boolean lrBoundsHit = false;
				boolean tbBoundsHit = false;

				if(positionUpdate.x < moveMetrics.lConstraint) {
					positionUpdate.x = moveMetrics.lConstraint;
					lrBoundsHit = true;
				}
				if(positionUpdate.y < moveMetrics.tConstraint) {
					positionUpdate.y = moveMetrics.tConstraint;
					tbBoundsHit = true;
				}
				if(positionUpdate.x > moveMetrics.rConstraint) {
					positionUpdate.x = moveMetrics.rConstraint;
					lrBoundsHit = true;
				}
				if(positionUpdate.y > moveMetrics.bConstraint) {
					positionUpdate.y = moveMetrics.bConstraint;
					tbBoundsHit = true;
				}

				returnValue = (lrBoundsHit == true && tbBoundsHit == true) ? true : false;

			}
			else /* if(flingMetrics.flingSlideToCorner == false) */ {

				if(positionUpdate.x < moveMetrics.lConstraint || positionUpdate.x > moveMetrics.rConstraint) {
					positionUpdate.x = endPosition.x;
					returnValue = true;
				}

				if(positionUpdate.y < moveMetrics.tConstraint || positionUpdate.y > moveMetrics.bConstraint) {
					positionUpdate.y = endPosition.y;
					returnValue = true;
				}

			}//End if(flingMetrics.flingSlideToCorner == true)

		}//End if(moveMetrics.usePanZoomMode == false)

		moveMetrics.hgv3Info.setCurrentPosition(positionUpdate.x, positionUpdate.y);

		return returnValue;

	}//End boolean checkOutOfBounds()


	private static class MoveHandler extends Handler {

		private final WeakReference<MoveAnimationHandler> moveAnimationHandlerWeakReference;

		MoveHandler(MoveAnimationHandler moveAnimationHandler) {
			moveAnimationHandlerWeakReference = new WeakReference<>(moveAnimationHandler);
		}

		@Override
		public void handleMessage(Message msg) {

			if(moveAnimationHandlerWeakReference != null) {

				final MoveAnimationHandler moveAnimationHandler = moveAnimationHandlerWeakReference.get();
				moveAnimationHandler.moveMetrics.hgv3Info.setFlingTriggered(false);

				moveAnimationHandler.hacerGestoV3.post(new Runnable() {
					@Override
					public void run() {

						moveAnimationHandler.moveAnimationThread = null;
						moveAnimationHandler.checkOutOfBounds();

						if(moveAnimationHandler.flingInterrupted == true) {
							moveAnimationHandler.moveMetrics.hgv3Info.setCurrentPosition(moveAnimationHandler.positionUpdate.x, moveAnimationHandler.positionUpdate.y);
						}
						else {
							moveAnimationHandler.positionUpdate.x = moveAnimationHandler.endPosition.x;
							moveAnimationHandler.positionUpdate.y = moveAnimationHandler.endPosition.y;
							if(moveAnimationHandler.flingMetrics.flingSlideToCorner == false) {
								moveAnimationHandler.moveMetrics.synchroniseFlingMove(moveAnimationHandler.endPosition);
							}
							moveAnimationHandler.moveMetrics.hgv3Info.setCurrentPosition(moveAnimationHandler.endPosition.x, moveAnimationHandler.endPosition.y);
						}

						moveAnimationHandler.flingInterrupted = false;
						moveAnimationHandler.hacerGestoV3.invalidate();
						moveAnimationHandler.hacerGestoV3.iHacerGestoV3.onUp(moveAnimationHandler.moveMetrics.hgv3Info);
						moveAnimationHandler.flingInterrupted = false;
						//Toast.makeText(moveAnimationHandler.hacerGestoV3.getContext(), "Thread Complete", Toast.LENGTH_SHORT).show();

					}
				});

			}//End if(moveAnimationHandlerWeakReference != null)

		}

	}//End private static class MoveHandler extends Handler

}