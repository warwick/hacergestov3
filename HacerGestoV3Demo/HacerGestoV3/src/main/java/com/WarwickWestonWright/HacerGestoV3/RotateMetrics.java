/*
LICENSE. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License

In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".

Copyright (c) 2015, Warwick Weston Wright All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HacerGestoV3;

import android.graphics.Point;
import android.view.MotionEvent;

final class RotateMetrics {

	/*
	Developer note about separation of concerns strategy. This class only ever reads package local fields from the metrics classes (scaleMetrics, moveMetrics and flingMetrics)
	and never write to each other.
	Addendum: This is a new library and the separation of concerns has not been fully checked so their may be some mixed concerns that need fixing.
	*/

	//Top of block separation of concerns objects
	final HGV3Info hgv3Info;
	private final HGGeometry hgGeometry;
	final AngleWrapper angleWrapper;
	private final HacerGestoV3 hacerGestoV3;
	SpinAnimationHandler spinAnimationHandler;
	//Bottom of block separation of concerns objects

	//Top of block behavioural flags
	boolean cumulativeRotate;
	boolean useSingleFinger;
	boolean useMinMaxRotation;
	boolean useVariableDial;
	private boolean useVariableDialCurve;
	boolean positiveCurve;
	private boolean rotateSnapped;
	//Bottom of block behavioural flags returnable to client

	//Top of block other behavioural values
	double precisionRotation;
	private double onDownAngle;
	private double onUpAngle;
	double angleSnap;
	double angleSnapNext;
	private double angleSnapProximity;
	double variableDialInner;
	double variableDialOuter;
	double maximumRotation;
	double minimumRotation;
	private int minMaxRotationOutOfBounds;
	//Bottom of block other behavioural values

	//Top of block Fling Spin Variables
	float spinStartSpeed;
	float spinEndSpeed;
	long spinDuration;
	float spinStartSpeedState;
	float spinEndSpeedState;
	long spinDurationState;
	float slowFactor;
	boolean slowDown;
	float flingAngleTolerance;
	private float spinCurrentSpeed;
	private long spinEndTime;
	private double spinStartAngle;
	private double fullGestureDownAngle;
	private long gestureDownTime;//Also used for quick tap
	private float flingDownTouchX;
	private float flingDownTouchY;
	int flingDistanceThreshold;
	long flingTimeThreshold;
	volatile boolean spinTriggered;
	boolean spinTriggeredProgrammatically;
	private boolean slowSpinOnCurve;
	private boolean slowSpinOnPositiveCurve;
	private int lastTextureDirection;//Also used for rotation direction in dynamic return type (HGDialInfo)
	//Bottom of block Fling Spin Variables

	//Top of block other variables for internal usage
	private float touchXLocal;
	private float touchYLocal;
	double imageRadiusX2;
	double imageRadius;
	private double storedGestureAngle;
	double fullGestureAngle;
	private double currentGestureAngle;
	private int touchPointerCount;
	//Bottom of block other variables for internal usage

	RotateMetrics(final HacerGestoV3 hacerGestoV3, final HGV3Info hgv3Info, final HGGeometry hgGeometry, final AngleWrapper angleWrapper) {

		//X-Class objects instantiation
		this.hacerGestoV3 = hacerGestoV3;
		this.hgv3Info = hgv3Info;
		this.hgGeometry = hgGeometry;
		this.angleWrapper = angleWrapper;
		this.spinAnimationHandler = new SpinAnimationHandler(this, hacerGestoV3);

		//Top of block behavioural flags
		this.cumulativeRotate = true;
		this.useSingleFinger = true;
		this.useMinMaxRotation = false;
		this.useVariableDial = false;
		this.useVariableDialCurve = false;
		this.positiveCurve = false;
		this.rotateSnapped = false;
		//Bottom of block behavioural flags returnable to client

		//Top of block other behavioural values
		this.precisionRotation = 1d;
		this.onDownAngle = 0d;
		this.onUpAngle = 0d;
		this.angleSnap = 0d;
		this.angleSnapNext = 0d;
		this.angleSnapProximity = 0d;
		this.variableDialInner = 0d;
		this.variableDialOuter = 0d;
		this.maximumRotation = 0d;
		this.minimumRotation = 0d;
		this.minMaxRotationOutOfBounds = 0;
		//Bottom of block other behavioural values

		//Top of block Fling Spin Variables
		this.spinStartSpeed = 0f;
		this.spinEndSpeed = 0f;
		this.spinDuration = 0L;
		this.spinStartSpeedState = this.spinStartSpeed;
		this.spinEndSpeedState = this.spinEndSpeed;
		this.spinDurationState = this.spinDuration;
		this.slowFactor = 0f;
		this.slowDown = true;
		this.flingAngleTolerance = 0f;
		this.spinCurrentSpeed = 0f;
		this.spinEndTime = 0L;
		this.spinStartAngle = 0d;
		this.fullGestureDownAngle = 0d;
		this.gestureDownTime = 0L;//Also used for quick tap
		this.flingDownTouchX = 0f;
		this.flingDownTouchY = 0f;
		this.flingDistanceThreshold = 0;
		this.flingTimeThreshold = 0L;
		this.spinTriggered = false;
		this.spinTriggeredProgrammatically = false;
		this.slowSpinOnCurve = false;
		this.slowSpinOnPositiveCurve = false;
		this.lastTextureDirection = 0;//Also used for rotation direction in dynamic return type (HGDialInfo)
		//Bottom of block Fling Spin Variables

		//Top of block other variables for internal usage
		this.touchXLocal = 0f;
		this.touchYLocal = 0f;
		this.imageRadiusX2 = 0d;
		this.imageRadius = 0d;
		this.storedGestureAngle = 0d;
		this.fullGestureAngle = 0d;
		this.currentGestureAngle = 0d;
		//Bottom of block other variables for internal usage

	}//End RotateMetrics(final HacerGestoV3 hacerGestoV3, final HGV3Info hgv3Info, final HGGeometry hgGeometry, final AngleWrapper angleWrapper)


	//Top of block main touch Handler
	void setHgv3Info(final MotionEvent event) {

		final int action = event.getAction() & MotionEvent.ACTION_MASK;

		if(action == MotionEvent.ACTION_MOVE) {

			//Prepare touches for single or dual touch
			if(touchPointerCount == 1 && useSingleFinger == true) {
				touchXLocal = hacerGestoV3.firstTouchX ;
				touchYLocal = hacerGestoV3.firstTouchY;
				angleWrapper.setTouchAngle(hgGeometry.getAngleFromPoint(angleWrapper.getCenterPoint(), new Point((int) touchXLocal, (int) touchYLocal)));
				setReturnType();
			}
			else if(touchPointerCount == 2 && useSingleFinger == false) {
				touchXLocal = hacerGestoV3.secondTouchX;
				touchYLocal = hacerGestoV3.secondTouchY;
				angleWrapper.setTouchAngle(hgGeometry.getAngleFromPoint(angleWrapper.getCenterPoint(), new Point((int) touchXLocal, (int) touchYLocal)));
				setReturnType();
			}//End if(touchPointerCount == 1 && useSingleFinger == true)
		}
		else if(action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_DOWN) {

			fullGestureDownAngle = fullGestureAngle;

			if(action == MotionEvent.ACTION_DOWN) {
				touchPointerCount = 1;
				flingDownTouchX = hacerGestoV3.firstTouchX;
				flingDownTouchY = hacerGestoV3.firstTouchY;
			}

			if(action == MotionEvent.ACTION_POINTER_DOWN) {touchPointerCount = 2;}

			//Prepare touches for single or dual touch
			if(touchPointerCount == 1 && useSingleFinger == true) {
				touchXLocal = hacerGestoV3.firstTouchX;
				touchYLocal = hacerGestoV3.firstTouchY;
			}
			else if(touchPointerCount == 1 && useSingleFinger == false) {
				final Point lastAngle = hgGeometry.getPointFromAngle(fullGestureAngle, imageRadius);
				touchXLocal = lastAngle.x + hacerGestoV3.getCenterPoint().x;
				touchYLocal = lastAngle.y + hacerGestoV3.getCenterPoint().y;
			}
			else if(touchPointerCount == 2 && useSingleFinger == false) {
				touchXLocal = hacerGestoV3.secondTouchX;
				touchYLocal = hacerGestoV3.secondTouchY;
			}
			else if(touchPointerCount == 2 && useSingleFinger == true) {
				return;
			}//End if(touchPointerCount == 1 && useSingleFinger == true)

			if(spinTriggered == true && angleSnap != 0) {if(useVariableDial == false) {doManualTextureDial(angleSnapNext);}}

			if(spinTriggered == true) {

				spinAnimationHandler.spinTriggered = spinTriggered = false;
				spinCurrentSpeed = (float) spinAnimationHandler.spinCurrentSpeed;

			}//End if(spinTriggered == true)

			gestureDownTime = System.currentTimeMillis();
			angleWrapper.setTouchAngle(hgGeometry.getAngleFromPoint(angleWrapper.getCenterPoint(), new Point((int) touchXLocal, (int) touchYLocal)));

			if(precisionRotation != 0) {

				if(cumulativeRotate == true) {

					onDownAngle = angleWrapper.getTouchAngle() - onUpAngle;

					if(useVariableDial == true) {fullGestureAngle = fullGestureAngle * precisionRotation;}

				}
				else /* if(cumulativeRotate == false) */ {

					minMaxRotationOutOfBounds = 0;
					onDownAngle = angleWrapper.getTouchAngle() - onUpAngle;

					if(useVariableDial == true) {

						fullGestureAngle = angleWrapper.getTouchAngle();

					}
					else if(useVariableDial == false) {

						fullGestureAngle = angleWrapper.getTouchAngle() / precisionRotation;

					}//End if(useVariableDial == true)

				}//End if(cumulativeRotate == true)

				setReturnType();
				spinStartAngle = angleWrapper.getTextureAngle();

			}//End if(precisionRotation != 0)

		}
		else if(action == MotionEvent.ACTION_POINTER_UP || action == MotionEvent.ACTION_UP) {

			//Prepare touches for single or dual touch
			if(useSingleFinger == false && action == MotionEvent.ACTION_POINTER_UP) {touchPointerCount = 0;}

			if(touchPointerCount == 1 && useSingleFinger == true) {
				touchXLocal = hacerGestoV3.firstTouchX;
				touchYLocal = hacerGestoV3.firstTouchY;
			}
			else if(touchPointerCount == 2 && useSingleFinger == false) {
				touchXLocal = hacerGestoV3.secondTouchX;
				touchYLocal = hacerGestoV3.secondTouchY;
			}//End if(touchPointerCount == 1 && useSingleFinger == true)

			angleWrapper.setTouchAngle(hgGeometry.getAngleFromPoint(angleWrapper.getCenterPoint(), new Point((int) touchXLocal, (int) touchYLocal)));

			if(precisionRotation != 0) {
				onUpAngle = (1 - (onDownAngle - angleWrapper.getTouchAngle())) % 1;
				setReturnType();
			}//End if(precisionRotation != 0)

			setSpinStatus();

			if(spinTriggered == true) {
				spinAnimationHandler.startSpinAnim();
			}
			else if(spinTriggered == false) {
				hacerGestoV3.getHgv3Info().setTextureDirection(0);
				hacerGestoV3.getHgv3Info().setGestureDirection(0);
			}//End if(flingTriggered == true)

		}//End if(motionEventAction == MotionEvent.ACTION_MOVE)

	}//End void setHgv3Info(final MotionEvent event)
	//Bottom of block main touch Handler


	//Sets the return type (HGV3Info) accessible in the main callback
	private void setReturnType() {

		if(useVariableDial == false) {

			if(useMinMaxRotation == false) {
				hgv3Info.setGestureDirection(setRotationAngleGestureAndReturnDirection());
			}
			else /* if(useMinMaxRotation == true) */ {
				hgv3Info.setGestureDirection(setRotationAngleGestureAndReturnDirectionForMinMaxBehaviour());
			}//End if(useMinMaxRotation == false)

			hgv3Info.setGestureAngle(angleWrapper.getGestureAngle());
			hgv3Info.setTextureDirection(lastTextureDirection);
			hgv3Info.setTextureAngle(angleWrapper.getTextureAngle());
			hgv3Info.setRotateSnapped(rotateSnapped);
			calculateTextureAngle();

		}
		else /* if(useVariableDial == true) */ {

			if(useMinMaxRotation == false) {
				setVariableDialAngleAndReturnDirection();
			}
			else /* if(useMinMaxRotation == true) */ {
				setVariableDialAngleAndReturnDirectionForMinMaxBehaviour();
			}//End if(useMinMaxRotation == false)

			hgv3Info.setGestureAngle(angleWrapper.getTouchAngle());
			hgv3Info.setTextureDirection(lastTextureDirection);
			hgv3Info.setTextureAngle(angleWrapper.getTextureAngle());
			hgv3Info.setRotateSnapped(rotateSnapped);

		}//End if(useVariableDial == false)

		if(angleSnap != 0) {
			checkAngleSnap();
		}

		hgv3Info.setTouchAngle(angleWrapper.getTouchAngle());

	}//End private void setReturnType()


	private void calculateTextureAngle() {angleWrapper.setTextureAngle(fullGestureAngle * precisionRotation);}


	private int setRotationAngleGestureAndReturnDirection() {

		final int[] returnValue = new int[1];
		currentGestureAngle = (1 - (onDownAngle - angleWrapper.getTouchAngle()));
		final float angleDifference = (float) (storedGestureAngle - currentGestureAngle);

		//Detect direction
		if(!(Math.abs(angleDifference) > 0.75d)) {

			if(angleDifference > 0) {

				returnValue[0] = -1;
				fullGestureAngle -= (angleDifference + 1d) % 1d;

			}
			else if(angleDifference < 0) {

				returnValue[0] = 1;
				fullGestureAngle += -angleDifference % 1d;

			}//End if(angleDifference > 0)

			if(returnValue[0] != 0) {

				if(precisionRotation < 0) {

					lastTextureDirection = -returnValue[0];

				}
				else if(precisionRotation > 0) {

					lastTextureDirection = returnValue[0];

				}
				else {

					lastTextureDirection = 0;

				}//End if(precisionRotation < 0)

			}//End if(returnValue[0] != 0)

		}//End if(!(Math.abs(angleDifference) > 0.75d))

		angleWrapper.setGestureAngle(fullGestureAngle);
		storedGestureAngle = currentGestureAngle;

		return returnValue[0];

	}//End private int setRotationAngleGestureAndReturnDirection()


	private int setRotationAngleGestureAndReturnDirectionForMinMaxBehaviour() {

		final int[] returnValue = new int[1];
		currentGestureAngle = (1 - (onDownAngle - angleWrapper.getTouchAngle()));
		final double angleDifference = (storedGestureAngle - currentGestureAngle);

		//Detect direction
		if(!(Math.abs(angleDifference) > 0.75d)) {

			if(angleDifference > 0) {

				returnValue[0] = -1;
				fullGestureAngle -= (angleDifference + 1d) % 1d;

			}
			else if(angleDifference < 0) {

				returnValue[0] = 1;
				fullGestureAngle += -angleDifference % 1d;

			}//End if(angleDifference > 0)

			if(returnValue[0] != 0) {

				if(precisionRotation < 0) {

					lastTextureDirection = -returnValue[0];

				}
				else if(precisionRotation > 0) {

					lastTextureDirection = returnValue[0];

				}
				else {

					lastTextureDirection = 0;

				}//End if(precisionRotation < 0)

			}//End if(returnValue[0] != 0)

		}//End if(!(Math.abs(angleDifference) > 0.75d))

		if(precisionRotation > 0) {

			if(fullGestureAngle < minimumRotation / precisionRotation) {

				minMaxRotationOutOfBounds = -1;
				hgv3Info.setMinMaxReached(minMaxRotationOutOfBounds);
				fullGestureAngle = minimumRotation / precisionRotation;

			}
			else if(fullGestureAngle > maximumRotation / precisionRotation) {

				minMaxRotationOutOfBounds = 1;
				hgv3Info.setMinMaxReached(minMaxRotationOutOfBounds);
				fullGestureAngle = maximumRotation / precisionRotation;

			}
			else {

				hgv3Info.setMinMaxReached(0);

			}//End if(fullGestureAngle < minimumRotation / precisionRotation)

		}
		else if(precisionRotation < 0) {

			if(-fullGestureAngle < -(minimumRotation / precisionRotation)) {

				minMaxRotationOutOfBounds = -1;
				hgv3Info.setMinMaxReached(minMaxRotationOutOfBounds);
				fullGestureAngle = (minimumRotation / precisionRotation);

			}
			else if(-fullGestureAngle > -(maximumRotation / precisionRotation)) {

				minMaxRotationOutOfBounds = 1;
				hgv3Info.setMinMaxReached(minMaxRotationOutOfBounds);
				fullGestureAngle = (maximumRotation / precisionRotation);

			}
			else {

				hgv3Info.setMinMaxReached(0);

			}//End if(-fullGestureAngle < -(minimumRotation / precisionRotation))

		}//End if(precisionRotation > 0)

		angleWrapper.setGestureAngle(fullGestureAngle);
		storedGestureAngle = currentGestureAngle;

		return returnValue[0];

	}//End private int setRotationAngleGestureAndReturnDirectionForMinMaxBehaviour()


	private void setVariableDialAngleAndReturnDirection() {

		final int[] returnValue = new int[1];
		currentGestureAngle = (1 - (onDownAngle - angleWrapper.getTouchAngle()));
		final double angleDifference = (storedGestureAngle - currentGestureAngle) % 1;
		final double variablePrecision;

		if(useVariableDialCurve == false) {

			//Variable dial acceleration on a straight line.
			variablePrecision = getVariablePrecision();

		}
		else /* if(useVariableDialCurve == true) */ {

			//Variable dial acceleration on a curve.
			variablePrecision = getVariablePrecisionCurve();

		}//End if(useVariableDialCurve == false)

		//Detect direction
		if(!(Math.abs(angleDifference) > 0.75d)) {

			if(angleDifference > 0) {

				returnValue[0] = -1;

				if(minMaxRotationOutOfBounds == 0) {

					fullGestureAngle -= ((angleDifference * variablePrecision));

				}

			}
			else if(angleDifference < 0) {

				returnValue[0] = 1;

				if(minMaxRotationOutOfBounds == 0) {

					fullGestureAngle += -(angleDifference * variablePrecision);

				}

			}//End if(angleDifference > 0)

			if(returnValue[0] != 0) {

				if(variablePrecision > 0) {

					lastTextureDirection = returnValue[0];

				}
				else if(variablePrecision < 0) {

					lastTextureDirection = -returnValue[0];

				}//End if(variablePrecision > 0)

				hgv3Info.setTextureDirection(lastTextureDirection);

			}//End if(returnValue[0] != 0)

		}//End if(!(Math.abs(angleDifference) > 0.75d))

		angleWrapper.setTextureAngle(fullGestureAngle);
		storedGestureAngle = currentGestureAngle;

	}//End private void setVariableDialAngleAndReturnDirection()


	private void setVariableDialAngleAndReturnDirectionForMinMaxBehaviour() {

		final int[] returnValue = new int[1];
		currentGestureAngle = (1 - (onDownAngle - angleWrapper.getTouchAngle()));
		final double angleDifference = (storedGestureAngle - currentGestureAngle) % 1;
		final double variablePrecision;

		if(useVariableDialCurve == false) {

			//Variable dial acceleration on a straight line.
			variablePrecision = getVariablePrecision();

		}
		else /* if(useVariableDialCurve == true) */ {

			//Variable dial acceleration on a curve.
			variablePrecision = getVariablePrecisionCurve();

		}//End if(useVariableDialCurve == false)

		//Detect direction
		if(!(Math.abs(angleDifference) > 0.75d)) {

			if(angleDifference > 0) {

				returnValue[0] = -1;
				fullGestureAngle -= ((angleDifference * variablePrecision));

			}
			else if(angleDifference < 0) {

				returnValue[0] = 1;
				fullGestureAngle += -(angleDifference * variablePrecision);

			}//End if(angleDifference > 0)

			if(returnValue[0] != 0) {

				if(variablePrecision > 0) {

					lastTextureDirection = returnValue[0];

				}
				else if(variablePrecision < 0) {

					lastTextureDirection = -returnValue[0];

				}//End if(variablePrecision > 0)

				hgv3Info.setTextureDirection(lastTextureDirection);

			}//End if(returnValue[0] != 0)

		}//End if(!(Math.abs(angleDifference) > 0.75d))

		if(fullGestureAngle < minimumRotation) {

			minMaxRotationOutOfBounds = -1;
			hgv3Info.setMinMaxReached(minMaxRotationOutOfBounds);
			fullGestureAngle = minimumRotation;

		}
		else if(fullGestureAngle > maximumRotation) {

			minMaxRotationOutOfBounds = 1;
			hgv3Info.setMinMaxReached(minMaxRotationOutOfBounds);
			fullGestureAngle = maximumRotation;

		}//End if(fullGestureAngle < minimumRotation)

		if(minMaxRotationOutOfBounds != 0) {

			if(variablePrecision > 0) {

				if(fullGestureAngle == minimumRotation) {

					if(returnValue[0] == 1) {minMaxRotationOutOfBounds = 0; hgv3Info.setMinMaxReached(minMaxRotationOutOfBounds);}

				}
				else if(fullGestureAngle == maximumRotation) {

					if(returnValue[0] == -1) {minMaxRotationOutOfBounds = 0; hgv3Info.setMinMaxReached(minMaxRotationOutOfBounds);}

				}
				else {

					hgv3Info.setMinMaxReached(0);

				}//End if(fullGestureAngle == minimumRotation)

			}
			else if(variablePrecision < 0) {

				if(fullGestureAngle == minimumRotation) {

					if(returnValue[0] == -1) {minMaxRotationOutOfBounds = 0; hgv3Info.setMinMaxReached(minMaxRotationOutOfBounds);}

				}
				else if(fullGestureAngle == maximumRotation) {

					if(returnValue[0] == 1) {minMaxRotationOutOfBounds = 0; hgv3Info.setMinMaxReached(minMaxRotationOutOfBounds);}

				}
				else {

					hgv3Info.setMinMaxReached(0);

				}//End if(fullGestureAngle == minimumRotation)

			}//End if(variablePrecision > 0)

		}//End if(minMaxRotationOutOfBounds != 0)

		angleWrapper.setTextureAngle(fullGestureAngle);
		storedGestureAngle = currentGestureAngle;

	}//End private void setVariableDialAngleAndReturnDirectionForMinMaxBehaviour()


	private double getVariablePrecision() {

		double distanceFromCenter = hgGeometry.getTwoFingerDistance(angleWrapper.getCenterPoint().x, angleWrapper.getCenterPoint().y, touchXLocal, touchYLocal);
		double[] variablePrecision = new double[1];

		//Behavioural note about this method. The rotation will occur if user is touching anywhere on the view regardless of the visual size of the image.
		//If you only want rotation to occur when touching the visible image use method getVariablePrecisionCurve with an identical inner and outer precision.
		if(distanceFromCenter <= imageRadius) {

			variablePrecision[0] = (variableDialInner - (((distanceFromCenter / imageRadius) * (variableDialInner - variableDialOuter)) + variableDialOuter)) + variableDialOuter;
			hgv3Info.setVariablePrecision(variablePrecision[0]);

		}

		return variablePrecision[0];

	}//End private double getVariablePrecision()


	private double getVariablePrecisionCurve() {

		final double distanceFromCenter = hgGeometry.getTwoFingerDistance(angleWrapper.getCenterPoint().x, angleWrapper.getCenterPoint().y, touchXLocal, touchYLocal);
		double[] variablePrecision = new double[1];

		//Behavioural note about this method: The rotation will only occur if user is touching the physical image and will not rotate if touches are outside the image. This is by design.
		if(distanceFromCenter <= imageRadius) {

			final double rangeDiff = (variableDialInner - variableDialOuter) * ((distanceFromCenter - imageRadius) / imageRadius);

			if(positiveCurve == false) {

				variablePrecision[0] = -rangeDiff * (1d - (Math.sin((distanceFromCenter / imageRadiusX2) * Math.PI))) + variableDialOuter;
				hgv3Info.setVariablePrecision(variablePrecision[0]);

			}
			else /* if(positiveCurve == true) */ {

				variablePrecision[0] = -rangeDiff * (Math.cos((distanceFromCenter / imageRadiusX2) * Math.PI)) + variableDialOuter;
				hgv3Info.setVariablePrecision(variablePrecision[0]);

			}//End if(positiveCurve == false)

		}//End if(distanceFromCenter <= imageRadius)

		return variablePrecision[0];

	}//End private double getVariablePrecisionCurve()


	//Checks if spin has been triggered setting all appropriate flags in preparation for the spin animation.
	private void setSpinStatus() {

		final long currentTime = System.currentTimeMillis();
		final double twoFingerDistance = hgGeometry.getTwoFingerDistance(flingDownTouchX, flingDownTouchY, touchXLocal, touchYLocal);
		final boolean spinIsDisabled;
		final boolean angleToleranceMet = Math.abs(fullGestureDownAngle - fullGestureAngle) > flingAngleTolerance;
		final boolean distanceToleranceMet = twoFingerDistance > flingDistanceThreshold;

		if((flingAngleTolerance != 0f && angleToleranceMet == true) || (flingDistanceThreshold != 0  && flingAngleTolerance == 0f && distanceToleranceMet == true)) {
			spinIsDisabled = false;
		}
		else {
			spinIsDisabled = true;
		}

		//Note: Fling only works in single finger mode will need to fix. Not priority.
		if(spinIsDisabled == false && useSingleFinger == true) {

			//If flingTimeThreshold met
			if(currentTime < gestureDownTime + flingTimeThreshold) {

				if(angleToleranceMet == true || angleToleranceMet == true) {

					if(spinAnimationHandler == null) {spinAnimationHandler = new SpinAnimationHandler(this, this.hacerGestoV3);}

					if(spinStartSpeed == 0d && spinEndSpeed == 0d) {
						spinAnimationHandler.spinStartSpeed = (float) (Math.abs(spinStartAngle - angleWrapper.getTextureAngle()) / ((double) (currentTime - gestureDownTime) / 1000d));
					}
					else if(spinStartSpeed != 0d || spinEndSpeed != 0d) {
						spinAnimationHandler.spinStartSpeed = spinStartSpeed;
					}//End if(spinStartSpeed == 0d && spinEndSpeed == 0d)

					spinAnimationHandler.spinEndSpeed = this.spinEndSpeed;

					if(slowDown == true) {

						if(slowFactor == 0) {
							spinAnimationHandler.spinDuration = this.spinDuration;
							this.spinEndTime = spinAnimationHandler.spinEndTime = currentTime + spinDuration;
						}
						else /* if(slowFactor != 0) */ {

							if(spinStartSpeed == 0d && spinEndSpeed == 0d) {
								spinAnimationHandler.spinDuration = (long) (spinAnimationHandler.spinStartSpeed * slowFactor * 1000L);
							}
							else if(spinStartSpeed != 0d || spinEndSpeed != 0d) {
								spinAnimationHandler.spinDuration = (long) (spinStartSpeed * slowFactor * 1000L);
							}//End if(spinStartSpeed == 0d && spinEndSpeed == 0d)

							this.spinEndTime = spinAnimationHandler.spinEndTime = currentTime + spinAnimationHandler.spinDuration;

						}//End if(slowFactor == 0)

					}//End if(slowDown == true)

					if(slowDown == false) {

						this.spinEndTime = spinAnimationHandler.spinEndTime = Long.MAX_VALUE;
						spinAnimationHandler.spinDuration = this.spinEndTime - System.currentTimeMillis();

					}//End if(slowDown == false)

					if(angleToleranceMet == true || distanceToleranceMet == true) {
						hgv3Info.setSpinTriggered(true);
						spinAnimationHandler.spinTriggered = spinTriggered = true;
						spinAnimationHandler.lastTextureDirection = this.lastTextureDirection;
						hgv3Info.setSpinCurrentSpeed((float) spinAnimationHandler.spinStartSpeed);
					}//End if(angleToleranceMet == true || distanceToleranceMet == true)

				}//End if(angleToleranceMet == true || angleToleranceMet == true)

			}
			else /* if(currentTime >= gestureDownTime + flingTimeThreshold) */ {

				hgv3Info.setSpinTriggered(false);
				spinAnimationHandler.spinTriggered = spinTriggered = false;
				spinAnimationHandler.lastTextureDirection = this.lastTextureDirection;
				hgv3Info.setSpinCurrentSpeed(0f);

			}//End if(currentTime < gestureDownTime + flingTimeThreshold)

		}//End if(spinIsDisabled == false && useSingleFinger == true)

	}//End private void setSpinStatus()


	/* Top of block convenience methods */
	void doManualTextureDial(final double manualDial) {doManualGestureDial(manualDial / precisionRotation);}


	void doManualGestureDial(final double manualDial) {

		fullGestureAngle = manualDial;
		angleWrapper.setGestureAngle(manualDial);
		angleWrapper.setTextureAngle(angleWrapper.getTextureAngle());
		hgv3Info.setGestureAngle(angleWrapper.getGestureAngle());
		hgv3Info.setTextureAngle(angleWrapper.getTextureAngle());
		setReturnType();

	}//End public void doManualGestureDial(final double manualDial)


	//When called with the correct values; triggers a fling-to-spin event.
	void triggerSpin(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration, final int lastTextureDirection) {

		this.spinStartSpeedState = this.spinStartSpeed;
		this.spinEndSpeedState = this.spinEndSpeed;
		this.spinDurationState = this.spinDuration;
		this.fullGestureDownAngle = this.fullGestureAngle;
		this.spinStartSpeed = spinStartSpeed;
		this.spinEndSpeed = spinEndSpeed;
		this.spinDuration = spinDuration;
		this.spinEndTime = System.currentTimeMillis() + this.spinDuration;
		this.lastTextureDirection = lastTextureDirection;
		this.spinTriggered = true;
		this.spinTriggeredProgrammatically = true;
		spinAnimationHandler.triggerSpin(spinStartSpeed, spinEndSpeed, spinDuration, lastTextureDirection, this.spinEndTime);

	}//End void triggerSpin(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration, final int lastTextureDirection)
	/* Bottom of block convenience methods */


	//Top of block Accessors
	double getPrecisionRotation() {return this.precisionRotation;}
	double getFullGestureAngle() {return this.fullGestureAngle;}
	boolean getCumulativeRotate() {return this.cumulativeRotate;}
	double getVariableDialInner() {return this.variableDialInner;}
	double getVariableDialOuter() {return this.variableDialOuter;}
	boolean getUseVariableDial() {return this.useVariableDial;}
	boolean getPositiveCurve() {return this.positiveCurve;}
	boolean getIsSingleFinger() {return this.useSingleFinger;}
	int getFlingDistanceThreshold() {return this.flingDistanceThreshold;}
	long getFlingTimeThreshold() {return this.flingTimeThreshold;}
	float getSpinStartSpeed() {return this.spinStartSpeed;}
	float getSpinEndSpeed() {return this.spinEndSpeed;}
	long getSpinDuration() {return this.spinDuration;}
	boolean getSlowSpinOnCurve() {return this.slowSpinOnCurve;}
	boolean getSlowSpinOnPositiveCurve() {return this.slowSpinOnPositiveCurve;}
	boolean getSpinTriggered() {return this.spinTriggered;}
	int getLastTextureDirection() {return this.lastTextureDirection;}
	//Bottom of block Accessors


	//Top of block Mutators
	void setPrecisionRotation(final double precisionRotation) {
		this.precisionRotation = precisionRotation;
		doManualTextureDial(angleWrapper.getTextureAngle());
	}//End void setPrecisionRotation(final double precisionRotation)

	void setFullGestureAngle(final double fullGestureAngle) {this.fullGestureAngle = fullGestureAngle;}
	void setCumulativeRotate(final boolean cumulativeRotate) {this.cumulativeRotate = cumulativeRotate;}
	void setUseSingleFinger(final boolean useSingleFinger) {this.useSingleFinger = useSingleFinger;}
	void setFlingTolerance(final int flingDistanceThreshold, final long flingTimeThreshold) {this.flingDistanceThreshold = flingDistanceThreshold; this.flingTimeThreshold = flingTimeThreshold;}
	void setSpinAnimation(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration) {this.spinStartSpeed = spinStartSpeed; this.spinEndSpeed = spinEndSpeed; this.spinDuration = spinDuration;}
	void setSlowSpinOnCurve(final boolean slowSpinOnCurve, final boolean slowSpinOnPositiveCurve) {this.slowSpinOnCurve = slowSpinOnCurve; this.slowSpinOnPositiveCurve = slowSpinOnPositiveCurve;}
	void setSpinTriggered(final boolean spinTriggered) {this.spinTriggered = spinTriggered;}
	//Bottom of block Mutators


	/* Top of block behavioural methods */
	void cancelSpin() {spinAnimationHandler.spinTriggered = spinTriggered = false;}


	void setMinMaxDial(final double minimumRotation, final double maximumRotation, final boolean useMinMaxRotation) {

		this.minimumRotation = minimumRotation;
		this.maximumRotation = maximumRotation;
		this.useMinMaxRotation = useMinMaxRotation;

		if(useMinMaxRotation == true) {

			if(minimumRotation >= maximumRotation || maximumRotation <= minimumRotation) {

				this.useMinMaxRotation = false;

			}
			else {

				this.useMinMaxRotation = true;

			}//End if(minimumRotation >= maximumRotation || maximumRotation <= minimumRotation)

		}//End if(useMinMaxRotation == true)

		if(this.useMinMaxRotation == false) {minMaxRotationOutOfBounds = 0; hgv3Info.setMinMaxReached(minMaxRotationOutOfBounds);}

	}//End void setMinMaxDial(final double minimumRotation, final double maximumRotation, final boolean useMinMaxRotation)


	void setVariableDial(final double variableDialInner, final double variableDialOuter, final Boolean useVariableDial) {

		this.variableDialInner = variableDialInner;
		this.variableDialOuter = variableDialOuter;
		this.useVariableDial = useVariableDial;

		if(useVariableDial == false) {

			doManualGestureDial(fullGestureAngle);

		}
		else /* if(useVariableDial == true) */ {

			precisionRotation = 1d;

		}//End if(useVariableDial == false)

	}//End void setVariableDial(final double variableDialInner, final double variableDialOuter, final Boolean useVariableDial)


	void setUseVariableDialCurve(final boolean useVariableDialCurve, final boolean positiveCurve) {this.useVariableDialCurve = useVariableDialCurve; this.positiveCurve = positiveCurve;}


	void setAngleSnap(final double angleSnap, final double angleSnapProximity) {

		this.angleSnap = angleSnap;
		this.angleSnapProximity = (angleSnapProximity);
		if(angleSnapProximity > angleSnap / 2d) {this.angleSnapProximity = angleSnap / 2d;}

	}//End void setAngleSnap(final double angleSnap, final double angleSnapProximity)


	void checkAngleSnap() {

		final double tempAngleSnap = Math.round(angleWrapper.getTextureAngle() / angleSnap);

		if(angleWrapper.getTextureAngle() > (tempAngleSnap * angleSnap) - angleSnapProximity && angleWrapper.getTextureAngle() < (tempAngleSnap * angleSnap) + angleSnapProximity) {

			angleSnapNext = tempAngleSnap * angleSnap;
			rotateSnapped = true;

		}
		else {

			angleSnapNext = angleWrapper.getTextureAngle();
			rotateSnapped = false;

		}

		hgv3Info.setRotateSnapped(rotateSnapped);

	}//End private void checkAngleSnap()
	/* Bottom of block behavioural methods */

}