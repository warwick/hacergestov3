/*
LICENSE. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License

In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".

Copyright (c) 2015, Warwick Weston Wright All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HacerGestoV3;

import android.graphics.Point;
import android.view.MotionEvent;

final class ScaleMetrics {

	/*
	Developer note about separation of concerns strategy. This class only ever reads package local fields from the metrics classes (rotateMetrics, moveMetrics and flingMetrics)
	and never write to each other.
	Addendum: This is a new library and the separation of concerns has not been fully checked so their may be some mixed concerns that need fixing.
	*/

	//Top of block separation of concerns objects
	private final HacerGestoV3 hacerGestoV3;
	private final HGV3Info hgv3Info;
	private final HGGeometry hgGeometry;
	//Bottom of block separation of concerns objects

	//Top of block behavioural flags
	private float maxScale;
	private float minScale;
	private boolean useMinMaxScale;
	private int scaleSnapping;
	//Bottom of block behavioural flags

	//Top of block other variables for internal usage
	private float firstTouchX;
	private float firstTouchY;
	private float secondTouchX;
	private float secondTouchY;
	float scale;
	private boolean manuallyScaled;
	private int scaleDirection;//Open End not used. Will be adding a flag indicating direction of scale.
	private int scaledWidth;
	private int scaledHeight;
	private final Point originalDimensions = new Point(0, 0);
	private final Point newDimensions = new Point(0, 0);
	private double touchDistanceOnDown;
	private double touchDistanceOnUp;
	private double touchDistance;
	private double lastDistance;
	private final double[] minMaxCumulativeDistance;
	//Bottom of block other variables for internal usage


	ScaleMetrics(final HacerGestoV3 hacerGestoV3, final HGV3Info hgv3Info, final HGGeometry hgGeometry) {

		//X-Class objects instantiation
		this.hacerGestoV3 = hacerGestoV3;
		this.hgv3Info = hgv3Info;
		this.hgGeometry = hgGeometry;
		this.scale = 1.0f;
		this.useMinMaxScale = false;
		this.scaleSnapping = 0;
		minMaxCumulativeDistance = new double[2];

	}//End ScaleMetrics(final HacerGestoV3 hacerGestoV3, final HGV3Info hgv3Info, final HGGeometry hgGeometry)


	//Top of block main touch Handler
	void setHgv3Info(final MotionEvent event) {

		final int action = event.getAction() & MotionEvent.ACTION_MASK;

		if(action == MotionEvent.ACTION_MOVE) {

			//Prepare touches for single or dual touch
			if(event.getPointerCount() == 2) {
				firstTouchX = hacerGestoV3.firstTouchX;
				firstTouchY = hacerGestoV3.firstTouchY;
				secondTouchX = hacerGestoV3.secondTouchX;
				secondTouchY = hacerGestoV3.secondTouchY;
				lastDistance = touchDistance;

				final boolean outOfBounds = ((useMinMaxScale == true && minMaxCumulativeDistance != null) &&  (touchDistance < minMaxCumulativeDistance[0] || touchDistance > minMaxCumulativeDistance[1]));

				if(outOfBounds == true) {
					if(touchDistance < minMaxCumulativeDistance[0] && scaleDirection == 1) {
						touchDistance = minMaxCumulativeDistance[0];
						touchDistanceOnDown = hgGeometry.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) - touchDistance;
					}
					else if(touchDistance > minMaxCumulativeDistance[1] && scaleDirection == -1) {
						touchDistance = minMaxCumulativeDistance[1];
						touchDistanceOnDown = hgGeometry.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) - touchDistance;
					}

				}

				touchDistance = hgGeometry.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) - touchDistanceOnDown;
				setReturnType();

			}

		}
		else if(action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_POINTER_DOWN) {

			//Prepare touches for single or dual touch
			if(action == MotionEvent.ACTION_DOWN) {
				if(manuallyScaled == true && (newDimensions.x == 0 || newDimensions.y == 0)) {
					newDimensions.x = (int) ((float) originalDimensions.x * scale);
					newDimensions.y = (int) ((float) originalDimensions.y * scale);
				}
				else if(manuallyScaled == false) {
					newDimensions.x = originalDimensions.x;
					newDimensions.y = originalDimensions.y;
				}//End if(manuallyScaled == true && (newDimensions.x == 0 || newDimensions.y == 0))

				if(minMaxCumulativeDistance != null) {

					if(scale == minScale && touchDistance < minMaxCumulativeDistance[0]) {
						touchDistance = minMaxCumulativeDistance[0];
						touchDistanceOnUp = touchDistance;
						touchDistanceOnDown = touchDistance;
					}
					else if(scale == maxScale && touchDistance > minMaxCumulativeDistance[1]) {
						touchDistance = minMaxCumulativeDistance[1];
						touchDistanceOnUp = touchDistance;
						touchDistanceOnDown = touchDistance;
					}

				}//End if(minMaxCumulativeDistance == null)

			}
			else if(action == MotionEvent.ACTION_POINTER_DOWN) {
				firstTouchX = hacerGestoV3.firstTouchX;
				firstTouchY = hacerGestoV3.firstTouchY;
				secondTouchX = hacerGestoV3.secondTouchX;
				secondTouchY = hacerGestoV3.secondTouchY;
				touchDistanceOnDown = hgGeometry.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) - touchDistanceOnUp;
			}//End if(action == MotionEvent.ACTION_DOWN)

		}
		else if(action == MotionEvent.ACTION_POINTER_UP || action == MotionEvent.ACTION_UP) {

			scaleDirection = 0;
			//Prepare touches for single or dual touch
			if(action == MotionEvent.ACTION_POINTER_UP) {
				touchDistanceOnUp = hgGeometry.getTwoFingerDistance(firstTouchX, firstTouchY, secondTouchX, secondTouchY) - touchDistanceOnDown;
			}//End if(action == MotionEvent.ACTION_POINTER_UP)

			hgv3Info.setTouchDistance(0);

		}//End if(action == MotionEvent.ACTION_MOVE)

	}//End void setHgv3Info(final MotionEvent event)
	//Bottom of block main touch Handler


	//Subscript 0 is minimum and 1 maximum
	private double[] getMinMaxCumulativeDistance(final float minScale, final float maxScale) {

		if(useMinMaxScale == true) {

			minMaxCumulativeDistance[0] = (minScale * originalDimensions.x) - (scale * originalDimensions.x);
			minMaxCumulativeDistance[1] = (maxScale * originalDimensions.x) - (scale * originalDimensions.x);

			return minMaxCumulativeDistance;
		}
		else if(useMinMaxScale == false) {
			return null;
		}//End if(useMinMaxScale == true)

		return null;

	}//End private double[] getMinMaxCumulativeDistance()


	//Sets the return type (HGV3Info) accessible in the main callback
	private void setReturnType() {

		if(touchDistance > lastDistance) {
			scaleDirection = 1;
		}
		else if(touchDistance < lastDistance) {
			scaleDirection = -1;
		}

		if(scaleSnapping == 0) {

			scaledWidth = newDimensions.x + (int) touchDistance;
			scaledHeight = (int) ((float) scaledWidth * ((float) originalDimensions.y / (float) originalDimensions.x));
			scale = (float) scaledWidth / (float) originalDimensions.x;

			if(useMinMaxScale == true) {

				if(scale > maxScale) {
					scale = maxScale;
				}
				else if(scale < minScale) {
					scale = minScale;
				}

			}

		}
		else if(scaleSnapping != 0) {
			checkScaleSnapping();
		}//End if(scaleSnapping == 0)

		hgv3Info.setScaleDirection(scaleDirection);
		hgv3Info.setTouchDistance(touchDistance);
		hgv3Info.setScale(scale);

	}//End private void setReturnType()


	private void checkScaleSnapping() {

		scaledWidth = newDimensions.x + (int) touchDistance;
		scaledHeight = (int) ((float) scaledWidth * ((float) originalDimensions.y / (float) originalDimensions.x));
		final int scaleProportion = scaledWidth / scaleSnapping;
		scale = (scaleProportion * scaleSnapping) / (float) originalDimensions.x;

		if(useMinMaxScale == true) {

			if(scale > maxScale) {
				scale = maxScale;
			}
			else if(scale < minScale) {
				scale = minScale;
			}

		}//End if(useMinMaxScale == true)

	}//End private void checkScaleSnapping()


	//Accessors
	boolean getUseMinMaxScale() {return this.useMinMaxScale;}
	float getMinScale() {return this.minScale;}
	float getMaxScale() {return this.maxScale;}
	int getScaleSnapping() {return this.scaleSnapping;}


	//Mutators
	void setUseMinMaxScale(final float minScale, final float maxScale, final boolean useMinMaxScale) {
		this.minScale = minScale;
		this.maxScale = maxScale;
		this.useMinMaxScale = useMinMaxScale;
	}//End void setUseMinMaxScale(final float minScale, final float maxScale, final boolean useMinMaxScale)


	void setScaleSnapping(final int scaleSnapping) {this.scaleSnapping = scaleSnapping;}


	void setOriginalDimensions(final Point originalDimensions) {
		this.originalDimensions.x = originalDimensions.x;
		this.originalDimensions.y = originalDimensions.y;

		if(useMinMaxScale == true) {
			final double[] minMaxCumulativeTemp = getMinMaxCumulativeDistance(minScale, maxScale);
			if(minMaxCumulativeTemp != null) {
				minMaxCumulativeDistance[0] = minMaxCumulativeTemp[0];
				minMaxCumulativeDistance[1] = minMaxCumulativeTemp[1];
			}
		}

	}

	public void doManualScale(final float scale) {
		this.manuallyScaled = true;
		this.scale = scale;
		this.scaledWidth = (int) ((float) this.originalDimensions.x * scale);
		this.scaledHeight = (int) ((float) this.originalDimensions.y * scale);
		this.newDimensions.x = this.scaledWidth;
		this.newDimensions.y = this.scaledHeight;
		hacerGestoV3.invalidate();
	}//End public void doManualScale(final float scale)
	/* Bottom of block convenience methods */

}