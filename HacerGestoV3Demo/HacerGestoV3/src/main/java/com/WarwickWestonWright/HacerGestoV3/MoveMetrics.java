/*
LICENSE. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License

In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".

Copyright (c) 2015, Warwick Weston Wright All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HacerGestoV3;

import android.graphics.Point;
import android.view.MotionEvent;

import static android.view.MotionEvent.ACTION_DOWN;
import static android.view.MotionEvent.ACTION_POINTER_UP;
import static android.view.MotionEvent.ACTION_UP;

final class MoveMetrics {

	/*
	Developer note about separation of concerns strategy. This class only ever reads package local fields from the metrics classes (rotateMetrics, scaleMetrics and flingMetrics)
	and never write to each other.
	Addendum: This is a new library and the separation of concerns has not been fully checked so their may be some mixed concerns that need fixing.
	*/

	//Top of block separation of concerns objects

	private final HacerGestoV3 hacerGestoV3;
	final HGV3Info hgv3Info;
	private final HGGeometry hgGeometry;
	final MoveConstraintObj moveConstraintObj = new MoveConstraintObj();
	//Bottom of block separation of concerns objects

	//Top of block behavioural flags
	boolean cumulativeMove;
	int lConstraint;
	int tConstraint;
	int rConstraint;
	int bConstraint;
	private boolean moveOffEdge;
	private boolean moveToEdge;
	private boolean useMoveConstraint;
	boolean zoomCenterCrop;
	private boolean zoomWithRotation;
	boolean usePanZoomMode;
	private int moveDirection;
	private float firstTouchXState;//Used to detect direction of movement will be implemented in callback when full tested
	private float firstTouchYState;//Used to detect direction of movement will be implemented in callback when full tested
	Point[] moveSnapping = null;
	private int[] moveSnappingTolerance;
	int snapIdx;
	boolean hasSnapped;
	//Bottom of block behavioural flags

	//Top of block other variables for internal usage
	private float firstTouchX;
	private float firstTouchY;
	private float secondTouchX;
	private float secondTouchY;
	private float downTouchX;
	private float downTouchY;
	private float xTouchDistanceHandle;
	private float yTouchDistanceHandle;
	private float upTouchX;
	private float upTouchY;
	float xMove;
	float yMove;
	private int moveSnappingLength;
	private int pointerCount;
	private int checkOutOfBounds;
	private boolean touchSwap;
	private boolean touchSwapBack;
	//Bottom of block other variables for internal usage


	MoveMetrics(final HacerGestoV3 hacerGestoV3, final HGV3Info hgv3Info, final HGGeometry hgGeometry) {

		//X-Class objects instantiation
		this.hacerGestoV3 = hacerGestoV3;
		this.hgv3Info = hgv3Info;
		this.hgGeometry = hgGeometry;
		this.cumulativeMove = true;
		this.lConstraint = 0;
		this.tConstraint = 0;
		this.rConstraint = 0;
		this.bConstraint = 0;
		this.useMoveConstraint = false;
		this.moveDirection = 0;//Zero for not moving
		this.firstTouchXState = hacerGestoV3.firstTouchX;
		this.firstTouchYState = hacerGestoV3.firstTouchY;
		this.xMove = 0f;
		this.yMove = 0f;
		this.moveSnappingLength = 0;
		this.touchSwap = false;
		this.touchSwapBack = false;

	}//End MoveMetrics(final HacerGestoV3 hacerGestoV3, final HGV3Info hgv3Info, final HGGeometry hgGeometry)


	//Top of block main touch Handler
	void setHgv3Info(final MotionEvent event) {

		final int actionIdx = event.getAction() & MotionEvent.ACTION_MASK;

		switch(actionIdx) {
			case ACTION_DOWN:
				downOne();
				break;

			case MotionEvent.ACTION_POINTER_DOWN:
				downTwo();
				break;

			case ACTION_POINTER_UP:
				upOne(event.getActionIndex());
				break;

			case ACTION_UP:
				upTwo();
				break;

			case MotionEvent.ACTION_MOVE:
				setMove();
				break;
		}

	}//End void setHgv3Info(final MotionEvent event)
	//Bottom of block main touch Handler

	private void downOne() {
		pointerCount = 1;

		if(touchSwap == true) {
			if(cumulativeMove == true) {
				downTouchX = secondTouchX = xTouchDistanceHandle = hacerGestoV3.firstTouchX - (upTouchX - downTouchX);
				downTouchY = secondTouchY = yTouchDistanceHandle = hacerGestoV3.firstTouchY - (upTouchY - downTouchY);
			}
			else /* if(cumulativeMove == false) */ {
				firstTouchX = hacerGestoV3.secondTouchX;
				firstTouchY = hacerGestoV3.secondTouchY;
				setReturnType();
			}//End if(cumulativeMove == true)
		}
		else /* if(touchSwap == false) */ {
			if(cumulativeMove == true) {
				downTouchX = firstTouchX = xTouchDistanceHandle = hacerGestoV3.firstTouchX - (upTouchX - downTouchX);
				downTouchY = firstTouchY = yTouchDistanceHandle = hacerGestoV3.firstTouchY - (upTouchY - downTouchY);
			}
			else /* if(cumulativeMove == false) */ {
				firstTouchX = hacerGestoV3.firstTouchX;
				firstTouchY = hacerGestoV3.firstTouchY;
				setReturnType();
			}//End if(cumulativeMove == true)
		}//End if(touchSwap == true)

	}

	private void downTwo() {
		pointerCount = 2;

		if(touchSwap == true) {

			touchSwapBack = true;

			if(cumulativeMove == true) {
				downTouchX -= ((secondTouchX = hacerGestoV3.secondTouchX - (firstTouchX = hacerGestoV3.firstTouchX)) / 2);
				downTouchY -= ((secondTouchY = hacerGestoV3.secondTouchY - (firstTouchY = hacerGestoV3.firstTouchY)) / 2);
			}

		}
		else /* if(touchSwap == false) */ {

			if(cumulativeMove == true) {
				downTouchX -= ((firstTouchX = hacerGestoV3.firstTouchX - (secondTouchX = hacerGestoV3.secondTouchX)) / 2);
				downTouchY -= ((firstTouchY = hacerGestoV3.firstTouchY - (secondTouchY = hacerGestoV3.secondTouchY)) / 2);
			}

		}//End if(touchSwap == true)

	}

	private synchronized void upOne(final int actionIdx) {
		pointerCount = 1;

		if(cumulativeMove == true) {

			if(actionIdx == 1) {
				touchSwap = false;
				downTouchX += (((firstTouchX = hacerGestoV3.firstTouchX) - secondTouchX) / 2);
				downTouchY += (((firstTouchY = hacerGestoV3.firstTouchY) - secondTouchY) / 2);
			}
			else if(actionIdx == 0) {
				touchSwap = true;
				downTouchX -= (((firstTouchX = hacerGestoV3.firstTouchX) - secondTouchX) / 2);
				downTouchY -= (((firstTouchY = hacerGestoV3.firstTouchY) - secondTouchY) / 2);
			}

		}
		else /* if(cumulativeMove == false) */ {

			if(actionIdx == 1) {
				touchSwap = false;
				firstTouchX = hacerGestoV3.firstTouchX;
				firstTouchY = hacerGestoV3.firstTouchY;
				secondTouchX = hacerGestoV3.secondTouchX;
				secondTouchY = hacerGestoV3.secondTouchY;

			}
			else if(actionIdx == 0) {
				touchSwap = true;
				firstTouchX = hacerGestoV3.secondTouchX;
				firstTouchY = hacerGestoV3.secondTouchY;
				secondTouchX = hacerGestoV3.firstTouchX;
				secondTouchY = hacerGestoV3.firstTouchY;

			}

		}//End if(cumulativeMove == true)

	}

	private synchronized void upTwo() {
		pointerCount = 1;
		moveDirection = 0;

		if(touchSwap == true && touchSwapBack == false) {
			touchSwap = false;
		}
		else if(touchSwap == false && touchSwapBack == true) {
			touchSwapBack = false;
		}
		else if(touchSwap == true && touchSwapBack == true) {
			touchSwap = false;
			touchSwapBack = false;
		}

		firstTouchX = hacerGestoV3.firstTouchX;
		firstTouchY = hacerGestoV3.firstTouchY;

		if(checkOutOfBounds != 0) {
			upTouchX = firstTouchX - (((firstTouchX - upTouchX) - downTouchX));
			upTouchY = firstTouchY - (((firstTouchY - upTouchY) - downTouchY));
			checkOutOfBounds = 0;
		}
		else {
			upTouchX = firstTouchX;
			upTouchY = firstTouchY;
		}

	}

	private void setMove() {

		//Prepare touches for single or dual touch
		firstTouchX = hacerGestoV3.firstTouchX;
		firstTouchY = hacerGestoV3.firstTouchY;
		secondTouchX = hacerGestoV3.secondTouchX;
		secondTouchY = hacerGestoV3.secondTouchY;
		setReturnType();
		if(usePanZoomMode == true || moveOffEdge != moveToEdge) {setDynamicBounds();}
		if(usePanZoomMode == true || useMoveConstraint == true) {checkOutOfBounds = checkOutOfBounds();}
		setMoveDirection(firstTouchX, firstTouchY);
		//Save last touch state for direction detection
		firstTouchXState = firstTouchX;
		firstTouchYState = firstTouchY;
	}

	/* Top of block behavioural methods */
	private void setReturnType() {

		if(cumulativeMove == true) {

			if(pointerCount < 2) {
				xMove = firstTouchX - downTouchX;
				yMove = firstTouchY - downTouchY;
			}
			else {
				xMove = ((firstTouchX - downTouchX) - (firstTouchX - xTouchDistanceHandle) / 2) + ((hacerGestoV3.secondTouchX - xTouchDistanceHandle) / 2);
				yMove = ((firstTouchY - downTouchY) - (firstTouchY - yTouchDistanceHandle) / 2) + ((hacerGestoV3.secondTouchY - yTouchDistanceHandle) / 2);
			}
		}
		else {
			xMove = firstTouchX;
			yMove = firstTouchY;
		}//End if(cumulativeMove == true)

		if(moveSnapping != null) {
			hgv3Info.setMoveSnapped(hasSnapped = checkMoveSnapping(), snapIdx);
		}

		if(hasSnapped == false) {
			if(cumulativeMove == true) {
				hgv3Info.setCurrentPosition(xMove, yMove);
			}
			else if(cumulativeMove == false) {
				hgv3Info.setCurrentPosition(firstTouchX - hacerGestoV3.viewWidthOver2, firstTouchY - hacerGestoV3.viewHeightOver2);
			}//End if(cumulativeMove == true)
		}
		else /* if(hasSnapped == true) */ {
			if(moveSnapping != null) {hgv3Info.setCurrentPosition(moveSnapping[snapIdx].x, moveSnapping[snapIdx].y);}
		}//Ene if(hasSnapped == false)

		if(hasSnapped == true) {
			xMove = moveSnapping[snapIdx].x;
			yMove = moveSnapping[snapIdx].y;
		}

	}//End private void setReturnType()


	private void setMoveDirection(final float xMoveState, final float yMoveState) {

		if(firstTouchXState > xMoveState) {
			//Moving Left
			moveDirection |= (1 << 1);
			moveDirection &= ~(1 << 3);
		}
		else if(firstTouchXState < xMoveState) {
			//Moving Right
			moveDirection |= (1 << 3);
			moveDirection &= ~(1 << 1);
		}
		else {
			//Not Moving Left or Right
			moveDirection &= ~(1 << 1);
			moveDirection &= ~(1 << 3);
		}//End if(firstTouchXState > xMoveState)

		if(firstTouchYState > yMoveState) {
			//Moving up
			moveDirection |= (1 << 2);
			moveDirection &= ~(1 << 4);
		}
		else if(firstTouchYState < yMoveState) {
			//Moving down
			moveDirection |= (1 << 4);
			moveDirection &= ~(1 << 2);
		}
		else {
			//Not Moving Up or Down
			moveDirection &= ~(1 << 4);
			moveDirection &= ~(1 << 2);
		}//End if(firstTouchYState > yMoveState)

		hgv3Info.setMoveDirection(moveDirection);

	}//End private void setMoveDirection(final float xMoveState, final float yMoveState)


	private boolean checkMoveSnapping() {

		final int xPos;
		final int yPos;
		xPos = (int) xMove;
		yPos = (int) yMove;

		for(int i = 0; i < moveSnappingLength; i++) {

			if(cumulativeMove == true) {

				if(hgGeometry.difference(xPos, moveSnapping[i].x) < moveSnappingTolerance[i] && hgGeometry.difference(yPos, moveSnapping[i].y) < moveSnappingTolerance[i]) {
					snapIdx = i;
					return true;
				}

			}
			else {

				if((hgGeometry.difference(xPos - hacerGestoV3.viewWidth / 2, moveSnapping[i].x)) < moveSnappingTolerance[i] && (hgGeometry.difference(yPos - hacerGestoV3.viewHeight / 2, moveSnapping[i].y)) < moveSnappingTolerance[i]) {
					snapIdx = i;
					return true;
				}

			}//End if(cumulativeMove == true)

		}

		return false;

	}//End private boolean checkMoveSnapping()


	private int checkOutOfBounds() {

		final float[] currentPos = hgv3Info.getCurrentPosition();
		int returnVal = 0;

		if(currentPos[0] < lConstraint) {
			//Out of Bounds to the Left
			returnVal |= (1 << 1);
			returnVal &= ~(1 << 3);
		}
		else if(currentPos[0] > rConstraint) {
			//Out of Bounds to the Right
			returnVal |= (1 << 3);
			returnVal &= ~(1 << 1);
		}
		else {
			//In Bounds to the Left and Right
			returnVal &= ~(1 << 1);
			returnVal &= ~(1 << 3);
		}//End if(firstTouchXState > xMoveState)

		if(currentPos[1] < tConstraint) {
			//Out of Bounds to the Top
			returnVal |= (1 << 2);
			returnVal &= ~(1 << 4);
		}
		else if(currentPos[1] > bConstraint) {
			//Out of Bounds to the Bottom
			returnVal |= (1 << 4);
			returnVal &= ~(1 << 2);
		}
		else {
			//In Bounds to the Top and Bottom
			returnVal &= ~(1 << 4);
			returnVal &= ~(1 << 2);
		}//End if(firstTouchYState > yMoveState)

		if(cumulativeMove == true) {

			if(returnVal != 0) {
				if(currentPos[0] < lConstraint) {constrainMovement(new Point(lConstraint, (int) yMove));}
				if(currentPos[1] < tConstraint) {constrainMovement(new Point((int) xMove, tConstraint));}
				if(currentPos[0] > rConstraint) {constrainMovement(new Point(rConstraint, (int) yMove));}
				if(currentPos[1] > bConstraint) {constrainMovement(new Point((int) xMove, bConstraint));}
			}

		}
		else /* if(cumulativeMove == false) */ {

			if(returnVal != 0) {

				//Check Corners TL TR BR BL
				if((returnVal & (moveConstraintObj.movingU | moveConstraintObj.movingL)) == (moveConstraintObj.movingU | moveConstraintObj.movingL)) {
					xMove = lConstraint; yMove = tConstraint;
				}
				else if((returnVal & (moveConstraintObj.movingU | moveConstraintObj.movingR)) == (moveConstraintObj.movingU | moveConstraintObj.movingR)) {
					xMove = rConstraint; yMove = tConstraint;
				}
				else if((returnVal & (moveConstraintObj.movingD | moveConstraintObj.movingR)) == (moveConstraintObj.movingD | moveConstraintObj.movingR)) {
					xMove = rConstraint; yMove = bConstraint;
				}
				else if((returnVal & (moveConstraintObj.movingD | moveConstraintObj.movingL)) == (moveConstraintObj.movingD | moveConstraintObj.movingL)) {
					xMove = lConstraint; yMove = bConstraint;
				}
				//Check Edges LTRB
				else if((returnVal & moveConstraintObj.movingL) == moveConstraintObj.movingL) {
					xMove = lConstraint; yMove = firstTouchY - hacerGestoV3.viewHeightOver2;
				}
				else if((returnVal & moveConstraintObj.movingU) == moveConstraintObj.movingU) {
					yMove = tConstraint; xMove = firstTouchX - hacerGestoV3.viewWidthOver2;
				}
				else if((returnVal & moveConstraintObj.movingR) == moveConstraintObj.movingR) {
					xMove = rConstraint; yMove = firstTouchY - hacerGestoV3.viewHeightOver2;
				}
				else if((returnVal & moveConstraintObj.movingD) == moveConstraintObj.movingD) {
					yMove = bConstraint; xMove = firstTouchX - hacerGestoV3.viewWidthOver2;
				}

				constrainMovement(new Point((int) xMove, (int) yMove));
			}//End if((returnVal & (moveConstraintObj.movingU | moveConstraintObj.movingL)) == (moveConstraintObj.movingU | moveConstraintObj.movingL))

		}//End if(cumulativeMove == true)

		return returnVal;

	}//End private int checkOutOfBounds()


	private void constrainMovement(final Point newPosition) {

		if(cumulativeMove == true) {

			downTouchX -= (newPosition.x - xMove);
			downTouchY -= (newPosition.y - yMove);

			setReturnType();
			xMove = newPosition.x;
			yMove = newPosition.y;
			upTouchX = xMove;
			upTouchY = yMove;
		}
		else /* if(cumulativeMove == false) */ {
			firstTouchX = newPosition.x + hacerGestoV3.viewWidthOver2;
			firstTouchY = newPosition.y + hacerGestoV3.viewHeightOver2;
			setReturnType();
		}//End if(cumulativeMove == true)

		hgv3Info.setCurrentPosition(newPosition.x, newPosition.y);

	}//End private void constrainMovement(final Point newPosition)


	void synchroniseFlingMove(final Point newPosition) {

		hgv3Info.setCurrentPosition(newPosition.x, newPosition.y);

		if(cumulativeMove == true) {
			downTouchX -= (newPosition.x - xMove);
			downTouchY -= (newPosition.y - yMove);
			setReturnType();
		}
		else /* if(cumulativeMove == false) */ {
			firstTouchX = newPosition.x + hacerGestoV3.viewWidthOver2;
			firstTouchY = newPosition.y + hacerGestoV3.viewHeightOver2;
			setReturnType();
		}//End if(cumulativeMove == true)

	}//End void synchroniseFlingMove(final Point newPosition)


	private void setDynamicBounds() {
		//This method is used for offEdge/toEdge behaviour
		int outOfBoundsLeftRight = 0;
		int outOfBoundsTopBottom = 0;

		if(usePanZoomMode == true) {

			if(zoomCenterCrop == true) {
				outOfBoundsLeftRight = (hacerGestoV3.contentWidth - hacerGestoV3.viewWidth) / 2;
				outOfBoundsTopBottom = (hacerGestoV3.contentHeight - hacerGestoV3.viewHeight) / 2;
			}
			else {

				if(hacerGestoV3.contentWidth < hacerGestoV3.viewWidth) {
					outOfBoundsLeftRight = -(hacerGestoV3.contentWidth - hacerGestoV3.viewWidth) / 2;
				}
				else {
					outOfBoundsLeftRight = (hacerGestoV3.contentWidth - hacerGestoV3.viewWidth) / 2;
				}

				if(hacerGestoV3.contentHeight < hacerGestoV3.viewHeight) {
					outOfBoundsTopBottom = -(hacerGestoV3.contentHeight - hacerGestoV3.viewHeight) / 2;
				}
				else {
					outOfBoundsTopBottom = (hacerGestoV3.contentHeight - hacerGestoV3.viewHeight) / 2;
				}

			}

		}
		else if(useMoveConstraint == true) {

			if(moveOffEdge == true) {

				if(hacerGestoV3.contentWidth < hacerGestoV3.contentHeight) {
					outOfBoundsLeftRight = hacerGestoV3.viewWidthOver2 + (hacerGestoV3.contentWidth / 2);
					outOfBoundsTopBottom = hacerGestoV3.viewHeightOver2 + (hacerGestoV3.contentHeight / 2);
				}
				else {
					outOfBoundsLeftRight = hacerGestoV3.viewWidthOver2 + (hacerGestoV3.contentWidth / 2);
					outOfBoundsTopBottom = hacerGestoV3.viewHeightOver2 + (hacerGestoV3.contentHeight / 2);
				}//End if(hacerGestoV3.contentWidth < hacerGestoV3.contentHeight)

			}
			else if(moveToEdge == true) {

				if(hacerGestoV3.contentWidth < hacerGestoV3.contentHeight) {
					//Condition for portrait
					outOfBoundsLeftRight = hacerGestoV3.viewWidthOver2 - (hacerGestoV3.contentWidth / 2);
					outOfBoundsTopBottom = hacerGestoV3.viewHeightOver2 - (hacerGestoV3.contentHeight / 2);
				}
				else {
					//Condition for landscape
					outOfBoundsLeftRight = hacerGestoV3.viewWidthOver2 - (hacerGestoV3.contentWidth / 2);
					outOfBoundsTopBottom = hacerGestoV3.viewHeightOver2 - (hacerGestoV3.contentHeight / 2);
				}//End if(hacerGestoV3.contentWidth < hacerGestoV3.contentHeight)

			}//End if(moveOffEdge == true)

		}//End if(usePanZoomMode == true)

		//Draw move boundary inside view
		lConstraint = -outOfBoundsLeftRight;
		tConstraint = -outOfBoundsTopBottom;
		rConstraint = outOfBoundsLeftRight;
		bConstraint = outOfBoundsTopBottom;

	}//End private void setDynamicBounds()
	/* Bottom of block behavioural methods */


	//Accessors
	boolean getMinMaxBounds(int outL, int outT, int outR, int outB) {
		//ToDo: implement move bounds
		if(outL != 0 || outT != 0 || outR != 0 || outB != 0) {
			return true;
		}
		else {
			return false;
		}
	}

	Point[] getMoveSnapping() {return this.moveSnapping;}
	int[] getMoveSnappingTolerance() {return this.moveSnappingTolerance;}
	boolean getCumulativeMove() {return this.cumulativeMove;}
	MoveConstraintObj getMoveConstraintObj() {return this.moveConstraintObj;}
	boolean getZoomCenterCrop() {return zoomCenterCrop;}
	boolean getZoomWithRotation() {return zoomWithRotation;}
	boolean getUsePanZoomMode() {return usePanZoomMode;}

	//Mutators
	void setMoveConstraintObj(final int l, final int t, final int r, final int b, final boolean moveOffEdge, final boolean moveToEdge, final boolean useMoveConstraint) {
		//Set move constraints (l = Left, t = Top, r = Right, b = Bottom). If both offEdge and toEdge are the same value move constraints will be centre centric.
		this.moveConstraintObj.setAll(l, t, r, b, moveOffEdge, moveToEdge, useMoveConstraint);
		this.moveOffEdge = moveOffEdge;
		this.moveToEdge = moveToEdge;

		/*
		Dev Note need to document that moveConstraintObj only returns initial constraints and not dynamic constraints for off and to edge behaviour.
		To get dynamic constraints client developer will need to get handle to this by a combination of width and position.
		*/
		if(moveOffEdge == moveToEdge) {
			this.lConstraint = l;
			this.tConstraint = t;
			this.rConstraint = r;
			this.bConstraint = b;
		}
		else /* if(moveOffEdge != moveToEdge) */ {
			setDynamicBounds();
		}
		this.useMoveConstraint = useMoveConstraint;
		checkOutOfBounds = checkOutOfBounds();
	}//End void setMoveConstraintObj(final int l, final int t, final int r, final int b, final boolean offEdge, final boolean toEdge, final boolean useMoveConstraint)


	void setCumulativeMove(boolean cumulativeMove) {this.cumulativeMove = cumulativeMove;}


	void setMoveSnapping(final Point[] moveSnapping, final int[] moveSnappingTolerance) {
		this.moveSnapping = moveSnapping;
		this.moveSnappingTolerance = moveSnappingTolerance;
		if(moveSnapping != null) {
			this.moveSnappingLength = moveSnappingTolerance.length;
		}
		else /* if(moveSnapping == null) */ {
			this.moveSnappingLength = 0;
			this.hasSnapped = false;
		}//End if(moveSnapping != null)
	}//End void setMoveSnapping(final Point[] moveSnapping, final int[] moveSnappingTolerance)


	void setPanZoomMode(final boolean zoomCenterCrop, final boolean zoomWithRotation, final boolean usePanZoomMode) {
		this.zoomCenterCrop = zoomCenterCrop;
		this.zoomWithRotation = zoomWithRotation;
		this.usePanZoomMode = usePanZoomMode;
	}//End void setPanZoomMode(final boolean zoomCenterCrop, final boolean zoomWithRotation, final boolean usePanZoomMode)


	/* Top of block convenience methods */
	void doManualMove(final Point newPosition) {

		hgv3Info.setCurrentPosition(newPosition.x, newPosition.y);

		if(cumulativeMove == true) {
			setReturnType();
			xMove = newPosition.x;
			yMove = newPosition.y;
			upTouchX = xMove;
			upTouchY = yMove;
		}
		else /* if(cumulativeMove == false) */ {
			firstTouchX = newPosition.x + (hacerGestoV3.viewWidthOver2);
			firstTouchY = newPosition.y + (hacerGestoV3.viewHeightOver2);
			setReturnType();
		}//End if(cumulativeMove == true)

		hacerGestoV3.invalidate();
	}//End void doManualMove(final Point newPosition)
	/* Bottom of block convenience methods */

	public final class MoveConstraintObj {

		public int l = 0;
		public int t = 0;
		public int r = 0;
		public int b = 0;
		public boolean offEdge = false;
		public boolean toEdge = false;
		public boolean useMoveConstraint = false;
		public final int movingL = 2;
		public final int movingU = 4;
		public final int movingR = 8;
		public final int movingD = 16;

		void setAll(final int l, final int t, final int r, final int b, final boolean offEdge, final boolean toEdge, final boolean useMoveConstraint) {
			this.l = l;
			this.t = t;
			this.r = r;
			this.b = b;
			this.offEdge = offEdge;
			this.toEdge = toEdge;
			this.useMoveConstraint = useMoveConstraint;
		}
	}//End public final class MoveConstraintObj

}