/*
LICENSE. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License

In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".

Copyright (c) 2015, Warwick Weston Wright All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HacerGestoV3;

import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.MotionEvent;

final class FlingMetrics {

	/*
	Developer note about separation of concerns strategy. This class only ever reads package local fields from the metrics classes (rotateMetrics, scaleMetrics and moveMetrics)
	and never write to each other.
	Addendum: This is a new library and the separation of concerns has not been fully checked so their may be some mixed concerns that need fixing.
	*/

	//Top of block separation of concerns objects
	private final HacerGestoV3 hacerGestoV3;
	private final HGV3Info hgv3Info;
	private final HGGeometry hgGeometry;
	private final MoveMetrics moveMetrics;
	final MoveAnimationHandler moveAnimationHandler;
	//Bottom of block separation of concerns objects

	//Top of block behavioural flags
	private boolean flingOffEdge;
	private boolean flingToCorner;
	private boolean flingToEdge;
	boolean flingSlideToCorner;
	private int flingDistance;
	private long flingToMoveTime;
	long flingToMoveAnimation;
	//Bottom of block behavioural flags

	//Top of block other variables for internal usage
	private final Point downPoint = new Point(0, 0);
	private final Point upPoint = new Point(0, 0);
	private float downTouchX, downTouchY, upTouchX, upTouchY;//Used to measure touch distance
	private long touchDownTime;
	private boolean outOfBounds;
	private final Rect flingBoundary = new Rect();
	private final RectF flingRatioRect = new RectF();
	private final RectF downPointToBoundaryRatio = new RectF();
	private double flingRatio;
	long downUpSpeedHandle;
	long distanceSpeedHandle;
	boolean useDynamicAnimation;
	//Bottom of block other variables for internal usage


	FlingMetrics(final HacerGestoV3 hacerGestoV3, final HGV3Info hgv3Info, final HGGeometry hgGeometry, final MoveMetrics moveMetrics) {

		//X-Class objects instantiation
		this.hacerGestoV3 = hacerGestoV3;
		this.hgv3Info = hgv3Info;
		this.hgGeometry = hgGeometry;
		this.moveMetrics = moveMetrics;
		this.moveAnimationHandler = new MoveAnimationHandler(this, moveMetrics, hacerGestoV3);
		this.moveAnimationHandler.flingTriggered = false;
		this.flingDistance = 150;
		this.flingToMoveTime = 250L;
		this.flingToMoveAnimation = 650L;
		this.flingOffEdge = true;
		this.flingToCorner = false;
		this.flingToEdge = false;
		this.touchDownTime = 0;
		this.outOfBounds = false;
		this.flingRatio = 0f;
		this.moveMetrics.xMove = 0;
		this.moveMetrics.yMove = 0;

	}//End FlingMetrics(final HacerGestoV3 hacerGestoV3, final HGV3Info hgv3Info, final HGGeometry hgGeometry, final MoveMetrics moveMetrics)


	//Top of block main touch Handler
	void setHgv3Info(final MotionEvent event) {

		final int action = event.getAction() & MotionEvent.ACTION_MASK;

		if(action == MotionEvent.ACTION_DOWN) {

			hgv3Info.setFlingTriggered(false);
			touchDownTime = System.currentTimeMillis();

			if(moveMetrics.cumulativeMove == true) {
				downPoint.x = (int) hacerGestoV3.moveMetrics.xMove;
				downPoint.y = (int) hacerGestoV3.moveMetrics.yMove;
			}
			else {
				downPoint.x = (int) hacerGestoV3.firstTouchX - (hacerGestoV3.viewWidthOver2);
				downPoint.y = (int) hacerGestoV3.firstTouchY - (hacerGestoV3.viewHeightOver2);
			}//End if(moveMetrics.cumulativeMove == true)

			downTouchX = hacerGestoV3.firstTouchX;
			downTouchY = hacerGestoV3.firstTouchY;

		}
		else if(action == MotionEvent.ACTION_UP) {

			if(hacerGestoV3.secondFingerWasDown == true) {return;}

			upTouchX = hacerGestoV3.firstTouchX;
			upTouchY = hacerGestoV3.firstTouchY;

			if(hacerGestoV3.moveMetrics.cumulativeMove == true) {
				moveAnimationHandler.startPosition.x = upPoint.x = (int) hacerGestoV3.moveMetrics.xMove;
				moveAnimationHandler.startPosition.y = upPoint.y = (int) hacerGestoV3.moveMetrics.yMove;
			}
			else {
				moveAnimationHandler.startPosition.x = upPoint.x = (int) hacerGestoV3.firstTouchX - (hacerGestoV3.viewWidthOver2);
				moveAnimationHandler.startPosition.y = upPoint.y = (int) hacerGestoV3.firstTouchY - (hacerGestoV3.viewHeightOver2);
			}//End if(hacerGestoV3.moveMetrics.cumulativeMove == true)

			long currentTime = System.currentTimeMillis();
			downUpSpeedHandle = currentTime - touchDownTime;

			if(currentTime < touchDownTime + flingToMoveTime) {

				if((distanceSpeedHandle = (long) hgGeometry.getTwoFingerDistance((double) downTouchX, (double) downTouchY, (double) upTouchX, (double) upTouchY)) > flingDistance) {

					moveAnimationHandler.flingTriggered = true;
					hgv3Info.setFlingTriggered(true);
					outOfBounds = getIsOutOfBounds(hacerGestoV3.moveMetrics.usePanZoomMode);

					if(outOfBounds == false) {
						setReturnType(hacerGestoV3.moveMetrics.usePanZoomMode);

						if(useDynamicAnimation == true) {
							final double distanceToEnd = hgGeometry.getTwoFingerDistance(moveAnimationHandler.startPosition.x, moveAnimationHandler.startPosition.y,
								moveAnimationHandler.endPosition.x, moveAnimationHandler.endPosition.y);
							final float pixelsPerMilliSecond = (float) ((double) distanceSpeedHandle / (double) downUpSpeedHandle);
							flingToMoveAnimation = (long) (distanceToEnd / pixelsPerMilliSecond);
							moveAnimationHandler.moveEndTime = currentTime + flingToMoveAnimation;
						}
						else /* if(useDynamicAnimation == false) */ {
							moveAnimationHandler.moveEndTime = currentTime + flingToMoveAnimation;
						}

						moveAnimationHandler.startMoveAnim();
					}

				}

			}//End if(currentTime < touchDownTime + flingToMoveTime)

		}//End if(action == MotionEvent.ACTION_DOWN)

	}//End void setHgv3Info(final MotionEvent event)
	//Bottom of block main touch Handler


	//Sets the return type (HGV3Info) accessible in the main callback
	private boolean getIsOutOfBounds(final boolean usePanZoomMode) {

		//Calculate outOfBounds if image already off screen. When off screen cancel fling// Tested
		int outOfBoundsLeftRight = 0;
		int outOfBoundsTopBottom = 0;
		boolean outOfBounds = false;

		if(usePanZoomMode == false) {

			if(flingOffEdge == true) {

				if(hacerGestoV3.contentWidth < hacerGestoV3.contentHeight) {
					outOfBoundsLeftRight = hacerGestoV3.viewWidthOver2 + (hacerGestoV3.contentWidth / 2);
					outOfBoundsTopBottom = hacerGestoV3.viewHeightOver2 + (hacerGestoV3.contentHeight / 2);
				}
				else {
					outOfBoundsLeftRight = hacerGestoV3.viewWidthOver2 + (hacerGestoV3.contentWidth / 2);
					outOfBoundsTopBottom = hacerGestoV3.viewHeightOver2 + (hacerGestoV3.contentHeight / 2);
				}//End if(hacerGestoV3.contentWidth < hacerGestoV3.contentHeight)

			}
			else /* if(flingOffEdge == false) */ {

				if(hacerGestoV3.contentWidth < hacerGestoV3.contentHeight) {
					//Condition for portrait
					outOfBoundsLeftRight = hacerGestoV3.viewWidthOver2 - (hacerGestoV3.contentWidth / 2);
					outOfBoundsTopBottom = hacerGestoV3.viewHeightOver2 - (hacerGestoV3.contentHeight / 2);
				}
				else {
					//Condition for landscape
					outOfBoundsLeftRight = hacerGestoV3.viewWidthOver2 - (hacerGestoV3.contentWidth / 2);
					outOfBoundsTopBottom = hacerGestoV3.viewHeightOver2 - (hacerGestoV3.contentHeight / 2);
				}//End if(hacerGestoV3.contentWidth < hacerGestoV3.contentHeight)

			}//End if(flingOffEdge == true)

			//Draw move boundary inside view
			flingBoundary.left = -outOfBoundsLeftRight;
			flingBoundary.top = -outOfBoundsTopBottom;
			flingBoundary.right = outOfBoundsLeftRight;
			flingBoundary.bottom = outOfBoundsTopBottom;

		}
		else if(usePanZoomMode == true) {

			outOfBoundsLeftRight = moveMetrics.rConstraint;
			outOfBoundsTopBottom = moveMetrics.bConstraint;

			//Draw move boundary inside view
			flingBoundary.left = -outOfBoundsLeftRight;
			flingBoundary.top = -outOfBoundsTopBottom;
			flingBoundary.right = outOfBoundsLeftRight;
			flingBoundary.bottom = outOfBoundsTopBottom;

		}//End if(usePanZoomMode == false)

		if(moveMetrics.cumulativeMove == true) {

			if(hacerGestoV3.moveMetrics.xMove > outOfBoundsLeftRight || hacerGestoV3.moveMetrics.xMove < -outOfBoundsLeftRight) {
				outOfBounds = true;
			}
			else if(hacerGestoV3.moveMetrics.yMove > outOfBoundsTopBottom || hacerGestoV3.moveMetrics.yMove < -outOfBoundsTopBottom) {
				outOfBounds = true;
			}
			else {
				outOfBounds = false;
			}//End if(hacerGestoV3.moveMetrics.xMove > outOfBoundsLeftRight || hacerGestoV3.moveMetrics.xMove < -outOfBoundsLeftRight)
		}
		else if(moveMetrics.cumulativeMove == false) {

			if(hacerGestoV3.firstTouchX - (hacerGestoV3.viewWidthOver2) > outOfBoundsLeftRight || hacerGestoV3.firstTouchX - (hacerGestoV3.viewWidthOver2) < -outOfBoundsLeftRight) {
				outOfBounds = true;
			}
			else if(hacerGestoV3.firstTouchY - (hacerGestoV3.viewHeightOver2) > outOfBoundsTopBottom || hacerGestoV3.firstTouchY - (hacerGestoV3.viewHeightOver2) < -outOfBoundsTopBottom) {
				outOfBounds = true;
			}
			else {
				outOfBounds = false;
			}//End if(hacerGestoV3.firstTouchX - (hacerGestoV3.viewWidthOver2) > outOfBoundsLeftRight || hacerGestoV3.firstTouchX - (hacerGestoV3.viewWidthOver2) < -outOfBoundsLeftRight)

		}//End if(moveMetrics.cumulativeMove == true)

		return  outOfBounds;

	}//End private boolean getIsOutOfBounds(final boolean usePanZoomMode)


	//Calculates where the fling will end. This is available in the main callback before the animation has ended (in the onUp touch event)
	private void setReturnType(final boolean usePanZoomMode) {

		if(usePanZoomMode == false) {
			calculateEndPointForNonPanZoom();
		}
		else if(usePanZoomMode == true) {
			calculateEndPointForPanZoom();
		}//End if(usePanZoomMode == false)

	}//End private void setReturnType(final boolean usePanZoomMode)


	private void calculateEndPointForNonPanZoom() {

		final Point returnValue = new Point(0, 0);

		if(flingToCorner == false && flingToEdge == false) {

			boolean flungToRight;
			boolean flungToBottom;
			final double xDistance = hgGeometry.difference((int) upTouchX, (int) downTouchX);
			final double yDistance = hgGeometry.difference((int) upTouchY, (int) downTouchY);

			flingRatioRect.left = 0f;
			flingRatioRect.top = 0f;
			flingRatioRect.right = (float) xDistance;
			flingRatioRect.bottom = (float) yDistance;
			flingRatio = Math.abs(flingRatioRect.width() / flingRatioRect.height());

			downPointToBoundaryRatio.left = 0;
			downPointToBoundaryRatio.top = 0;

			if(upTouchX >= downTouchX) {
				flungToRight = true;
				downPointToBoundaryRatio.right = hgGeometry.difference(downPoint.x, flingBoundary.right);
			}
			else {
				flungToRight = false;
				downPointToBoundaryRatio.right = hgGeometry.difference(downPoint.x, flingBoundary.left);
			}

			if(upTouchY >= downTouchY) {
				flungToBottom = true;
				downPointToBoundaryRatio.bottom = hgGeometry.difference(downPoint.y, flingBoundary.bottom);
			}
			else {
				flungToBottom = false;
				downPointToBoundaryRatio.bottom = hgGeometry.difference(downPoint.y, flingBoundary.top);
			}//End if(upTouchX >= downTouchX)

			final float downPointToCornerRatio = Math.abs(downPointToBoundaryRatio.width() / downPointToBoundaryRatio.height());

			//Upper Right
			if(flungToRight == true && flungToBottom == false) {

				//Closer To Right
				if(flingRatio > downPointToCornerRatio) {
					returnValue.x = flingBoundary.right;
					returnValue.y = downPoint.y - (int) (hgGeometry.difference(flingBoundary.right, downPoint.x) / flingRatio);
				}
				//Closer To Top
				else {
					returnValue.x = downPoint.x + (int) (hgGeometry.difference(flingBoundary.top, downPoint.y) * flingRatio);
					returnValue.y = flingBoundary.top;
				}

			}
			//Lower Right
			else if(flungToRight == true && flungToBottom == true) {

				//Closer To Right
				if(flingRatio > downPointToCornerRatio) {
					returnValue.x = flingBoundary.right;
					returnValue.y = downPoint.y + (int) (hgGeometry.difference(flingBoundary.right, downPoint.x) / flingRatio);
				}
				//Closer To Bottom
				else {
					returnValue.x = downPoint.x + (int) (hgGeometry.difference(flingBoundary.bottom, downPoint.y) * flingRatio);
					returnValue.y = flingBoundary.bottom;
				}

			}
			//Lower Left
			else if(flungToRight == false && flungToBottom == true) {

				//Closer To Left
				if(flingRatio > downPointToCornerRatio) {
					returnValue.x = flingBoundary.left;
					returnValue.y = downPoint.y + (int) (hgGeometry.difference(flingBoundary.left, downPoint.x) / flingRatio);
				}
				//Closer To Bottom
				else {
					returnValue.x = downPoint.x - (int) (hgGeometry.difference(flingBoundary.bottom, downPoint.y) * flingRatio);
					returnValue.y = flingBoundary.bottom;
				}

			}
			//Upper Left
			else if(flungToRight == false && flungToBottom == false) {

				//Closer To Left
				if(flingRatio > downPointToCornerRatio) {
					returnValue.x = flingBoundary.left;
					returnValue.y = downPoint.y - (int) (hgGeometry.difference(flingBoundary.left, downPoint.x) / flingRatio);
				}
				//Closer To Top
				else {
					returnValue.x = downPoint.x - (int) (hgGeometry.difference(flingBoundary.top, downPoint.y) * flingRatio);
					returnValue.y = flingBoundary.top;
				}

			}//End if(flungToRight == true && flungToBottom == false)

		}
		else if(flingToCorner == true) {

			//Condition for Upper Right Corner
			if(upTouchX >= downTouchX && upTouchY < downTouchY) {
				returnValue.x = flingBoundary.right;
				returnValue.y = flingBoundary.top;
			}
			//Condition for Lower Right Corner
			else if(upTouchX >= downTouchX && upTouchY >= downTouchY) {
				returnValue.x = flingBoundary.right;
				returnValue.y = flingBoundary.bottom;
			}
			//Condition for Lower Left Corner
			else if(upTouchX < downTouchX && upTouchY >= downTouchY) {
				returnValue.x = flingBoundary.left;
				returnValue.y = flingBoundary.bottom;
			}
			//Condition for Upper Left Corner
			else if(upTouchX < downTouchX && upTouchY < downTouchY) {
				returnValue.x = flingBoundary.left;
				returnValue.y = flingBoundary.top;
			}//End if(upTouchX >= downTouchX && upTouchY < downTouchY)

		}
		else if(flingToEdge == true) {

			flingRatioRect.left = downTouchX;
			flingRatioRect.top = downTouchY;
			flingRatioRect.right = upTouchX;
			flingRatioRect.bottom = upTouchY;
			flingRatio = Math.abs(flingRatioRect.width() / flingRatioRect.height());

			//Condition for Upper Right Corner
			if(upTouchX >= downTouchX && upTouchY < downTouchY) {
				final float moveRatio = (float) (flingBoundary.right - upPoint.x) / (float) -(flingBoundary.top - upPoint.y);

				if(moveRatio > flingRatio) {
					//Closer To Top
					returnValue.x = 0;
					returnValue.y = flingBoundary.top;
				}
				else if(moveRatio <= flingRatio) {
					//Closer To Right
					returnValue.x = flingBoundary.right;
					returnValue.y = 0;
				}//End if(moveRatio > flingRatio)

			}
			//Condition for Lower Right Corner
			else if(upTouchX >= downTouchX && upTouchY >= downTouchY) {
				final float moveRatio = (float) (flingBoundary.right - upPoint.x) / (float) (flingBoundary.bottom - upPoint.y);

				if(moveRatio > flingRatio) {
					//Closer To Bottom
					returnValue.x = 0;
					returnValue.y = flingBoundary.bottom;
				}
				else if(moveRatio <= flingRatio) {
					//Closer To Right
					returnValue.x = flingBoundary.right;
					returnValue.y = 0;
				}//End if(moveRatio > flingRatio)

			}
			//Condition for Lower Left Corner
			else if(upTouchX < downTouchX && upTouchY >= downTouchY) {
				final float moveRatio = (float) -(flingBoundary.left - upPoint.x) / (float) (flingBoundary.bottom - upPoint.y);

				if(moveRatio > flingRatio) {
					//Closer To Bottom
					returnValue.x = 0;
					returnValue.y = flingBoundary.bottom;
				}
				else if(moveRatio <= flingRatio) {
					//Closer To Left
					returnValue.x = flingBoundary.left;
					returnValue.y = 0;
				}//End if(moveRatio > flingRatio)

			}
			//Condition for Upper Left Corner
			else if(upTouchX < downTouchX && upTouchY < downTouchY) {
				final float moveRatio = (float) -(flingBoundary.left - upPoint.x) / (float) -(flingBoundary.top - upPoint.y);

				if(moveRatio > flingRatio) {
					//Closer To Top
					returnValue.x = 0;
					returnValue.y = flingBoundary.top;
				}
				else if(moveRatio <= flingRatio) {
					//Closer To Left
					returnValue.x = flingBoundary.left;
					returnValue.y = 0;
				}//End if(moveRatio > flingRatio)

			}//End if(upTouchX >= downTouchX && upTouchY < downTouchY)

		}//End if(flingToCorner == false && flingToEdge == false)

		moveAnimationHandler.endPosition.x = returnValue.x;
		moveAnimationHandler.endPosition.y = returnValue.y;

	}//End private void calculateEndPointForNonPanZoom()


	private void calculateEndPointForPanZoom() {

		final Point returnValue = new Point(0, 0);

		if(flingToCorner == false && flingToEdge == false) {

			boolean flungToRight;
			boolean flungToBottom;
			final double xDistance = hgGeometry.difference((int) upTouchX, (int) downTouchX);
			final double yDistance = hgGeometry.difference((int) upTouchY, (int) downTouchY);

			flingRatioRect.left = 0f;
			flingRatioRect.top = 0f;
			flingRatioRect.right = (float) xDistance;
			flingRatioRect.bottom = (float) yDistance;
			flingRatio = Math.abs(flingRatioRect.width() / flingRatioRect.height());

			downPointToBoundaryRatio.left = 0;
			downPointToBoundaryRatio.top = 0;

			if(upTouchX >= downTouchX) {
				flungToRight = true;
				downPointToBoundaryRatio.right = hgGeometry.difference(downPoint.x, flingBoundary.right);
			}
			else {
				flungToRight = false;
				downPointToBoundaryRatio.right = hgGeometry.difference(downPoint.x, flingBoundary.left);
			}

			if(upTouchY >= downTouchY) {
				flungToBottom = true;
				downPointToBoundaryRatio.bottom = hgGeometry.difference(downPoint.y, flingBoundary.bottom);
			}
			else {
				flungToBottom = false;
				downPointToBoundaryRatio.bottom = hgGeometry.difference(downPoint.y, flingBoundary.top);
			}//End if(upTouchX >= downTouchX)

			final float downPointToCornerRatio = Math.abs(downPointToBoundaryRatio.width() / downPointToBoundaryRatio.height());

			if(flingSlideToCorner == false) {

				//Upper Right
				if(flungToRight == true && flungToBottom == false) {

					//Closer To Right
					if(flingRatio > downPointToCornerRatio) {
						returnValue.x = flingBoundary.right;
						returnValue.y = downPoint.y - (int) (hgGeometry.difference(flingBoundary.right, downPoint.x) / flingRatio);
					}
					//Closer To Top
					else {
						returnValue.x = downPoint.x + (int) (hgGeometry.difference(flingBoundary.top, downPoint.y) * flingRatio);
						returnValue.y = flingBoundary.top;
					}

				}
				//Lower Right
				else if(flungToRight == true && flungToBottom == true) {

					//Closer To Right
					if(flingRatio > downPointToCornerRatio) {
						returnValue.x = flingBoundary.right;
						returnValue.y = downPoint.y + (int) (hgGeometry.difference(flingBoundary.right, downPoint.x) / flingRatio);
					}
					//Closer To Bottom
					else {
						returnValue.x = downPoint.x + (int) (hgGeometry.difference(flingBoundary.bottom, downPoint.y) * flingRatio);
						returnValue.y = flingBoundary.bottom;
					}

				}
				//Lower Left
				else if(flungToRight == false && flungToBottom == true) {

					//Closer To Left
					if(flingRatio > downPointToCornerRatio) {
						returnValue.x = flingBoundary.left;
						returnValue.y = downPoint.y + (int) (hgGeometry.difference(flingBoundary.left, downPoint.x) / flingRatio);
					}
					//Closer To Bottom
					else {
						returnValue.x = downPoint.x - (int) (hgGeometry.difference(flingBoundary.bottom, downPoint.y) * flingRatio);
						returnValue.y = flingBoundary.bottom;
					}

				}
				//Upper Left
				else if(flungToRight == false && flungToBottom == false) {

					//Closer To Left
					if(flingRatio > downPointToCornerRatio) {
						returnValue.x = flingBoundary.left;
						returnValue.y = downPoint.y - (int) (hgGeometry.difference(flingBoundary.left, downPoint.x) / flingRatio);
					}
					//Closer To Top
					else {
						returnValue.x = downPoint.x - (int) (hgGeometry.difference(flingBoundary.top, downPoint.y) * flingRatio);
						returnValue.y = flingBoundary.top;
					}

				}//End if(flungToRight == true && flungToBottom == false)

			}
			else if(flingSlideToCorner == true) {

				if(flungToRight == true && flungToBottom == false) {

					//Closer To Right
					if(flingRatio > downPointToCornerRatio) {
						final double distanceFromTop = flingBoundary.top - (downPoint.y - (int) (hgGeometry.difference(flingBoundary.right, downPoint.x) / flingRatio));
						returnValue.x = flingBoundary.right + -(int) (distanceFromTop * flingRatio);
						returnValue.y = flingBoundary.top;
					}
					//Closer To Top
					else {
						final double distanceFromRight = flingBoundary.right - (downPoint.x + (int) (hgGeometry.difference(flingBoundary.top, downPoint.y) * flingRatio));
						returnValue.x = flingBoundary.right;
						returnValue.y = flingBoundary.top - (int) (distanceFromRight / flingRatio);
					}

				}
				else if(flungToRight == true && flungToBottom == true) {

					//Closer To Right
					if(flingRatio > downPointToCornerRatio) {
						final double distanceFromBottom = flingBoundary.bottom - (downPoint.y + (int) (hgGeometry.difference(flingBoundary.right, downPoint.x) / flingRatio));//Numbers Don't add up
						returnValue.x = flingBoundary.right + (int) (distanceFromBottom * flingRatio);
						returnValue.y = flingBoundary.bottom;
					}
					//Closer To Bottom
					else {
						final double distanceFromRight = flingBoundary.right - (downPoint.x + (int) (hgGeometry.difference(flingBoundary.bottom, downPoint.y) * flingRatio));
						returnValue.x = flingBoundary.right;
						returnValue.y = flingBoundary.bottom + (int) (distanceFromRight / flingRatio);
					}

				}
				//Lower Left
				else if(flungToRight == false && flungToBottom == true) {

					//Tested
					//Closer To Left
					if(flingRatio > downPointToCornerRatio) {
						final double distanceFromBottom = flingBoundary.bottom - (downPoint.y + (int) (hgGeometry.difference(flingBoundary.left, downPoint.x) / flingRatio));
						returnValue.x = flingBoundary.left - (int) (distanceFromBottom * flingRatio);
						returnValue.y = flingBoundary.bottom;
					}
					//Closer To Bottom
					else {
						final double distanceFromLeft = flingBoundary.left - (downPoint.x - (int) (hgGeometry.difference(flingBoundary.bottom, downPoint.y) * flingRatio));
						returnValue.x = flingBoundary.left;
						returnValue.y = flingBoundary.bottom - (int) (distanceFromLeft / flingRatio);
					}

				}
				//Upper Left
				else if(flungToRight == false && flungToBottom == false) {

					//Closer To Left
					if(flingRatio > downPointToCornerRatio) {
						final double distanceFromTop = flingBoundary.top - (downPoint.y - (int) (hgGeometry.difference(flingBoundary.left, downPoint.x) / flingRatio));
						returnValue.x = flingBoundary.left - -(int) (distanceFromTop * flingRatio);
						returnValue.y = flingBoundary.top;
					}
					//Closer To Top
					else {
						final double distanceFromLeft = flingBoundary.left - (downPoint.x - (int) (hgGeometry.difference(flingBoundary.top, downPoint.y) * flingRatio));
						returnValue.x = flingBoundary.left;
						returnValue.y = flingBoundary.top + (int) (distanceFromLeft / flingRatio);
					}

				}//End if(flungToRight == true && flungToBottom == false)

			}//End if(flingSlideToCorner == false)

		}
		else if(flingToCorner == true) {

			//Condition for Upper Right Corner
			if(upTouchX >= downTouchX && upTouchY < downTouchY) {
				returnValue.x = flingBoundary.right;
				returnValue.y = flingBoundary.top;
			}
			//Condition for Lower Right Corner
			else if(upTouchX >= downTouchX && upTouchY >= downTouchY) {
				returnValue.x = flingBoundary.right;
				returnValue.y = flingBoundary.bottom;
			}
			//Condition for Lower Left Corner
			else if(upTouchX < downTouchX && upTouchY >= downTouchY) {
				returnValue.x = flingBoundary.left;
				returnValue.y = flingBoundary.bottom;
			}
			//Condition for Upper Left Corner
			else if(upTouchX < downTouchX && upTouchY < downTouchY) {
				returnValue.x = flingBoundary.left;
				returnValue.y = flingBoundary.top;
			}//End if(upTouchX >= downTouchX && upTouchY < downTouchY)

		}
		else if(flingToEdge == true) {

			flingRatioRect.left = downTouchX;
			flingRatioRect.top = downTouchY;
			flingRatioRect.right = upTouchX;
			flingRatioRect.bottom = upTouchY;
			flingRatio = Math.abs(flingRatioRect.width() / flingRatioRect.height());

			//Condition for Upper Right Corner
			if(upTouchX >= downTouchX && upTouchY < downTouchY) {
				final float moveRatio = (float) (flingBoundary.right - upPoint.x) / (float) -(flingBoundary.top - upPoint.y);

				if(moveRatio > flingRatio) {
					//Closer To Top
					returnValue.x = 0;
					returnValue.y = flingBoundary.top;
				}
				else if(moveRatio <= flingRatio) {
					//Closer To Right
					returnValue.x = flingBoundary.right;
					returnValue.y = 0;
				}//End if(moveRatio > flingRatio)

			}
			//Condition for Lower Right Corner
			else if(upTouchX >= downTouchX && upTouchY >= downTouchY) {
				final float moveRatio = (float) (flingBoundary.right - upPoint.x) / (float) (flingBoundary.bottom - upPoint.y);

				if(moveRatio > flingRatio) {
					//Closer To Bottom
					returnValue.x = 0;
					returnValue.y = flingBoundary.bottom;
				}
				else if(moveRatio <= flingRatio) {
					//Closer To Right
					returnValue.x = flingBoundary.right;
					returnValue.y = 0;
				}//End if(moveRatio > flingRatio)

			}
			//Condition for Lower Left Corner
			else if(upTouchX < downTouchX && upTouchY >= downTouchY) {
				final float moveRatio = (float) -(flingBoundary.left - upPoint.x) / (float) (flingBoundary.bottom - upPoint.y);

				if(moveRatio > flingRatio) {
					//Closer To Bottom
					returnValue.x = 0;
					returnValue.y = flingBoundary.bottom;
				}
				else if(moveRatio <= flingRatio) {
					//Closer To Left
					returnValue.x = flingBoundary.left;
					returnValue.y = 0;
				}//End if(moveRatio > flingRatio)

			}
			//Condition for Upper Left Corner
			else if(upTouchX < downTouchX && upTouchY < downTouchY) {
				final float moveRatio = (float) -(flingBoundary.left - upPoint.x) / (float) -(flingBoundary.top - upPoint.y);

				if(moveRatio > flingRatio) {
					//Closer To Top
					returnValue.x = 0;
					returnValue.y = flingBoundary.top;
				}
				else if(moveRatio <= flingRatio) {
					//Closer To Left
					returnValue.x = flingBoundary.left;
					returnValue.y = 0;
				}//End if(moveRatio > flingRatio)

			}//End if(upTouchX >= downTouchX && upTouchY < downTouchY)

		}//End if(flingToCorner == false && flingToEdge == false)

		moveAnimationHandler.endPosition.x = returnValue.x;
		moveAnimationHandler.endPosition.y = returnValue.y;

	}//End private void calculateEndPointForPanZoom()


	//Accessors
	int getFlingDistance() {return this.flingDistance;}
	long getFlingToMoveTime() {return this.flingToMoveTime;}
	long getFlingToMoveAnimation() {return this.flingToMoveAnimation;}
	boolean getFlingOffEdge() {return this.flingOffEdge;}
	boolean getFlingToCorner() {return this.flingToCorner;}
	boolean getFlingToEdge() {return this.flingToEdge;}
	boolean getBounceBack() {return moveAnimationHandler.bounceBack;}
	boolean getDoManualFling() {return moveAnimationHandler.doManualFling;}
	boolean getUseDynamicAnimation() {return this.useDynamicAnimation;}
	Point getFlingStartPosition() {return moveAnimationHandler.startPosition;}
	Point getFlingEndPosition() {return moveAnimationHandler.endPosition;}
	boolean getFlingSlideToCorner() {return this.flingSlideToCorner;}


	//Mutators
	void setBounceBack(final boolean bounceBack) {moveAnimationHandler.bounceBack = bounceBack;}
	void setDoManualFling(final boolean doManualFling) {moveAnimationHandler.doManualFling = doManualFling;}
	void setUseDynamicAnimation(final boolean useDynamicAnimation) {this.useDynamicAnimation = useDynamicAnimation;}
	void setFlingSlideToCorner(final boolean flingSlideToCorner) {this.flingSlideToCorner = flingSlideToCorner;}

	void setMoveFlingTolerance(final int flingDistance, final long flingToMoveTime, final long flingToMoveAnimation) {
		this.flingDistance = flingDistance;
		this.flingToMoveTime = flingToMoveTime;
		this.flingToMoveAnimation = flingToMoveAnimation;
	}

	void setMoveFlingBehaviours(final boolean flingOffEdge, final boolean flingToCorner, final boolean flingToEdge) {
		this.flingOffEdge = flingOffEdge;
		if(flingToCorner == true) {
			this.flingToEdge = false;
			this.flingToCorner = flingToCorner;
		}
		else if(flingToEdge == true) {
			this.flingToCorner = false;
			this.flingToEdge = flingToEdge;
		}
		else if(flingToCorner == false && flingToEdge == false) {
			this.flingToCorner = flingToCorner;
			this.flingToEdge = flingToEdge;
		}
	}

	/* Top of block convenience methods */
	void triggerMoveAnimation(final Point startPosition, final Point endPosition, final long animationDuration) {

		moveAnimationHandler.flingTriggered = false;
		moveAnimationHandler.startPosition.x = startPosition.x;
		moveAnimationHandler.startPosition.y = startPosition.y;
		moveAnimationHandler.endPosition.x = endPosition.x;
		moveAnimationHandler.endPosition.y = endPosition.y;
		flingToMoveAnimation = animationDuration;
		moveAnimationHandler.startMoveAnim();
	}
	/* Bottom of block convenience methods */

}