/*
LICENSE. This project is licensed with the 2-clause BSD license. The BSD 2-Clause License [OSI Approved License] The BSD 2-Clause License

In the original BSD license, both occurrences of the phrase "COPYRIGHT HOLDERS AND CONTRIBUTORS" in the disclaimer read "REGENTS AND CONTRIBUTORS".

Copyright (c) 2015, Warwick Weston Wright All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package com.WarwickWestonWright.HacerGestoV3;

import android.os.Handler;
import android.os.Message;

import java.lang.ref.WeakReference;

final class SpinAnimationHandler implements Runnable {

	private final SpinHandler spinHandler = new SpinHandler(this);
	private final RotateMetrics rotateMetrics;
	private final HacerGestoV3 hacerGestoV3;
	private Thread spinAnimationThread;
	volatile boolean spinTriggered;
	double spinStartSpeed;
	double spinCurrentSpeed;
	double spinEndSpeed;
	long spinDuration;
	int lastTextureDirection;
	long spinEndTime;
	private int minMax;

	public SpinAnimationHandler(final RotateMetrics rotateMetrics, final HacerGestoV3 hacerGestoV3) {this.rotateMetrics = rotateMetrics; this.hacerGestoV3 = hacerGestoV3;}

	void startSpinAnim() {

		if(spinAnimationThread == null) {spinAnimationThread = new Thread(this);}

		spinAnimationThread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
			@Override
			public void uncaughtException(Thread t, Throwable e) {
				spinTriggered = false;
				spinAnimationThread = null;
			}
		});

		try {
			spinAnimationThread.start();
		}
		catch(IllegalThreadStateException e) {
			spinTriggered = false;
			spinAnimationThread = null;
		}

	}//End void startSpinAnim()


	void triggerSpin(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration, final int lastTextureDirection, final long spinEndTime) {

		this.spinStartSpeed = spinStartSpeed;
		this.spinEndSpeed = spinEndSpeed;
		this.spinDuration = spinDuration;
		this.lastTextureDirection = lastTextureDirection;
		this.spinEndTime = spinEndTime;
		this.spinTriggered = true;
		if(spinAnimationThread == null) {spinAnimationThread = new Thread(this);}

		try {

			spinAnimationThread.start();

		}
		catch(IllegalThreadStateException e) {

			spinTriggered = false;
			spinAnimationThread = null;

		}

	}//End void triggerSpin(final float spinStartSpeed, final float spinEndSpeed, final long spinDuration, final int lastTextureDirection, final long spinEndTime)


	@Override
	public void run() {

		if(spinDuration == 0) {
			//Used to signal a fling to the callback without any fling animation. This is for developer convenience.
			rotateMetrics.hgv3Info.setSpinTriggered(true);
		}
		else /* if(spinDuration != 0) */ {

			if(rotateMetrics.slowDown == true) {
				doSpinAnimation();
			}
			else /* if(rotateMetrics.slowDown == false) */ {
				doContinuousSpinAnimation();
			}//End if(dialMetrics.slowDown == true)

		}//End if(spinDuration == 0)

		spinHandler.sendEmptyMessage(0);

	}//End public void run()


	/* Top of block Spin Functions called on thread */
	private void doSpinAnimation() {

		if(rotateMetrics.precisionRotation < 0) {lastTextureDirection = -lastTextureDirection;}
		this.minMax = 0;
		long currentTime = System.currentTimeMillis();
		final double speedDifference = spinStartSpeed - spinEndSpeed;

		while(spinTriggered == true) {

			spinCurrentSpeed = (spinStartSpeed + (speedDifference * ((double) (spinEndTime - currentTime) / spinDuration))) - speedDifference;

			if(currentTime < spinEndTime) {

				//Update view every 10 milliseconds
				if(currentTime + 10L < System.currentTimeMillis()) {

					currentTime = System.currentTimeMillis();
					final double lastAngle = rotateMetrics.fullGestureAngle;
					rotateMetrics.fullGestureAngle += (0.01f * spinCurrentSpeed) * lastTextureDirection;
					int direction = lastAngle > rotateMetrics.fullGestureAngle ? -1 : 1;
					if(rotateMetrics.precisionRotation < 0) {direction = -direction;}
					rotateMetrics.angleWrapper.setTextureAngle(rotateMetrics.fullGestureAngle * rotateMetrics.precisionRotation);
					if(rotateMetrics.angleSnap != 0) {rotateMetrics.checkAngleSnap();}
					rotateMetrics.hgv3Info.setTextureAngle(rotateMetrics.angleWrapper.getTextureAngle());
					rotateMetrics.hgv3Info.setTextureDirection(direction);
					rotateMetrics.hgv3Info.setSpinCurrentSpeed((float) spinCurrentSpeed);
					final int minMax;

					if((minMax = checkMinMaxBounds(rotateMetrics.angleWrapper.getTextureAngle())) == 0) {

						hacerGestoV3.post(new Runnable() {
							@Override
							public void run() {
								if(hacerGestoV3.iHacerGestoV3 != null) {
									hacerGestoV3.iHacerGestoV3.onMove(rotateMetrics.hgv3Info);
									hacerGestoV3.invalidate();
								}
							}
						});

					}
					else if(checkMinMaxBounds(rotateMetrics.angleWrapper.getTextureAngle()) != 0) {

						this.minMax = minMax;
						rotateMetrics.hgv3Info.setMinMaxReached(minMax);
						spinTriggered = false;

					}//End if((minMax = checkMinMaxBounds(dialMetrics.angleWrapper.getTextureAngle())) == 0)

				}//End if(currentTime + 10l < System.currentTimeMillis())

			}
			else {

				spinTriggered = false;
				rotateMetrics.hgv3Info.setSpinTriggered(false);

			}//End if(spinCurrentSpeed > 0)

		}//End while(spinTriggered == true)

	}//End private void doSpinAnimation()


	private void doContinuousSpinAnimation() {

		this.minMax = 0;
		long currentTime = System.currentTimeMillis();
		final long startTime = currentTime;
		final double startSpinAngle = rotateMetrics.fullGestureAngle;
		rotateMetrics.hgv3Info.setSpinCurrentSpeed((float) spinStartSpeed);
		spinCurrentSpeed = spinStartSpeed;

		if(rotateMetrics.useVariableDial == false && rotateMetrics.precisionRotation < 0) {lastTextureDirection = -lastTextureDirection;}

		while(spinTriggered == true) {

			//Update view every 10 milliseconds
			if(currentTime + 10L < System.currentTimeMillis()) {

				currentTime = System.currentTimeMillis();
				final double animDuration = ((double) (currentTime - startTime) / 1000d);
				rotateMetrics.fullGestureAngle = startSpinAngle + (animDuration * spinStartSpeed * lastTextureDirection);
				rotateMetrics.angleWrapper.setTextureAngle(rotateMetrics.fullGestureAngle * rotateMetrics.precisionRotation);
				if(rotateMetrics.angleSnap != 0) {rotateMetrics.checkAngleSnap();}
				rotateMetrics.hgv3Info.setTextureAngle(rotateMetrics.angleWrapper.getTextureAngle());
				final int minMax;

				if((minMax = checkMinMaxBounds(rotateMetrics.angleWrapper.getTextureAngle())) == 0) {

					hacerGestoV3.post(new Runnable() {
						@Override
						public void run() {
							if(hacerGestoV3.iHacerGestoV3 != null) {
								hacerGestoV3.iHacerGestoV3.onMove(rotateMetrics.hgv3Info);
								hacerGestoV3.invalidate();
							}
						}
					});

				}
				else if(checkMinMaxBounds(rotateMetrics.angleWrapper.getTextureAngle()) != 0) {

					this.minMax = minMax;
					rotateMetrics.hgv3Info.setMinMaxReached(minMax);
					spinTriggered = false;

				}//End if((minMax = checkMinMaxBounds(dialMetrics.angleWrapper.getTextureAngle())) == 0)

			}//End if(currentTime + 10l < System.currentTimeMillis())

		}//End while(spinTriggered == true)

	}//End private void doContinuousSpinAnimation()
	/* Bottom of block Spin Functions called on thread */


	//Allows spin behaviour to work intuitively with min max behaviour
	private int checkMinMaxBounds(final double textureAngle) {

		if(rotateMetrics.useMinMaxRotation == false) {

			return 0;

		}
		else /* if(rotateMetrics.useMinMaxRotation == true) */ {

			if(textureAngle > rotateMetrics.maximumRotation) {

				return 1;

			}
			else if(textureAngle < rotateMetrics.minimumRotation) {

				return -1;

			}//End if(textureAngle > dialMetrics.maximumRotation)

		}//End if(dialMetrics.useMinMaxRotation == false)

		return 0;

	}//End private int checkMinMaxBounds(final double textureAngle)


	private static class SpinHandler extends Handler {

		private final WeakReference<SpinAnimationHandler> spinAnimationHandlerWeakReference;

		SpinHandler(SpinAnimationHandler spinAnimationHandler) {
			spinAnimationHandlerWeakReference = new WeakReference<>(spinAnimationHandler);
		}

		@Override
		public void handleMessage(Message msg) {

			if(spinAnimationHandlerWeakReference != null) {

				final SpinAnimationHandler spinAnimationHandler = spinAnimationHandlerWeakReference.get();

				if(spinAnimationHandler.rotateMetrics.spinTriggeredProgrammatically == true) {
					spinAnimationHandler.rotateMetrics.spinTriggeredProgrammatically = false;
					spinAnimationHandler.rotateMetrics.spinStartSpeed = spinAnimationHandler.rotateMetrics.spinStartSpeedState;
					spinAnimationHandler.rotateMetrics.spinEndSpeed = spinAnimationHandler.rotateMetrics.spinEndSpeedState;
					spinAnimationHandler.rotateMetrics.spinDuration = spinAnimationHandler.rotateMetrics.spinDurationState;
				}

				if(spinAnimationHandler.spinTriggered == false) {

					spinAnimationHandler.rotateMetrics.hgv3Info.setSpinTriggered(false);
					spinAnimationHandler.rotateMetrics.hgv3Info.setGestureDirection(0);
					spinAnimationHandler.rotateMetrics.hgv3Info.setTextureDirection(0);
					spinAnimationHandler.rotateMetrics.hgv3Info.setSpinCurrentSpeed(0f);

					if(spinAnimationHandler.minMax != 0) {

						if(spinAnimationHandler.minMax == 1) {

							spinAnimationHandler.rotateMetrics.doManualTextureDial(spinAnimationHandler.rotateMetrics.maximumRotation);
							spinAnimationHandler.rotateMetrics.hgv3Info.setMinMaxReached(spinAnimationHandler.minMax);
							spinAnimationHandler.rotateMetrics.hgv3Info.setTextureAngle(spinAnimationHandler.rotateMetrics.maximumRotation);

						}
						else if(spinAnimationHandler.minMax == -1) {

							spinAnimationHandler.rotateMetrics.doManualTextureDial(spinAnimationHandler.rotateMetrics.minimumRotation);
							spinAnimationHandler.rotateMetrics.hgv3Info.setMinMaxReached(spinAnimationHandler.minMax);
							spinAnimationHandler.rotateMetrics.hgv3Info.setTextureAngle(spinAnimationHandler.rotateMetrics.minimumRotation);

						}//End if(animationHandler.minMax == 1)

					}
					else if(spinAnimationHandler.rotateMetrics.useVariableDial == false) {

						spinAnimationHandler.rotateMetrics.doManualTextureDial(spinAnimationHandler.rotateMetrics.angleWrapper.getTextureAngle());
						spinAnimationHandler.rotateMetrics.hgv3Info.setMinMaxReached(spinAnimationHandler.minMax);

					}//End if(spinAnimationHandler.minMax != 0)

					if(spinAnimationHandler.hacerGestoV3.iHacerGestoV3 != null) {
						spinAnimationHandler.hacerGestoV3.iHacerGestoV3.onUp(spinAnimationHandler.rotateMetrics.hgv3Info);
						spinAnimationHandler.hacerGestoV3.invalidate();
					}

				}//End if(spinAnimationHandler.spinTriggered == false)

				spinAnimationHandler.spinAnimationThread = null;

			}//End if(spinAnimationHandlerWeakReference != null)

		}

	}//End private static class SpinHandler extends Handler

}