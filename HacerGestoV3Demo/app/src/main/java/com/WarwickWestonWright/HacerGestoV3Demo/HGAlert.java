package com.WarwickWestonWright.HacerGestoV3Demo;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

public class HGAlert extends DialogFragment {

	public interface IHGAlert {
		void hgAlertCallback(DialogInterface dialog, int id);
	}

	private DialogInterface.OnClickListener clickListener;

	public final static int OK = -1;
	public final static int CANCEL = -2;

	public HGAlert() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		clickListener = (DialogInterface.OnClickListener) getTargetFragment();

	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Use the Builder class for convenient dialog construction
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(R.string.quick_tap_warning)
			.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					if(clickListener != null) {
						clickListener.onClick(dialog, id);
					}
				}
			})
			.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					if(clickListener != null) {
						clickListener.onClick(dialog, id);
					}
				}
			});
		// Create the AlertDialog object and return it
		return builder.create();
	}


	/*
	@Override
	public void onAttach(Context context) {super.onAttach(context);}
	*/


}
