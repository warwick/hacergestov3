/*
Though the code in the ‘com.WarwickWestonWright.HacerGestoV3’ package is protected by open source, this code (the code in the demo app)
in the ‘com.WarwickWestonWright.HacerGestoV3Demo’ package is free source and can be modified and reused without any obligation to the developer.
*/

package com.WarwickWestonWright.HacerGestoV3Demo.SettingsFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.WarwickWestonWright.HacerGestoV3Demo.MainActivity;
import com.WarwickWestonWright.HacerGestoV3Demo.R;

public class FlingSettingsFragment extends Fragment implements Animation.AnimationListener {

	public interface IFlingSettingsFragment {void flingSettingsCallback();}

	private IFlingSettingsFragment iFlingSettingsFragment;
	private View rootView;
	private EditText txtDistanceThreshold;
	private EditText txtTimeThreshold;
	private EditText txtAnimationDuration;
	private CheckBox chkDynamicAnimation;
	private CheckBox chkFlingOffEdge;
	private CheckBox chkFlingToCorner;
	private CheckBox chkFlingToEdge;
	private CheckBox chkBounceBack;
	private Button btnCloseFlingSettings;
	private LinearLayout lLayoutDurationContainer;
	private SharedPreferences sp;
	private Animation animationDurationFadeIn;
	private Animation animationDurationFadeOut;

	public FlingSettingsFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sp = getActivity().getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE);
		animationDurationFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_expand_in);
		animationDurationFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_shrink_out);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fling_settings_fragment, container, false);

		animationDurationFadeIn.setAnimationListener(this);
		animationDurationFadeOut.setAnimationListener(this);

		txtDistanceThreshold = rootView.findViewById(R.id.txtDistanceThreshold);
		txtTimeThreshold = rootView.findViewById(R.id.txtTimeThreshold);
		txtAnimationDuration = rootView.findViewById(R.id.txtAnimationDuration);
		chkDynamicAnimation = rootView.findViewById(R.id.chkDynamicAnimation);
		chkFlingOffEdge = rootView.findViewById(R.id.chkFlingOffEdge);
		chkFlingToCorner = rootView.findViewById(R.id.chkFlingToCorner);
		chkFlingToEdge = rootView.findViewById(R.id.chkFlingToEdge);
		chkBounceBack = rootView.findViewById(R.id.chkBounceBack);
		btnCloseFlingSettings = rootView.findViewById(R.id.btnCloseFlingSettings);
		lLayoutDurationContainer = rootView.findViewById(R.id.lLayoutDurationContainer);

		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtDistanceThreshold);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtTimeThreshold);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtAnimationDuration);

		txtDistanceThreshold.setText(Integer.toString(sp.getInt("flingToMoveDistance", 150)));
		txtTimeThreshold.setText(Long.toString(sp.getLong("flingToMoveTime", 250)));
		txtAnimationDuration.setText(Long.toString(sp.getLong("flingToMoveAnimation", 650)));
		chkDynamicAnimation.setChecked(sp.getBoolean("dynamicAnimation", false));
		chkFlingOffEdge.setChecked(sp.getBoolean("flingOffEdge", true));
		chkFlingToCorner.setChecked(sp.getBoolean("flingToCorner", false));
		chkFlingToEdge.setChecked(sp.getBoolean("flingToEdge", false));
		chkBounceBack.setChecked(sp.getBoolean("bounceBack", false));

		if(chkDynamicAnimation.isChecked() == true) {

			lLayoutDurationContainer.setVisibility(View.GONE);
			lLayoutDurationContainer.requestLayout();

		}
		else if(chkDynamicAnimation.isChecked() == false) {

			lLayoutDurationContainer.setVisibility(View.VISIBLE);
			lLayoutDurationContainer.requestLayout();

		}//End if(chkDynamicAnimation.isChecked() == true)

		chkDynamicAnimation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(chkDynamicAnimation.isChecked() == true) {

					lLayoutDurationContainer.startAnimation(animationDurationFadeOut);

				}
				else if(chkDynamicAnimation.isChecked() == false) {

					lLayoutDurationContainer.setVisibility(View.VISIBLE);
					lLayoutDurationContainer.requestLayout();
					lLayoutDurationContainer.startAnimation(animationDurationFadeIn);

				}//End if(chkDynamicAnimation.isChecked() == true)

			}
		});

		chkFlingOffEdge.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//Empty for Haptic Feedback
			}
		});

		chkFlingToCorner.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(chkFlingToCorner.isChecked() == true) {chkFlingToEdge.setChecked(false);}
			}
		});

		chkFlingToEdge.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(chkFlingToEdge.isChecked() == true) {chkFlingToCorner.setChecked(false);}
			}
		});

		chkBounceBack.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				//Empty for Haptic Feedback
			}
		});

		btnCloseFlingSettings.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setPrefsOnExit();
				iFlingSettingsFragment.flingSettingsCallback();
			}
		});

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	private void setPrefsOnExit() {
		if(txtDistanceThreshold != null && txtDistanceThreshold.getText().toString().isEmpty() == true) {
			txtDistanceThreshold.setText(Integer.toString(sp.getInt("flingToMoveDistance", 150)));
		}

		if(txtTimeThreshold != null && txtTimeThreshold.getText().toString().isEmpty() == true) {
			txtTimeThreshold.setText(Long.toString(sp.getLong("flingToMoveTime", 250)));
		}

		if(txtAnimationDuration != null && txtAnimationDuration.getText().toString().isEmpty() == true) {
			txtAnimationDuration.setText(Long.toString(sp.getLong("flingToMoveAnimation", 650)));
		}

		sp.edit().putInt("flingToMoveDistance", Integer.parseInt(txtDistanceThreshold.getText().toString())).commit();
		sp.edit().putLong("flingToMoveTime", Long.parseLong(txtTimeThreshold.getText().toString())).commit();
		sp.edit().putLong("flingToMoveAnimation", Long.parseLong(txtAnimationDuration.getText().toString())).commit();
		sp.edit().putBoolean("dynamicAnimation", chkDynamicAnimation.isChecked()).commit();
		sp.edit().putBoolean("flingOffEdge", chkFlingOffEdge.isChecked()).commit();
		sp.edit().putBoolean("flingToCorner", chkFlingToCorner.isChecked()).commit();
		sp.edit().putBoolean("flingToEdge", chkFlingToEdge.isChecked()).commit();
		sp.edit().putBoolean("bounceBack", chkBounceBack.isChecked()).commit();
	}//End private void setPrefsOnExit()


	@Override
	public void onAnimationStart(Animation animation) {}
	@Override
	public void onAnimationEnd(Animation animation) {

		if(animation == animationDurationFadeIn) {

			lLayoutDurationContainer.setVisibility(View.VISIBLE);
			lLayoutDurationContainer.requestLayout();

		}
		else if(animation == animationDurationFadeOut) {

			lLayoutDurationContainer.setVisibility(View.GONE);
			lLayoutDurationContainer.requestLayout();

		}

	}
	@Override
	public void onAnimationRepeat(Animation animation) {}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(context instanceof IFlingSettingsFragment) {
			iFlingSettingsFragment = (IFlingSettingsFragment) context;
		}
		else {
			throw new RuntimeException(context.toString() + " must implement IFlingSettingsFragment");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		iFlingSettingsFragment = null;
	}

}