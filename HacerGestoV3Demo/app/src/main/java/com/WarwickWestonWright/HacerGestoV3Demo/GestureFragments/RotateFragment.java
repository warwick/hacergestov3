/*
Though the code in the ‘com.WarwickWestonWright.HacerGestoV3’ package is protected by open source, this code (the code in the demo app)
in the ‘com.WarwickWestonWright.HacerGestoV3Demo’ package is free source and can be modified and reused without any obligation to the developer.
*/

package com.WarwickWestonWright.HacerGestoV3Demo.GestureFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.WarwickWestonWright.HacerGestoV3.HGV3Info;
import com.WarwickWestonWright.HacerGestoV3.HacerGestoV3;
import com.WarwickWestonWright.HacerGestoV3Demo.HGFlagFullScreen;
import com.WarwickWestonWright.HacerGestoV3Demo.R;
import com.WarwickWestonWright.HacerGestoV3Demo.SettingsFragments.RotateSettingsFragment;
import com.WarwickWestonWright.HacerGestoV3Demo.SetupBehaviours;

public class RotateFragment extends Fragment {

	public interface IRotateFragment {void rotateFragmentCallback();}
	private IRotateFragment iRotateFragment;
	private HacerGestoV3.IHacerGestoV3 iHacerGestoV3;
	private View rootView;
	private HacerGestoV3 hacerGestoV3;
	private TextView lblRotateDemoStatus;
	private String status;
	private RotateSettingsFragment rotateSettingsFragment;
	private SharedPreferences sp;
	private HGFlagFullScreen hgFlagFullScreen;

	public RotateFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		status = "";
		sp = getActivity().getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.rotate_fragment, container, false);

		hgFlagFullScreen = new HGFlagFullScreen(getActivity().getWindow());
		hgFlagFullScreen.flagFullScreen();

		hacerGestoV3 = rootView.findViewById(R.id.hgRotateV3);
		hacerGestoV3.setUseGestures(hacerGestoV3.ROTATE);
		lblRotateDemoStatus = rootView.findViewById(R.id.lblRotateDemoStatus);
		hacerGestoV3.setQuickTapTime(sp.getLong("quickTapTime", 100L));
		/*
		The CRLFs are to prevent the angle from going out when the object redraws itself on the initial on down.
		This is not a bug in the library, but the programming overhead to fix this will be too extensive keep tis workaround in mind
		*/
		lblRotateDemoStatus.setText("\n\n\n\n\n");

		iHacerGestoV3 = new HacerGestoV3.IHacerGestoV3() {
			@Override
			public void onDown(HGV3Info hgv3Info) {

				status = "TextureAngle: " + Double.toString(hgv3Info.getTextureAngle()) + "\n" +
					"TextureDirection: " + Integer.toString(hgv3Info.getTextureDirection()) + "\n" +
					"GestureAngle: " + Double.toString(hgv3Info.getGestureAngle()) + "\n" +
					"GestureDirection: " + Integer.toString(hgv3Info.getGestureDirection()) + "\n" +
					"RotateSnapped: " + Boolean.toString(hgv3Info.getRotateSnapped()) + "\n" +
					"SpinTriggered: " + Boolean.toString(hgv3Info.isSpinTriggered());
				lblRotateDemoStatus.setText(status);

			}

			@Override
			public void onPointerDown(HGV3Info hgv3Info) {}

			@Override
			public void onMove(HGV3Info hgv3Info) {

				status = "TextureAngle: " + Double.toString(hgv3Info.getTextureAngle()) + "\n" +
					"TextureDirection: " + Integer.toString(hgv3Info.getTextureDirection()) + "\n" +
					"GestureAngle: " + Double.toString(hgv3Info.getGestureAngle()) + "\n" +
					"GestureDirection: " + Integer.toString(hgv3Info.getGestureDirection()) + "\n" +
					"RotateSnapped: " + Boolean.toString(hgv3Info.getRotateSnapped()) + "\n" +
					"SpinTriggered: " + Boolean.toString(hgv3Info.isSpinTriggered());
				lblRotateDemoStatus.setText(status);

			}

			@Override
			public void onPointerUp(HGV3Info hgv3Info) {}

			@Override
			public void onUp(HGV3Info hgv3Info) {

				status = "TextureAngle: " + Double.toString(hgv3Info.getTextureAngle()) + "\n" +
					"TextureDirection: " + Integer.toString(hgv3Info.getTextureDirection()) + "\n" +
					"GestureAngle: " + Double.toString(hgv3Info.getGestureAngle()) + "\n" +
					"GestureDirection: " + Integer.toString(hgv3Info.getGestureDirection()) + "\n" +
					"RotateSnapped: " + Boolean.toString(hgv3Info.getRotateSnapped()) + "\n" +
					"SpinTriggered: " + Boolean.toString(hgv3Info.isSpinTriggered());
				lblRotateDemoStatus.setText(status);

				if(hgv3Info.getQuickTap() == true) {

					//Set Test to true and quick tap rotate fragment to demonstrate triggerSpin method.
					boolean test = false;
					if(test == false) {

						if(rotateSettingsFragment == null) {rotateSettingsFragment = new RotateSettingsFragment();}

						if(rotateSettingsFragment.isAdded() == false) {

							getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutSettingsFragmentContainer, rotateSettingsFragment, "RotateSettingsFragment").commit();

						}//End if(mainSettingsFragment.isAdded() == false)

					}
					else if(test == true) {

						hacerGestoV3.triggerSpin(5f, 0f, 3500L, -1);

					}//End if(test == false)

				}//End if(hgv3Info.getQuickTap() == true)

			}
		};

		hacerGestoV3.registerCallback(iHacerGestoV3);

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	@Override
	public void onResume() {
		super.onResume();
		final SetupBehaviours setupBehaviours = new SetupBehaviours();
		setupBehaviours.setupRotate(hacerGestoV3, sp);
		hacerGestoV3.setPanZoomMode(true, false, false);
	}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if(context instanceof IRotateFragment) {
			iRotateFragment = (IRotateFragment) context;
		}
		else {
			throw new RuntimeException(context.toString() + " must implement IRotateFragment");
		}

	}//End public void onAttach(Context context)


	@Override
	public void onStop() {
		super.onStop();
		hacerGestoV3.cancelSpin();
	}


	@Override
	public void onDetach() {
		super.onDetach();
		iRotateFragment = null;
	}

}