/*
Though the code in the ‘com.WarwickWestonWright.HacerGestoV3’ package is protected by open source, this code (the code in the demo app)
in the ‘com.WarwickWestonWright.HacerGestoV3Demo’ package is free source and can be modified and reused without any obligation to the developer.
*/

package com.WarwickWestonWright.HacerGestoV3Demo;

import android.content.SharedPreferences;
import android.graphics.Point;

import com.WarwickWestonWright.HacerGestoV3.HacerGestoV3;

public class SetupBehaviours {

	public SetupBehaviours() {}

	public void setupRotate(final HacerGestoV3 hacerGestoV3, final SharedPreferences sp) {

		//This bock demonstrates how easy it is to set gesture behaviours.

		//Enable/Disable precision dial behaviour
		if(sp.getBoolean("usePrecision", false) == true) {
			hacerGestoV3.setPrecisionRotation(sp.getFloat("precision", 0.5f));
		}
		else if(sp.getBoolean("usePrecision", false) == false) {
			hacerGestoV3.setPrecisionRotation(1.0d);
		}//End if(sp.getBoolean("usePrecision", false) == true)

		//Enable/Disable variable dial behaviour
		if(sp.getBoolean("useVariableDial", false) == true) {
			hacerGestoV3.setVariableDial(sp.getFloat("innerPrecision", 5.2f), sp.getFloat("outerPrecision", 0.2f), true);
			hacerGestoV3.setUseVariableDialCurve(sp.getBoolean("useVariableCurve", false), sp.getBoolean("usePositiveCurve", false));
		}
		else if(sp.getBoolean("useVariableDial", false) == false) {
			hacerGestoV3.setVariableDial(0, 0, false);
		}//End if(sp.getBoolean("useVariableDial", false) == true)

		//Enable/Disable cumulative dial behaviour
		hacerGestoV3.setCumulativeRotate(sp.getBoolean("cumulativeDial", true));

		//Enable/Disable single finger behaviour
		hacerGestoV3.setUseSingleFinger(sp.getBoolean("isSingleFinger", false));

		//Setup angle snapping behaviour
		if(sp.getBoolean("useAngleSnap", false) == true) {
			hacerGestoV3.setAngleSnap(sp.getFloat("angleSnap", 0.125f), sp.getFloat("snapProximity", 0.03125f));
		}
		else if(sp.getBoolean("useAngleSnap", false) == false) {
			hacerGestoV3.setAngleSnap(0f, 0f);
		}//End if(sp.getBoolean("useAngleSnap", false) == true)

		//Enable/Disable min/max rotation behaviour
		if(sp.getBoolean("useMinMax", false) == true) {
			hacerGestoV3.setMinMaxDial(sp.getFloat("minRotation", -2.45f), sp.getFloat("maxRotation", 2.88f), true);
		}
		else if(sp.getBoolean("useMinMax", false) == false) {
			hacerGestoV3.setMinMaxDial(0, 0, false);
		}//End if(sp.getBoolean("useMinMax", false) == true)

		//Enable/Disable fling-to-spin behaviour
		if(sp.getBoolean("enableFlingToSpin", false) == true) {
			hacerGestoV3.setSpinTolerance(sp.getInt("flingDistanceToSpin", 150), sp.getLong("flingTimeToSpin", 200L));
			hacerGestoV3.setSpinAnimation(sp.getFloat("spinStartSpeed", 0f), sp.getFloat("spinEndSpeed", 0f), sp.getLong("spinDuration", 200L));
			hacerGestoV3.setSlowFactor(sp.getFloat("slowFactor", 0f));
			hacerGestoV3.setSlowDown(sp.getBoolean("chkSlowDown", true));
			hacerGestoV3.setFlingAngleTolerance(sp.getFloat("flingAngle", 0f));
		}
		else if(sp.getBoolean("enableFlingToSpin", false) == false) {
			hacerGestoV3.setSpinTolerance(0, 0L);
			hacerGestoV3.setSpinAnimation(0f, 0f, 0L);
			hacerGestoV3.setSlowFactor(0f);
			hacerGestoV3.setSlowDown(true);
			hacerGestoV3.setFlingAngleTolerance(0f);
		}//End if(sp.getBoolean("enableFlingToSpin", false) == true)

	}//End public void setupRotate(final HacerGestoV3 hacerGestoV3, final SharedPreferences sp)

	public void setupScale(final HacerGestoV3 hacerGestoV3, final SharedPreferences sp) {

		if(sp.getBoolean("useMinMaxScale", false) == true) {
			hacerGestoV3.setUseMinMaxScale(sp.getFloat("minScale", 0.2f), sp.getFloat("maxScale", 1.5f), true);
		}
		else if(sp.getBoolean("useMinMaxScale", false) == false) {
			hacerGestoV3.setUseMinMaxScale(sp.getFloat("minScale", 0.2f), sp.getFloat("maxScale", 1.5f), false);
		}//End if(sp.getBoolean("useMinMaxScale", false) == true)

		hacerGestoV3.setScaleSnapping(sp.getInt("scaleSnapping", 0));

	}//End public void setupScale(final HacerGestoV3 hacerGestoV3, final SharedPreferences sp)

	public void setupMove(final HacerGestoV3 hacerGestoV3, final SharedPreferences sp, Point[] sides, int[] tolerances) {

		hacerGestoV3.setCumulativeMove(sp.getBoolean("cumulativeMove", true));

		if(sp.getBoolean("moveSnapping", false) == true) {
			hacerGestoV3.setMoveSnapping(sides, tolerances);
		}
		else if(sp.getBoolean("moveSnapping", false) == false) {
			hacerGestoV3.setMoveSnapping(null, null);
		}//End if(sp.getBoolean("moveSnapping", false) == true)

		if(sp.getBoolean("useMoveConstraint", false) == true) {
			hacerGestoV3.setMoveConstraintObj(sp.getInt("l", 0), sp.getInt("t", 0), sp.getInt("r", 0), sp.getInt("b", 0),
				sp.getBoolean("offEdge", false), sp.getBoolean("toEdge", true), true);
		}
		else if(sp.getBoolean("useMoveConstraint", false) == false) {
			hacerGestoV3.setMoveConstraintObj(0, 0, 0, 0, false, false, false);
		}//End if(sp.getBoolean("useMoveConstraint", false) == true)

		if(sp.getBoolean("usePanZoomMode", false) == true) {
			hacerGestoV3.setPanZoomMode(sp.getBoolean("zoomCenterCrop", true), sp.getBoolean("zoomWithRotation", false), true);
		}
		else if(sp.getBoolean("usePanZoomMode", false) == false) {
			hacerGestoV3.setPanZoomMode(sp.getBoolean("zoomCenterCrop", true), sp.getBoolean("zoomWithRotation", false), false);
		}//End if(sp.getBoolean("usePanZoomMode", false) == true)

	}//End public void setupMove(final HacerGestoV3 hacerGestoV3, final SharedPreferences sp, Point[] sides, int[] tolerances)

	public void setupFling(final HacerGestoV3 hacerGestoV3, final SharedPreferences sp) {

		if(sp.getBoolean("flingToMove", false) == true) {
			hacerGestoV3.setMoveFlingTolerance(sp.getInt("flingToMoveDistance", 150), sp.getLong("flingToMoveTime", 250), sp.getLong("flingToMoveAnimation", 250));
			hacerGestoV3.setMoveFlingBehaviours(sp.getBoolean("flingOffEdge", true), sp.getBoolean("flingToCorner", false), sp.getBoolean("flingToEdge", false));
			hacerGestoV3.setUseDynamicAnimation(sp.getBoolean("dynamicAnimation", false));
			hacerGestoV3.setBounceBack(sp.getBoolean("bounceBack", false));
			hacerGestoV3.setFlingSlideToCorner(sp.getBoolean("flingSlideToCorner", true));
		}//End if(sp.getBoolean("flingToMove", false) == true)

	}//End public void setupFling(final HacerGestoV3 hacerGestoV3, final SharedPreferences sp)

}