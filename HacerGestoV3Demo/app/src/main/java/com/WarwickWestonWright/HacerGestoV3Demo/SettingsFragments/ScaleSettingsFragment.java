/*
Though the code in the ‘com.WarwickWestonWright.HacerGestoV3’ package is protected by open source, this code (the code in the demo app)
in the ‘com.WarwickWestonWright.HacerGestoV3Demo’ package is free source and can be modified and reused without any obligation to the developer.
*/

package com.WarwickWestonWright.HacerGestoV3Demo.SettingsFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.WarwickWestonWright.HacerGestoV3Demo.MainActivity;
import com.WarwickWestonWright.HacerGestoV3Demo.R;

public class ScaleSettingsFragment extends Fragment implements Animation.AnimationListener {

	private SharedPreferences sp;

	public interface IScaleSettingsFragment {void scaleSettingsCallback();}
	private IScaleSettingsFragment iScaleSettingsFragment;
	private View rootView;
	private AppCompatCheckBox chkUseScaling;
	private EditText txtMinScale;
	private EditText txtMaxScale;
	private EditText txtSnapping;
	private LinearLayout lLayoutScalingContainer;
	private AppCompatButton btnCloseScaleSettings;

	private Animation scaleConstraintFadeIn;
	private Animation scaleConstraintFadeOut;

	public ScaleSettingsFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sp = getActivity().getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE);
		scaleConstraintFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_expand_in);
		scaleConstraintFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_shrink_out);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.scale_settings_fragment, container, false);

		scaleConstraintFadeIn.setAnimationListener(this);
		scaleConstraintFadeOut.setAnimationListener(this);

		chkUseScaling = rootView.findViewById(R.id.chkUseScaling);
		txtMinScale = rootView.findViewById(R.id.txtMinScale);
		txtMaxScale = rootView.findViewById(R.id.txtMaxScale);
		txtSnapping = rootView.findViewById(R.id.txtSnapping);
		lLayoutScalingContainer = rootView.findViewById(R.id.lLayoutScalingContainer);
		btnCloseScaleSettings = rootView.findViewById(R.id.btnCloseScaleSettings);

		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtMinScale);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtMaxScale);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtSnapping);

		chkUseScaling.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(chkUseScaling.isChecked() == true) {

					lLayoutScalingContainer.setVisibility(View.VISIBLE);
					lLayoutScalingContainer.requestLayout();
					lLayoutScalingContainer.startAnimation(scaleConstraintFadeIn);

				}
				else if(chkUseScaling.isChecked() == false) {

					lLayoutScalingContainer.startAnimation(scaleConstraintFadeOut);

				}//End if(chkUseScaling.isChecked() == true)

			}
		});

		chkUseScaling.setChecked(sp.getBoolean("useMinMaxScale", false));
		txtMinScale.setText(Float.toString(sp.getFloat("minScale", 0.2f)));
		txtMaxScale.setText(Float.toString(sp.getFloat("maxScale", 1.5f)));
		txtSnapping.setText(Integer.toString(sp.getInt("scaleSnapping", 0)));

		if(chkUseScaling.isChecked() == true) {

			lLayoutScalingContainer.setVisibility(View.VISIBLE);
			lLayoutScalingContainer.requestLayout();

		}
		else if(chkUseScaling.isChecked() == false) {

			lLayoutScalingContainer.setVisibility(View.GONE);
			lLayoutScalingContainer.requestLayout();

		}//End if(chkUseScaling.isChecked() == true)

		btnCloseScaleSettings.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setPrefsOnExit();
				iScaleSettingsFragment.scaleSettingsCallback();
			}
		});

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	private void setPrefsOnExit() {

		if(txtMinScale != null && txtMinScale.getText().toString().isEmpty() == true) {
			txtMinScale.setText(Float.toString(sp.getFloat("minScale", 0.2f)));
		}

		if(txtMaxScale != null && txtMaxScale.getText().toString().isEmpty() == true) {
			txtMaxScale.setText(Float.toString(sp.getFloat("maxScale", 1.5f)));
		}

		if(txtSnapping != null && txtSnapping.getText().toString().isEmpty() == true) {
			txtSnapping.setText(Integer.toString(sp.getInt("scaleSnapping", 0)));
		}

		sp.edit().putBoolean("useMinMaxScale", chkUseScaling.isChecked()).commit();
		sp.edit().putFloat("minScale", Float.parseFloat(txtMinScale.getText().toString())).commit();
		sp.edit().putFloat("maxScale", Float.parseFloat(txtMaxScale.getText().toString())).commit();
		sp.edit().putInt("scaleSnapping", Integer.parseInt(txtSnapping.getText().toString())).commit();

	}//End private void setPrefsOnExit()


	@Override
	public void onAnimationStart(Animation animation) {}
	@Override
	public void onAnimationEnd(Animation animation) {

		if(animation == scaleConstraintFadeIn) {

			lLayoutScalingContainer.setVisibility(View.VISIBLE);
			lLayoutScalingContainer.requestLayout();

		}
		else if(animation == scaleConstraintFadeOut) {

			lLayoutScalingContainer.setVisibility(View.GONE);
			lLayoutScalingContainer.requestLayout();

		}

	}
	@Override
	public void onAnimationRepeat(Animation animation) {}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if(context instanceof IScaleSettingsFragment) {
			iScaleSettingsFragment = (IScaleSettingsFragment) context;
		}
		else {
			throw new RuntimeException(context.toString() + " must implement IScaleSettingsFragment");
		}

	}//End public void onAttach(Context context)


	@Override
	public void onDetach() {
		super.onDetach();
		iScaleSettingsFragment = null;
	}

}