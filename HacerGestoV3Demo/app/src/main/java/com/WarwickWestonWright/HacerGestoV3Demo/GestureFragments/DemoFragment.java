/*
Though the code in the ‘com.WarwickWestonWright.HacerGestoV3’ package is protected by open source, this code (the code in the demo app)
in the ‘com.WarwickWestonWright.HacerGestoV3Demo’ package is free source and can be modified and reused without any obligation to the developer.
*/

package com.WarwickWestonWright.HacerGestoV3Demo.GestureFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.WarwickWestonWright.HacerGestoV3.HGV3Info;
import com.WarwickWestonWright.HacerGestoV3.HacerGestoV3;
import com.WarwickWestonWright.HacerGestoV3Demo.HGFlagFullScreen;
import com.WarwickWestonWright.HacerGestoV3Demo.R;
import com.WarwickWestonWright.HacerGestoV3Demo.SettingsFragments.DemoSettingsFragment;
import com.WarwickWestonWright.HacerGestoV3Demo.SetupBehaviours;

public class DemoFragment extends Fragment {

	public interface IDemoFragment {void demoFragmentCallback();}

	private IDemoFragment iDemoFragment;
	private HacerGestoV3.IHacerGestoV3 iHacerGestoV3;
	private View rootView;
	private HacerGestoV3 hacerGestoV3;
	private DemoSettingsFragment demoSettingsFragment;
	private int useGestures = 0;
	private SharedPreferences sp;
	private HGFlagFullScreen hgFlagFullScreen;

	public DemoFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sp = getActivity().getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.demo_fragment, container, false);

		hgFlagFullScreen = new HGFlagFullScreen(getActivity().getWindow());
		hgFlagFullScreen.flagFullScreen();

		hacerGestoV3 = rootView.findViewById(R.id.hgMainDemoV3);

		if(sp.getBoolean("usePanZoomMode", false) == true) {
			final Bitmap gestureBitpam = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.pan_zoom_wide)).getBitmap();
			hacerGestoV3.setDrawable((new BitmapDrawable(getResources(), gestureBitpam)).getCurrent());
		}
		else if(sp.getBoolean("usePanZoomMode", false) == false) {
			final Bitmap gestureBitpam = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.hg_all_gestures)).getBitmap();
			hacerGestoV3.setDrawable((new BitmapDrawable(getResources(), gestureBitpam)).getCurrent());
		}//End if(sp.getBoolean("usePanZoomMode", false) == true)

		useGestures = 0;
		if(sp.getBoolean("enableRotate", true) == true) {
			useGestures += hacerGestoV3.ROTATE;
		}

		if(sp.getBoolean("enableScale", true) == true) {
			useGestures += hacerGestoV3.SCALE;
		}

		if(sp.getBoolean("enableMove", true) == true) {
			useGestures += hacerGestoV3.MOVE;
		}

		if(sp.getBoolean("flingToMove", true) == true) {
			useGestures += hacerGestoV3.FLING;
		}

		hacerGestoV3.setUseGestures(useGestures);
		hacerGestoV3.doManualScale(0.5f);
		hacerGestoV3.setQuickTapTime(sp.getLong("quickTapTime", 100L));
		hacerGestoV3.setBounceBack(true);

		iHacerGestoV3 = new HacerGestoV3.IHacerGestoV3() {
			@Override
			public void onDown(HGV3Info hgv3Info) {}
			@Override
			public void onPointerDown(HGV3Info hgv3Info) {}
			@Override
			public void onMove(HGV3Info hgv3Info) {}
			@Override
			public void onPointerUp(HGV3Info hgv3Info) {}
			@Override
			public void onUp(HGV3Info hgv3Info) {
				if(hgv3Info.getQuickTap() == true) {
					if(demoSettingsFragment == null) {demoSettingsFragment = new DemoSettingsFragment();}
					if(demoSettingsFragment.isAdded() == false) {
						getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutSettingsFragmentContainer, demoSettingsFragment, "DemoSettingsFragment").commit();
					}
				}//End if(hgv3Info.getQuickTap() == true)
			}
		};

		hacerGestoV3.registerCallback(iHacerGestoV3);

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	@Override
	public void onResume() {
		super.onResume();
		final SetupBehaviours setupBehaviours = new SetupBehaviours();
		setupBehaviours.setupRotate(hacerGestoV3, sp);
		setupBehaviours.setupScale(hacerGestoV3, sp);
		setupBehaviours.setupMove(hacerGestoV3, sp, null, null);
		setupBehaviours.setupFling(hacerGestoV3, sp);
	}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(context instanceof IDemoFragment) {
			iDemoFragment = (IDemoFragment) context;
		}
		else {
			throw new RuntimeException(context.toString() + " must implement IDemoFragment");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		iDemoFragment = null;
	}

}
