/*
Though the code in the ‘com.WarwickWestonWright.HacerGestoV3’ package is protected by open source, this code (the code in the demo app)
in the ‘com.WarwickWestonWright.HacerGestoV3Demo’ package is free source and can be modified and reused without any obligation to the developer.
*/

package com.WarwickWestonWright.HacerGestoV3Demo.SettingsFragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.WarwickWestonWright.HacerGestoV3Demo.CustomToast;
import com.WarwickWestonWright.HacerGestoV3Demo.HGAlert;
import com.WarwickWestonWright.HacerGestoV3Demo.MainActivity;
import com.WarwickWestonWright.HacerGestoV3Demo.R;

public class DemoSettingsFragment extends Fragment implements
	Animation.AnimationListener,
	DialogInterface.OnClickListener {

	public interface IDemoSettingsFragment {void demoSettingsFragmentCallback();}

	private IDemoSettingsFragment iDemoSettingsFragment;
	private View rootView;

	private EditText txtQuickTapTime;
	private CheckBox chkEnableRotate;
	private CheckBox chkEnableScale;
	private CheckBox chkEnableMove;
	private CheckBox chkEnableFling;
	private CheckBox chkEnablePanZoom;
	private CheckBox chkPanZoomWithRotate;
	private CheckBox chkPanZoomWithCenterCrop;
	private CheckBox chkPanZoomWithFlingSlideToCorner;
	private CustomToast customToast;
	private LinearLayout lLayoutPanZoom;
	private Button btnCloseDemoSettings;

	private Animation panZoomFadeIn = null;
	private Animation panZoomFadeOut = null;

	DialogInterface.OnClickListener clickListener;

	private SharedPreferences sp;

	public DemoSettingsFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sp = getActivity().getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE);
		panZoomFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_expand_in);
		panZoomFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_shrink_out);
		customToast = new CustomToast(getActivity());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.demo_settings_fragment, container, false);

		txtQuickTapTime = rootView.findViewById(R.id.txtQuickTapTime);
		chkEnableRotate = rootView.findViewById(R.id.chkEnableRotate);
		chkEnableScale = rootView.findViewById(R.id.chkEnableScale);
		chkEnableMove = rootView.findViewById(R.id.chkEnableMove);
		chkEnableFling = rootView.findViewById(R.id.chkEnableFling);
		chkEnablePanZoom = rootView.findViewById(R.id.chkEnablePanZoom);
		chkPanZoomWithRotate = rootView.findViewById(R.id.chkPanZoomWithRotate);
		chkPanZoomWithCenterCrop = rootView.findViewById(R.id.chkPanZoomWithCenterCrop);
		chkPanZoomWithFlingSlideToCorner = rootView.findViewById(R.id.chkPanZoomWithFlingSlideToCorner);
		lLayoutPanZoom = rootView.findViewById(R.id.lLayoutPanZoom);
		btnCloseDemoSettings = rootView.findViewById(R.id.btnCloseDemoSettings);

		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtQuickTapTime);

		txtQuickTapTime.setText(Long.toString(sp.getLong("quickTapTime", 100L)));
		chkEnableRotate.setChecked(sp.getBoolean("enableRotate", true));
		chkEnableScale.setChecked(sp.getBoolean("enableScale", true));
		chkEnableMove.setChecked(sp.getBoolean("enableMove", true));
		chkEnableFling.setChecked(sp.getBoolean("flingToMove", true));
		chkEnablePanZoom.setChecked(sp.getBoolean("usePanZoomMode", false));
		chkPanZoomWithRotate.setChecked(sp.getBoolean("zoomWithRotation", false));
		chkPanZoomWithCenterCrop.setChecked(sp.getBoolean("zoomCenterCrop", true));
		chkPanZoomWithFlingSlideToCorner.setChecked(sp.getBoolean("flingSlideToCorner", true));

		if(chkEnablePanZoom.isChecked() == true) {
			lLayoutPanZoom.setVisibility(View.VISIBLE);
		}
		else if(chkEnablePanZoom.isChecked() == false) {
			lLayoutPanZoom.setVisibility(View.GONE);
		}

		chkEnableRotate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sp.edit().putBoolean("enableRotate", chkEnableRotate.isChecked()).apply();
			}
		});

		chkEnableScale.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sp.edit().putBoolean("enableScale", chkEnableScale.isChecked()).apply();
			}
		});

		chkEnableMove.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sp.edit().putBoolean("enableMove", chkEnableMove.isChecked()).apply();
			}
		});

		chkEnableFling.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sp.edit().putBoolean("flingToMove", chkEnableFling.isChecked()).apply();
			}
		});

		chkEnablePanZoom.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(chkEnablePanZoom.isChecked() == true) {
					lLayoutPanZoom.setVisibility(View.VISIBLE);
					lLayoutPanZoom.requestLayout();
					lLayoutPanZoom.startAnimation(panZoomFadeIn);
					chkEnableRotate.setChecked(false);
					sp.edit().putBoolean("enableRotate", false).apply();
				}
				else if(chkEnablePanZoom.isChecked() == false) {
					lLayoutPanZoom.startAnimation(panZoomFadeOut);
				}//End if(chkAngleSnap.isChecked() == true)

				sp.edit().putBoolean("usePanZoomMode", chkEnablePanZoom.isChecked()).apply();

			}
		});

		chkPanZoomWithRotate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sp.edit().putBoolean("zoomWithRotation", chkPanZoomWithRotate.isChecked()).apply();
				if(customToast != null) {
					customToast.showCustomToast("ToDo .... ", Toast.LENGTH_SHORT);
				}
			}
		});

		chkPanZoomWithCenterCrop.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sp.edit().putBoolean("zoomCenterCrop", chkPanZoomWithCenterCrop.isChecked()).apply();
			}
		});

		chkPanZoomWithFlingSlideToCorner.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				sp.edit().putBoolean("flingSlideToCorner", chkPanZoomWithCenterCrop.isChecked()).apply();
			}
		});


		btnCloseDemoSettings.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(Long.parseLong(txtQuickTapTime.getText().toString()) > 49L) {
					setPrefsOnExit();
					iDemoSettingsFragment.demoSettingsFragmentCallback();
				}
				else {

					HGAlert hgAlert = new HGAlert();
					hgAlert.setTargetFragment(getFragmentManager().findFragmentByTag("DemoSettingsFragment"), 0);
					hgAlert.show(getActivity().getSupportFragmentManager(), "HGAlert");

				}

			}
		});

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	private void setPrefsOnExit() {
		sp.edit().putLong("quickTapTime", Long.parseLong(txtQuickTapTime.getText().toString())).commit();
		sp.edit().putBoolean("enableRotate", chkEnableRotate.isChecked()).commit();
		sp.edit().putBoolean("enableScale", chkEnableScale.isChecked()).commit();
		sp.edit().putBoolean("enableMove", chkEnableMove.isChecked()).commit();
		sp.edit().putBoolean("flingToMove", chkEnableFling.isChecked()).commit();
		sp.edit().putBoolean("usePanZoomMode", chkEnablePanZoom.isChecked()).commit();
		sp.edit().putBoolean("zoomWithRotation", chkPanZoomWithRotate.isChecked()).commit();
		sp.edit().putBoolean("zoomCenterCrop", chkPanZoomWithCenterCrop.isChecked()).commit();
		sp.edit().putBoolean("flingSlideToCorner", chkPanZoomWithFlingSlideToCorner.isChecked()).commit();
	}//End private void setPrefsOnExit()


	@Override
	public void onClick(DialogInterface dialogInterface, int i) {

		if(i == HGAlert.OK) {
			setPrefsOnExit();
			iDemoSettingsFragment.demoSettingsFragmentCallback();
		}
		else if(i == HGAlert.CANCEL) {
			iDemoSettingsFragment.demoSettingsFragmentCallback();
		}

	}


	@Override
	public void onAnimationStart(Animation animation) {}

	@Override
	public void onAnimationEnd(Animation animation) {

		if(animation == panZoomFadeOut) {

			lLayoutPanZoom.setVisibility(View.GONE);
			lLayoutPanZoom.requestLayout();

		}

	}

	@Override
	public void onAnimationRepeat(Animation animation) {}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(context instanceof IDemoSettingsFragment) {
			iDemoSettingsFragment = (IDemoSettingsFragment) context;
		}
		else {
			throw new RuntimeException(context.toString() + " must implement IDemoSettingsFragment");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		iDemoSettingsFragment = null;
	}

}