/*
Though the code in the ‘com.WarwickWestonWright.HacerGestoV3’ package is protected by open source, this code (the code in the demo app)
in the ‘com.WarwickWestonWright.HacerGestoV3Demo’ package is free source and can be modified and reused without any obligation to the developer.
*/

package com.WarwickWestonWright.HacerGestoV3Demo.GestureFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.WarwickWestonWright.HacerGestoV3.HGGeometry;
import com.WarwickWestonWright.HacerGestoV3.HGV3Info;
import com.WarwickWestonWright.HacerGestoV3.HacerGestoV3;
import com.WarwickWestonWright.HacerGestoV3Demo.HGFlagFullScreen;
import com.WarwickWestonWright.HacerGestoV3Demo.R;
import com.WarwickWestonWright.HacerGestoV3Demo.SettingsFragments.FlingSettingsFragment;
import com.WarwickWestonWright.HacerGestoV3Demo.SetupBehaviours;

public class FlingFragment extends Fragment {

	public interface IFlingFragment {void flingFragmentCallback();}

	private IFlingFragment iFlingFragment;

	private HacerGestoV3.IHacerGestoV3 iHacerGestoV3;
	private View rootView;
	private HacerGestoV3 hacerGestoV3;
	private TextView lblFlingDemoStatus;
	private FlingSettingsFragment flingSettingsFragment;
	private String status;
	private final HGGeometry hgGeometry = new HGGeometry();
	private SharedPreferences sp;
	private HGFlagFullScreen hgFlagFullScreen;

	public FlingFragment() {}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		status = "";
		sp = getActivity().getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fling_fragment, container, false);

		hgFlagFullScreen = new HGFlagFullScreen(getActivity().getWindow());
		hgFlagFullScreen.flagFullScreen();

		hacerGestoV3 = rootView.findViewById(R.id.hgFlingV3);
		lblFlingDemoStatus = rootView.findViewById(R.id.lblFlingDemoStatus);
		hacerGestoV3.setUseGestures(hacerGestoV3.FLING);
		hacerGestoV3.setQuickTapTime(sp.getLong("quickTapTime", 100L));
		hacerGestoV3.setBounceBack(sp.getBoolean("bounceBack", false));
		hacerGestoV3.doManualScale(1f / 3f);

		iHacerGestoV3 = new HacerGestoV3.IHacerGestoV3() {
			@Override
			public void onDown(HGV3Info hgv3Info) {

				//Un-comment the two lines below and block in the onUp event to demonstrate manual fling method
				//hacerGestoV3.setDoManualFling(true);
				//hacerGestoV3.setQuickTapTime(0L);

				final float[] xy = hgv3Info.getCurrentPosition();
				status = "X Pos: " + Float.toString(xy[0]) + "\n" +
					"Y Pos: " + Float.toString(xy[1]) + "\n" +
				"Fling Triggered: " + Boolean.toString(hgv3Info.getFlingTriggered());
				lblFlingDemoStatus.setText(status);

			}

			@Override
			public void onPointerDown(HGV3Info hgv3Info) {}
			@Override
			public void onMove(HGV3Info hgv3Info) {
				final float[] xy = hgv3Info.getCurrentPosition();
				status = "X Pos: " + Float.toString(xy[0]) + "\n" +
					"Y Pos: " + Float.toString(xy[1]) + "\n" +
					"Fling Triggered: " + Boolean.toString(hgv3Info.getFlingTriggered());
				lblFlingDemoStatus.setText(status);
			}
			@Override
			public void onPointerUp(HGV3Info hgv3Info) {}

			@Override
			public void onUp(HGV3Info hgv3Info) {

				final float[] xy = hgv3Info.getCurrentPosition();
				status = "X Pos: " + Float.toString(xy[0]) + "\n" +
					"Y Pos: " + Float.toString(xy[1]) + "\n" +
					"Fling Triggered: " + Boolean.toString(hgv3Info.getFlingTriggered());
				lblFlingDemoStatus.setText(status);

				//Top of Block Demonstrate triggerMoveAnimation method to manually fling to position regardless of fling direction.
				/*
				if(hgv3Info.getFlingTriggered() == true) {
					//Un comment and Execute hacerGestoV3.setDoManualFling(true) in the onDown to make one of the following lines of code to work.
					final Point startPoint = new Point(0, 0);

					final double[] returnPoint = hgGeometry.pythagoreanTheorem(338.0d, 58d, -67);
					//final Point endPoint = new Point(500, 500);//Algorithm 1 set fling to end at static point x500 y 500
					final Point endPoint = new Point((int) returnPoint[0], (int) returnPoint[1]);//Algorithm 2 direction dictated by x58.0d / y-67 ratio and distance of 338.0d
					hacerGestoV3.triggerMoveAnimation(startPoint, endPoint, 2500L);

				}
				*/
				//Bottom of Block Demonstrate triggerMoveAnimation method to manually fling to position regardless of fling direction.

				if(hgv3Info.getQuickTap() == true && hgv3Info.getFlingTriggered() == false) {
					if(flingSettingsFragment == null) {flingSettingsFragment = new FlingSettingsFragment();}
					if(flingSettingsFragment.isAdded() == false) {
						getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutSettingsFragmentContainer, flingSettingsFragment, "FlingSettingsFragment").commit();
					}
				}//End if(hgv3Info.getQuickTap() == true && hgv3Info.getFlingTriggered() == false)

			}
		};

		hacerGestoV3.registerCallback(iHacerGestoV3);

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	@Override
	public void onResume() {
		super.onResume();
		final SetupBehaviours setupBehaviours = new SetupBehaviours();
		setupBehaviours.setupFling(hacerGestoV3, sp);
		hacerGestoV3.setPanZoomMode(true, false, false);
	}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if(context instanceof IFlingFragment) {
			iFlingFragment = (IFlingFragment) context;
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		iFlingFragment = null;
	}
}