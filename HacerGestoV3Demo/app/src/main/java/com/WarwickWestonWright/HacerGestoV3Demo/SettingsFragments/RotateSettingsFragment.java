/*
Though the code in the ‘com.WarwickWestonWright.HacerGestoV3’ package is protected by open source, this code (the code in the demo app)
in the ‘com.WarwickWestonWright.HacerGestoV3Demo’ package is free source and can be modified and reused without any obligation to the developer.
*/

package com.WarwickWestonWright.HacerGestoV3Demo.SettingsFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.WarwickWestonWright.HacerGestoV3Demo.MainActivity;
import com.WarwickWestonWright.HacerGestoV3Demo.R;

public class RotateSettingsFragment extends Fragment implements Animation.AnimationListener {

	public interface IRotateSettingsFragment {void rotateSettingsCallback();}
	private IRotateSettingsFragment iRotateSettingsFragment;
	private View rootView;
	private CheckBox chkPrecision;
	private EditText txtPrecision;
	private LinearLayout lLayoutUseVariableDial;
	private CheckBox chkUseVariableDial;
	private EditText txtInnerPrecision;
	private EditText txtOuterPrecision;
	private CheckBox chkUseVariableCurve;
	private CheckBox chkUsePositiveCurve;
	private CheckBox chkCumulative;
	private CheckBox chkSingleFinger;
	private LinearLayout lLayoutSnappingContainer;
	private CheckBox chkAngleSnap;
	private EditText txtAngleSnap;
	private EditText txtProximity;
	private Button btnCloseRotateSettings;
	private LinearLayout lLayoutMinMaxContainer;
	private CheckBox chkEnableMinMax;
	private EditText txtMinDial;
	private EditText txtMaxDial;
	private LinearLayout lLayoutFlingContainer;
	private CheckBox chkEnableFling;
	private EditText txtFlingDistance;
	private EditText txtFlingTime;
	private EditText txtFlingStartSpeed;
	private EditText txtFlingEndSpeed;
	private EditText txtSpinDuration;
	private EditText txtSlowFactor;
	private CheckBox chkSlowDown;
	private EditText txtFlingAngle;

	private SharedPreferences sp;
	private Animation txtPrecisionFadeIn = null;
	private Animation precisionFadeOut = null;
	private Animation chkUsePositiveCurveFadeIn = null;
	private Animation chkUsePositiveCurveFadeOut = null;
	private Animation snappingContainerFadeIn = null;
	private Animation snappingContainerFadeOut = null;
	private Animation useVariableDialFadeIn = null;
	private Animation useVariableDialFadeOut = null;
	private Animation enableMinMaxFadeIn = null;
	private Animation enableMinMaxFadeout = null;
	private Animation flingContainerFadeIn = null;
	private Animation flingContainerFadeOut = null;
	private Animation quickTapTimeFadeOut = null;

	public RotateSettingsFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		snappingContainerFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_expand_in);
		snappingContainerFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_shrink_out);
		useVariableDialFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_expand_in);
		useVariableDialFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_shrink_out);
		txtPrecisionFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_expand_in);
		precisionFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_shrink_out);
		chkUsePositiveCurveFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_expand_in);
		chkUsePositiveCurveFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_shrink_out);
		enableMinMaxFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_expand_in);
		enableMinMaxFadeout = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_shrink_out);
		flingContainerFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_expand_in);
		flingContainerFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_shrink_out);
		quickTapTimeFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_shrink_out);
		sp = getActivity().getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE);
	}//End public void onCreate(Bundle savedInstanceState)


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.rotate_settings_fragment, container, false);

		txtPrecisionFadeIn.setAnimationListener(this);
		precisionFadeOut.setAnimationListener(this);
		useVariableDialFadeIn.setAnimationListener(this);
		useVariableDialFadeOut.setAnimationListener(this);
		chkUsePositiveCurveFadeOut.setAnimationListener(this);
		snappingContainerFadeOut.setAnimationListener(this);
		flingContainerFadeOut.setAnimationListener(this);
		quickTapTimeFadeOut.setAnimationListener(this);
		enableMinMaxFadeout.setAnimationListener(this);

		lLayoutSnappingContainer = rootView.findViewById(R.id.lLayoutSnappingContainer);
		chkPrecision = rootView.findViewById(R.id.chkPrecision);
		txtPrecision = rootView.findViewById(R.id.txtPrecision);
		lLayoutUseVariableDial = rootView.findViewById(R.id.lLayoutUseVariableDial);
		chkUseVariableDial = rootView.findViewById(R.id.chkUseVariableDial);
		txtInnerPrecision = rootView.findViewById(R.id.txtInnerPrecision);
		txtOuterPrecision = rootView.findViewById(R.id.txtOuterPrecision);
		chkUseVariableCurve = rootView.findViewById(R.id.chkUseVariableCurve);
		chkUsePositiveCurve = rootView.findViewById(R.id.chkUsePositiveCurve);
		chkCumulative = rootView.findViewById(R.id.chkCumulative);
		chkSingleFinger = rootView.findViewById(R.id.chkSingleFinger);
		chkAngleSnap = rootView.findViewById(R.id.chkAngleSnap);
		txtAngleSnap = rootView.findViewById(R.id.txtAngleSnap);
		txtProximity = rootView.findViewById(R.id.txtProximity);
		lLayoutMinMaxContainer = rootView.findViewById(R.id.lLayoutMinMaxContainer);
		chkEnableMinMax = rootView.findViewById(R.id.chkEnableMinMax);
		txtMinDial = rootView.findViewById(R.id.txtMinDial);
		txtMaxDial = rootView.findViewById(R.id.txtMaxDial);
		lLayoutFlingContainer = rootView.findViewById(R.id.lLayoutFlingContainer);
		chkEnableFling = rootView.findViewById(R.id.chkEnableFling);
		txtFlingDistance = rootView.findViewById(R.id.txtFlingDistance);
		txtFlingTime = rootView.findViewById(R.id.txtFlingTime);
		txtFlingStartSpeed = rootView.findViewById(R.id.txtFlingStartSpeed);
		txtFlingEndSpeed = rootView.findViewById(R.id.txtFlingEndSpeed);
		txtSpinDuration = rootView.findViewById(R.id.txtSpinDuration);
		txtSlowFactor = rootView.findViewById(R.id.txtSlowFactor);
		chkSlowDown = rootView.findViewById(R.id.chkSlowDown);
		txtFlingAngle = rootView.findViewById(R.id.txtFlingAngle);
		btnCloseRotateSettings = rootView.findViewById(R.id.btnCloseRotateSettings);

		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtPrecision);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtInnerPrecision);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtOuterPrecision);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtAngleSnap);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtProximity);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtMinDial);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtMaxDial);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtFlingDistance);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtFlingTime);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtFlingStartSpeed);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtFlingEndSpeed);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtSpinDuration);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtSlowFactor);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtFlingAngle);

		chkPrecision.setChecked(sp.getBoolean("usePrecision", true));
		txtPrecision.setText(Float.toString(sp.getFloat("precision", 0.5f)));
		chkUseVariableDial.setChecked(sp.getBoolean("useVariableDial", false));
		txtInnerPrecision.setText(Float.toString(sp.getFloat("innerPrecision", 5.2f)));
		txtOuterPrecision.setText(Float.toString(sp.getFloat("outerPrecision", 0.2f)));
		chkUseVariableCurve.setChecked(sp.getBoolean("useVariableCurve", false));
		chkUsePositiveCurve.setChecked(sp.getBoolean("usePositiveCurve", false));
		chkCumulative.setChecked(sp.getBoolean("cumulativeDial", true));
		chkSingleFinger.setChecked(sp.getBoolean("isSingleFinger", false));
		chkAngleSnap.setChecked(sp.getBoolean("useAngleSnap", false));
		txtAngleSnap.setText(Float.toString(sp.getFloat("angleSnap", 0.125f)));
		txtProximity.setText(Float.toString(sp.getFloat("snapProximity", 0.03125f)));
		chkEnableMinMax.setChecked(sp.getBoolean("useMinMax", false));
		txtMinDial.setText(Float.toString(sp.getFloat("minRotation", -2.45f)));
		txtMaxDial.setText(Float.toString(sp.getFloat("maxRotation", 2.88f)));
		chkEnableFling.setChecked(sp.getBoolean("enableFlingToSpin", false));
		txtFlingDistance.setText(Integer.toString(sp.getInt("flingDistanceToSpin", 150)));
		txtFlingTime.setText(Long.toString(sp.getLong("flingTimeToSpin", 200L)));
		txtFlingStartSpeed.setText(Float.toString(sp.getFloat("spinStartSpeed", 0f)));
		txtFlingEndSpeed.setText(Float.toString(sp.getFloat("spinEndSpeed", 0f)));
		txtSpinDuration.setText(Long.toString(sp.getLong("spinDuration", 8000L)));
		txtSlowFactor.setText(Float.toString(sp.getFloat("slowFactor", 0f)));
		chkSlowDown.setChecked(sp.getBoolean("chkSlowDown", true));
		txtFlingAngle.setText(Float.toString(sp.getFloat("flingAngle", 0)));

		//Top of Block For haptic Feedback
		chkUsePositiveCurve.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//Empty for haptic Feedback
			}
		});
		chkCumulative.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//Empty for haptic Feedback
			}
		});
		chkSingleFinger.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//Empty for haptic Feedback
			}
		});
		//Top of Block For haptic Feedback


		if(chkPrecision.isChecked() == true) {

			txtPrecision.setVisibility(View.VISIBLE);
			txtPrecision.requestLayout();

		}
		else if(chkPrecision.isChecked() == false) {

			txtPrecision.setVisibility(View.GONE);
			txtPrecision.requestLayout();

		}//End if(chkPrecision.isChecked() == true)

		if(chkUseVariableDial.isChecked() == true) {

			lLayoutUseVariableDial.setVisibility(View.VISIBLE);
			lLayoutUseVariableDial.requestLayout();

		}
		else if(chkUseVariableDial.isChecked() == false) {

			lLayoutUseVariableDial.setVisibility(View.GONE);
			lLayoutUseVariableDial.requestLayout();

		}//End if(chkUseVariableDial.isChecked() == true)

		if(chkUseVariableCurve.isChecked() == true) {

			chkUsePositiveCurve.setVisibility(View.VISIBLE);
			chkUsePositiveCurve.requestLayout();

		}
		else if(chkUseVariableCurve.isChecked() == false) {

			chkUsePositiveCurve.setVisibility(View.GONE);
			chkUsePositiveCurve.requestLayout();

		}//End if(chkUseVariableCurve.isChecked() == true)

		if(chkAngleSnap.isChecked() == true) {

			lLayoutSnappingContainer.setVisibility(View.VISIBLE);
			lLayoutSnappingContainer.requestLayout();

		}
		else if(chkAngleSnap.isChecked() == false) {

			lLayoutSnappingContainer.setVisibility(View.GONE);
			lLayoutSnappingContainer.requestLayout();

		}//End if(chkAngleSnap.isChecked() == true)

		if(chkEnableMinMax.isChecked() == true) {

			lLayoutMinMaxContainer.setVisibility(View.VISIBLE);
			lLayoutMinMaxContainer.requestLayout();

		}
		else if(chkEnableMinMax.isChecked() == false) {

			lLayoutMinMaxContainer.setVisibility(View.GONE);
			lLayoutMinMaxContainer.requestLayout();

		}//End if(chkEnableMinMax.isChecked() == true)

		if(chkEnableFling.isChecked() == true) {

			lLayoutFlingContainer.setVisibility(View.VISIBLE);
			lLayoutFlingContainer.requestLayout();

		}
		else if(chkEnableFling.isChecked() == false) {

			lLayoutFlingContainer.setVisibility(View.GONE);
			lLayoutFlingContainer.requestLayout();

		}//End if(chkEnableFling.isChecked() == true)

		chkPrecision.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				sp.edit().putBoolean("usePrecision", chkPrecision.isChecked()).apply();

				if(chkPrecision.isChecked() == true) {

					txtPrecision.setVisibility(View.VISIBLE);
					txtPrecision.startAnimation(txtPrecisionFadeIn);

				}
				else if(chkPrecision.isChecked() == false) {

					txtPrecision.startAnimation(precisionFadeOut);

				}//End if(chkPrecision.isChecked() == true)

			}
		});

		chkUseVariableDial.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				sp.edit().putBoolean("useVariableDial", chkUseVariableDial.isChecked()).apply();

				if(chkUseVariableDial.isChecked() == true) {

					lLayoutUseVariableDial.setVisibility(View.VISIBLE);
					lLayoutUseVariableDial.requestLayout();
					lLayoutUseVariableDial.startAnimation(useVariableDialFadeIn);

				}
				else if(chkUseVariableDial.isChecked() == false) {

					lLayoutUseVariableDial.startAnimation(useVariableDialFadeOut);

				}//End if(chkUseVariableDial.isChecked() == true)

			}
		});

		chkUseVariableCurve.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(chkUseVariableCurve.isChecked() == true) {

					chkUsePositiveCurve.setVisibility(View.VISIBLE);
					chkUsePositiveCurve.startAnimation(chkUsePositiveCurveFadeIn);

				}
				else if(chkUseVariableCurve.isChecked() == false) {

					chkUsePositiveCurve.startAnimation(chkUsePositiveCurveFadeOut);

				}

			}
		});

		chkAngleSnap.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(chkAngleSnap.isChecked() == true) {

					lLayoutSnappingContainer.setVisibility(View.VISIBLE);
					lLayoutSnappingContainer.requestLayout();
					lLayoutSnappingContainer.startAnimation(snappingContainerFadeIn);

				}
				else if(chkAngleSnap.isChecked() == false) {

					lLayoutSnappingContainer.startAnimation(snappingContainerFadeOut);

				}//End if(chkAngleSnap.isChecked() == true)

			}
		});

		chkEnableMinMax.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(chkEnableMinMax.isChecked() == true) {

					lLayoutMinMaxContainer.setVisibility(View.VISIBLE);
					lLayoutMinMaxContainer.requestLayout();
					lLayoutMinMaxContainer.startAnimation(enableMinMaxFadeIn);

				}
				else if(chkEnableMinMax.isChecked() == false) {

					lLayoutMinMaxContainer.startAnimation(enableMinMaxFadeout);

				}//End if(chkEnableMinMax.isChecked() == true)

			}
		});

		chkEnableFling.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(chkEnableFling.isChecked() == true) {

					lLayoutFlingContainer.setVisibility(View.VISIBLE);
					lLayoutFlingContainer.requestLayout();
					lLayoutFlingContainer.startAnimation(flingContainerFadeIn);

				}
				else if(chkEnableFling.isChecked() == false) {

					lLayoutFlingContainer.startAnimation(flingContainerFadeOut);

				}//End if(chkEnableFling.isChecked() == true)

			}
		});

		chkSlowDown.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				sp.edit().putBoolean("chkSlowDown", chkSlowDown.isChecked()).apply();

			}
		});

		btnCloseRotateSettings.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				setPrefsOnExit();
				iRotateSettingsFragment.rotateSettingsCallback();

			}
		});

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	private void setPrefsOnExit() {

		if(txtPrecision != null && txtPrecision.getText().toString().isEmpty() == true) {
			txtPrecision.setText(Float.toString(sp.getFloat("precision", 0.5f)));
		}

		if(txtInnerPrecision != null && txtInnerPrecision.getText().toString().isEmpty() == true) {
			txtInnerPrecision.setText(Float.toString(sp.getFloat("innerPrecision", 5.2f)));
		}

		if(txtOuterPrecision != null && txtOuterPrecision.getText().toString().isEmpty() == true) {
			txtOuterPrecision.setText(Float.toString(sp.getFloat("outerPrecision", 0.2f)));
		}

		if(txtAngleSnap != null && txtAngleSnap.getText().toString().isEmpty() == true) {
			txtAngleSnap.setText(Float.toString(sp.getFloat("angleSnap", 0.125f)));
		}

		if(txtProximity != null && txtProximity.getText().toString().isEmpty() == true) {
			txtProximity.setText(Float.toString(sp.getFloat("snapProximity", 0.03125f)));
		}

		if(txtMinDial != null && txtMinDial.getText().toString().isEmpty() == true) {
			txtMinDial.setText(Float.toString(sp.getFloat("minRotation", -2.45f)));
		}

		if(txtMaxDial != null && txtMaxDial.getText().toString().isEmpty() == true) {
			txtMaxDial.setText(Float.toString(sp.getFloat("maxRotation", 2.88f)));
		}

		if(txtFlingDistance != null && txtFlingDistance.getText().toString().isEmpty() == true) {
			txtFlingDistance.setText(Integer.toString(sp.getInt("flingDistanceToSpin", 150)));
		}

		if(txtFlingTime != null && txtFlingTime.getText().toString().isEmpty() == true) {
			txtFlingTime.setText(Long.toString(sp.getLong("flingTimeToSpin", 200L)));
		}

		if(txtFlingStartSpeed != null && txtFlingStartSpeed.getText().toString().isEmpty() == true) {
			txtFlingStartSpeed.setText(Float.toString(sp.getFloat("spinStartSpeed", 0f)));
		}

		if(txtFlingEndSpeed != null && txtFlingEndSpeed.getText().toString().isEmpty() == true) {
			txtFlingEndSpeed.setText(Float.toString(sp.getFloat("spinEndSpeed", 0f)));
		}

		if(txtSpinDuration != null && txtSpinDuration.getText().toString().isEmpty() == true) {
			txtSpinDuration.setText(Long.toString(sp.getLong("spinDuration", 8000L)));
		}

		if(txtSlowFactor != null && txtSlowFactor.getText().toString().isEmpty() == true) {
			txtSlowFactor.setText(Float.toString(sp.getFloat("slowFactor", 0f)));
		}

		if(txtFlingAngle != null && txtFlingAngle.getText().toString().isEmpty() == true) {
			txtFlingAngle.setText(Float.toString(sp.getFloat("flingAngle", 0f)));
		}

		sp.edit().putBoolean("useVariableCurve", chkUseVariableCurve.isChecked()).commit();
		sp.edit().putBoolean("usePositiveCurve", chkUsePositiveCurve.isChecked()).commit();
		sp.edit().putBoolean("cumulativeDial", chkCumulative.isChecked()).commit();
		sp.edit().putBoolean("isSingleFinger", chkSingleFinger.isChecked()).commit();
		sp.edit().putBoolean("useAngleSnap", chkAngleSnap.isChecked()).commit();
		sp.edit().putBoolean("useMinMax", chkEnableMinMax.isChecked()).commit();
		sp.edit().putBoolean("enableFlingToSpin", chkEnableFling.isChecked()).commit();
		sp.edit().putFloat("precision", Float.parseFloat(txtPrecision.getText().toString())).commit();
		sp.edit().putFloat("innerPrecision", Float.parseFloat(txtInnerPrecision.getText().toString())).commit();
		sp.edit().putFloat("outerPrecision", Float.parseFloat(txtOuterPrecision.getText().toString())).commit();
		sp.edit().putFloat("angleSnap", Float.parseFloat(txtAngleSnap.getText().toString())).commit();
		sp.edit().putFloat("snapProximity", Float.parseFloat(txtProximity.getText().toString())).commit();
		sp.edit().putFloat("minRotation", Float.parseFloat(txtMinDial.getText().toString())).commit();
		sp.edit().putFloat("maxRotation", Float.parseFloat(txtMaxDial.getText().toString())).commit();
		sp.edit().putInt("flingDistanceToSpin", Integer.parseInt(txtFlingDistance.getText().toString())).commit();
		sp.edit().putLong("flingTimeToSpin", Long.parseLong(txtFlingTime.getText().toString())).commit();
		sp.edit().putFloat("spinStartSpeed", Float.parseFloat(txtFlingStartSpeed.getText().toString())).commit();
		sp.edit().putFloat("spinEndSpeed", Float.parseFloat(txtFlingEndSpeed.getText().toString())).commit();
		sp.edit().putLong("spinDuration", Long.parseLong(txtSpinDuration.getText().toString())).commit();
		sp.edit().putFloat("slowFactor", Float.parseFloat(txtSlowFactor.getText().toString())).commit();
		sp.edit().putFloat("flingAngle", Float.parseFloat(txtFlingAngle.getText().toString())).commit();

	}//Emd private void setPrefsOnExit()


	@Override
	public void onAnimationStart(Animation animation) {}

	@Override
	public void onAnimationEnd(Animation animation) {

		if(animation == snappingContainerFadeOut) {

			lLayoutSnappingContainer.setVisibility(View.GONE);
			lLayoutSnappingContainer.requestLayout();

		}
		else if(animation == useVariableDialFadeOut) {

			lLayoutUseVariableDial.setVisibility(View.GONE);
			lLayoutUseVariableDial.requestLayout();

		}
		else if(animation == useVariableDialFadeIn) {

			lLayoutUseVariableDial.setVisibility(View.VISIBLE);
			lLayoutUseVariableDial.requestLayout();
			if(chkPrecision.isChecked() == true) {chkPrecision.performClick();}

		}
		else if(animation == precisionFadeOut) {

			txtPrecision.setVisibility(View.GONE);
			txtPrecision.requestLayout();

		}
		else if(animation == txtPrecisionFadeIn) {

			txtPrecision.setVisibility(View.VISIBLE);
			txtPrecision.requestLayout();
			if(chkUseVariableDial.isChecked() == true) {chkUseVariableDial.performClick();}

		}
		else if(animation == chkUsePositiveCurveFadeOut) {

			chkUsePositiveCurve.setVisibility(View.GONE);
			chkUsePositiveCurve.requestLayout();

		}
		else if(animation == enableMinMaxFadeout) {

			lLayoutMinMaxContainer.setVisibility(View.GONE);
			lLayoutMinMaxContainer.requestLayout();

		}
		else if(animation == flingContainerFadeOut) {

			lLayoutFlingContainer.setVisibility(View.GONE);
			lLayoutFlingContainer.requestLayout();

		}//End if(animation == snappingContainerFadeOut)

	}//End public void onAnimationEnd(Animation animation)

	@Override
	public void onAnimationRepeat(Animation animation) {}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if(context instanceof IRotateSettingsFragment) {
			iRotateSettingsFragment = (IRotateSettingsFragment) context;
		}
		else {
			throw new RuntimeException(context.toString() + " must implement IRotateSettingsFragment");
		}

	}//End public void onAttach(Context context)


	@Override
	public void onDetach() {
		super.onDetach();
		iRotateSettingsFragment = null;

	}

}