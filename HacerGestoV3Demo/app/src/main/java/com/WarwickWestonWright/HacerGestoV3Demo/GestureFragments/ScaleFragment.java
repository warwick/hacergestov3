/*
Though the code in the ‘com.WarwickWestonWright.HacerGestoV3’ package is protected by open source, this code (the code in the demo app)
in the ‘com.WarwickWestonWright.HacerGestoV3Demo’ package is free source and can be modified and reused without any obligation to the developer.
*/

package com.WarwickWestonWright.HacerGestoV3Demo.GestureFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.WarwickWestonWright.HacerGestoV3.HGV3Info;
import com.WarwickWestonWright.HacerGestoV3.HacerGestoV3;
import com.WarwickWestonWright.HacerGestoV3Demo.HGFlagFullScreen;
import com.WarwickWestonWright.HacerGestoV3Demo.R;
import com.WarwickWestonWright.HacerGestoV3Demo.SettingsFragments.ScaleSettingsFragment;
import com.WarwickWestonWright.HacerGestoV3Demo.SetupBehaviours;

public class ScaleFragment extends Fragment {

	public interface IScaleFragment {void scaleFragmentCallback();}
	private IScaleFragment iScaleFragment;
	private HacerGestoV3.IHacerGestoV3 iHacerGestoV3;
	private View rootView;
	private HacerGestoV3 hacerGestoV3;
	private TextView lblScaleDemoStatus;
	private String status;
	private ScaleSettingsFragment scaleSettingsFragment;
	private SharedPreferences sp;
	private HGFlagFullScreen hgFlagFullScreen;

	public ScaleFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		status = "";
		sp = getActivity().getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.scale_fragment, container, false);

		hgFlagFullScreen = new HGFlagFullScreen(getActivity().getWindow());
		hgFlagFullScreen.flagFullScreen();

		hacerGestoV3 = rootView.findViewById(R.id.hgScaleV3);
		hacerGestoV3.setUseGestures(hacerGestoV3.SCALE);
		hacerGestoV3.doManualScale(0.5f);
		lblScaleDemoStatus = rootView.findViewById(R.id.lblScaleDemoStatus);
		hacerGestoV3.setQuickTapTime(sp.getLong("quickTapTime", 100L));

		iHacerGestoV3 = new HacerGestoV3.IHacerGestoV3() {
			@Override
			public void onDown(HGV3Info hgv3Info) {

				status = "Touch Distance: " + Double.toString(hgv3Info.getTouchDistance()) + "\n" +
					"Direction: " + Integer.toString(hgv3Info.getScaleDirection()) + "\n" +
					"Scale: " + Float.toString(hgv3Info.getScale());
				lblScaleDemoStatus.setText(status);

			}

			@Override
			public void onPointerDown(HGV3Info hgv3Info) {

				status = "Touch Distance: " + Double.toString(hgv3Info.getTouchDistance()) + "\n" +
					"Direction: " + Integer.toString(hgv3Info.getScaleDirection()) + "\n" +
					"Scale: " + Float.toString(hgv3Info.getScale());
				lblScaleDemoStatus.setText(status);

			}

			@Override
			public void onMove(HGV3Info hgv3Info) {

				status = "Touch Distance: " + Double.toString(hgv3Info.getTouchDistance()) + "\n" +
					"Direction: " + Integer.toString(hgv3Info.getScaleDirection()) + "\n" +
					"Scale: " + Float.toString(hgv3Info.getScale());
				lblScaleDemoStatus.setText(status);

			}

			@Override
			public void onPointerUp(HGV3Info hgv3Info) {

				status = "Touch Distance: " + Double.toString(hgv3Info.getTouchDistance()) + "\n" +
					"Direction: " + Integer.toString(hgv3Info.getScaleDirection()) + "\n" +
					"Scale: " + Float.toString(hgv3Info.getScale());
				lblScaleDemoStatus.setText(status);

			}

			@Override
			public void onUp(HGV3Info hgv3Info) {

				status = "Touch Distance: " + Double.toString(hgv3Info.getTouchDistance()) + "\n" +
					"Direction: " + Integer.toString(hgv3Info.getScaleDirection()) + "\n" +
					"Scale: " + Float.toString(hgv3Info.getScale());
				lblScaleDemoStatus.setText(status);

				if(hgv3Info.getQuickTap() == true) {

					if(scaleSettingsFragment == null) {scaleSettingsFragment = new ScaleSettingsFragment();}
					if(scaleSettingsFragment.isAdded() == false) {
						getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutSettingsFragmentContainer, scaleSettingsFragment, "ScaleSettingsFragment").commit();
					}//End if(mainSettingsFragment.isAdded() == false)

				}//End if(hgv3Info.getQuickTap() == true)

			}
		};

		hacerGestoV3.registerCallback(iHacerGestoV3);

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	@Override
	public void onResume() {
		super.onResume();
		final SetupBehaviours setupBehaviours = new SetupBehaviours();
		setupBehaviours.setupScale(hacerGestoV3, sp);
		hacerGestoV3.setPanZoomMode(true, false, false);
	}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if(context instanceof IScaleFragment) {
			iScaleFragment = (IScaleFragment) context;
		}
		else {
			throw new RuntimeException(context.toString() + " must implement IScaleFragment");
		}

	}//End public void onAttach(Context context)


	@Override
	public void onDetach() {
		super.onDetach();
		iScaleFragment = null;

	}

}