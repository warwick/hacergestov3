/*
Though the code in the ‘com.WarwickWestonWright.HacerGestoV3’ package is protected by open source, this code (the code in the demo app)
in the ‘com.WarwickWestonWright.HacerGestoV3Demo’ package is free source and can be modified and reused without any obligation to the developer.
*/

package com.WarwickWestonWright.HacerGestoV3Demo;

import android.app.Activity;
import android.content.res.Resources;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class CustomToast {

	//Not currently ued.
	private LayoutInflater inflater;
	private Toast toast;
	private View customToastLayout;
	private TextView LblCustomToast;
	private final Activity context;

	public CustomToast(Activity context) {

		this.context = context;
		if(inflater == null) {inflater = context.getLayoutInflater();}
		if(customToastLayout == null) {customToastLayout = inflater.inflate(R.layout.toast_layout, (ViewGroup) context.findViewById(R.id.toast_layout_root));}
		if(LblCustomToast == null) {LblCustomToast = customToastLayout.findViewById(R.id.LblCustomToast);}
		if(toast == null) {toast = new Toast(context);}

	}

	public void showCustomToast(String message, int messageDuration) {

		LblCustomToast.setText(message);
		toast.setGravity(Gravity.CENTER_VERTICAL| Gravity.BOTTOM, 0, getPxFromDP(160, context.getResources()));
		toast.setDuration(messageDuration);
		toast.setView(customToastLayout);
		if(toast.getView().isShown()) {toast.cancel();}
		toast.show();

	}

	public Activity getContext() {return this.context;}

	private int getPxFromDP(int dimensionDp, Resources resources) {

		float density = resources.getDisplayMetrics().density;
		return (int) (dimensionDp * density + 0.5f);

	}

}