/*
Though the code in the ‘com.WarwickWestonWright.HacerGestoV3’ package is protected by open source, this code (the code in the demo app)
in the ‘com.WarwickWestonWright.HacerGestoV3Demo’ package is free source and can be modified and reused without any obligation to the developer.
*/

package com.WarwickWestonWright.HacerGestoV3Demo.GestureFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.WarwickWestonWright.HacerGestoV3.HGV3Info;
import com.WarwickWestonWright.HacerGestoV3.HacerGestoV3;
import com.WarwickWestonWright.HacerGestoV3Demo.HGFlagFullScreen;
import com.WarwickWestonWright.HacerGestoV3Demo.R;
import com.WarwickWestonWright.HacerGestoV3Demo.SettingsFragments.MoveSettingsFragment;
import com.WarwickWestonWright.HacerGestoV3Demo.SetupBehaviours;

public class MoveFragment extends Fragment {

	public interface IMoveFragment {void moveFragmentCallback();}
	private IMoveFragment iMoveFragment;
	private HacerGestoV3.IHacerGestoV3 iHacerGestoV3;
	private View rootView;
	private HacerGestoV3 hacerGestoV3;
	private TextView lblMoveDemoStatus;
	private MoveSettingsFragment moveSettingsFragment;
	private String status;
	private final Point[] sides = new Point[4];
	private final int[] tolerances = new int[4];
	private SharedPreferences sp;
	private HGFlagFullScreen hgFlagFullScreen;

	public MoveFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		status = "";
		sp = getActivity().getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.move_fragment, container, false);

		hgFlagFullScreen = new HGFlagFullScreen(getActivity().getWindow());
		hgFlagFullScreen.flagFullScreen();

		hacerGestoV3 = rootView.findViewById(R.id.hgMoveV3);
		hacerGestoV3.setUseGestures(hacerGestoV3.MOVE);
		hacerGestoV3.doManualScale(0.5f);
		lblMoveDemoStatus = rootView.findViewById(R.id.lblMoveDemoStatus);
		hacerGestoV3.setQuickTapTime(sp.getLong("quickTapTime", 100L));

		iHacerGestoV3 = new HacerGestoV3.IHacerGestoV3() {
			@Override
			public void onDown(HGV3Info hgv3Info) {

				final float[] xy = hgv3Info.getCurrentPosition();
				status = "X Pos: " + Float.toString(xy[0]) + "\n" +
					"Y Pos: " + Float.toString(xy[1]) + "\n" +
					"Direction: " + Integer.toString(hgv3Info.getMoveDirection()) ;
				lblMoveDemoStatus.setText(status);

			}

			@Override
			public void onPointerDown(HGV3Info hgv3Info) {}

			@Override
			public void onMove(HGV3Info hgv3Info) {

				final float[] xy = hgv3Info.getCurrentPosition();
				status = "X Pos: " + Float.toString(xy[0]) + "\n" +
					"Y Pos: " + Float.toString(xy[1]) + "\n" +
					"Direction: " + Integer.toString(hgv3Info.getMoveDirection()) ;
				lblMoveDemoStatus.setText(status);

				/* Uncomment code to demonstrate the move direction flags. Hint combine conditions to detect diagonal movement
				if((hgv3Info.getMoveDirection() & hacerGestoV3.getMoveConstraintObj().movingL) == hacerGestoV3.getMoveConstraintObj().movingL) {
					//You are moving left
				}

				if((hgv3Info.getMoveDirection() & hacerGestoV3.getMoveConstraintObj().movingU) == hacerGestoV3.getMoveConstraintObj().movingU) {
					//You are moving up
				}

				if((hgv3Info.getMoveDirection() & hacerGestoV3.getMoveConstraintObj().movingR) == hacerGestoV3.getMoveConstraintObj().movingR) {
					//You are moving right
				}

				if((hgv3Info.getMoveDirection() & hacerGestoV3.getMoveConstraintObj().movingD) == hacerGestoV3.getMoveConstraintObj().movingD) {
					//You are moving down
				}
				*/

			}

			@Override
			public void onPointerUp(HGV3Info hgv3Info) {}

			@Override
			public void onUp(HGV3Info hgv3Info) {

				final float[] xy = hgv3Info.getCurrentPosition();
				status = "X Pos: " + Float.toString(xy[0]) + "\n" +
					"Y Pos: " + Float.toString(xy[1]) + "\n" +
					"Direction: " + Integer.toString(hgv3Info.getMoveDirection()) ;
				lblMoveDemoStatus.setText(status);

				if(hgv3Info.getQuickTap() == true) {
					if(moveSettingsFragment == null) {moveSettingsFragment = new MoveSettingsFragment();}
					if(moveSettingsFragment.isAdded() == false) {
						getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutSettingsFragmentContainer, moveSettingsFragment, "MoveSettingsFragment").commit();
					}
				}//End if(hgv3Info.getQuickTap() == true)

			}
		};

		hacerGestoV3.registerCallback(iHacerGestoV3);

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	@Override
	public void onResume() {
		super.onResume();
		rootView.post(new Runnable() {
			@Override
			public void run() {
				setSides();
			}
		});
	}


	private void setSides() {
		final SetupBehaviours setupBehaviours = new SetupBehaviours();
		final int viewWidth = rootView.getWidth();
		final int viewHeight = rootView.getHeight();
		int distanceFromEdge = 0;

		if(this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
			distanceFromEdge = (int) (viewWidth * 0.25f);
		}
		else if(this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			distanceFromEdge = (int) (viewHeight * 0.25f);
		}//End if(this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)

		//Have as many snap points as you want.
		sides[0] = new Point(-(viewWidth / 2) + distanceFromEdge, 0);//Left
		sides[1] = new Point(0, -(viewHeight / 2) + distanceFromEdge);//Top
		sides[2] = new Point((viewWidth / 2) - distanceFromEdge, 0);//Right
		sides[3] = new Point(0, (viewHeight / 2) - distanceFromEdge);//Bottom

		//Tolerances can be what ever you want
		tolerances[0] = 50;
		tolerances[1] = 50;
		tolerances[2] = 50;
		tolerances[3] = 50;
		setupBehaviours.setupMove(hacerGestoV3, sp, sides, tolerances);
		hacerGestoV3.setPanZoomMode(true, false, false);
	}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if(context instanceof IMoveFragment) {
			iMoveFragment = (IMoveFragment) context;
		}
		else {
			throw new RuntimeException(context.toString() + " must implement IMoveFragment");
		}

	}


	@Override
	public void onDetach() {
		super.onDetach();
		iMoveFragment = null;
	}

}