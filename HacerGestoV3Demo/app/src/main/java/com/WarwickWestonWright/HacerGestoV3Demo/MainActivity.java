/*
Though the code in the ‘com.WarwickWestonWright.HacerGestoV3’ package is protected by open source, this code (the code in the demo app)
in the ‘com.WarwickWestonWright.HacerGestoV3Demo’ package is free source and can be modified and reused without any obligation to the developer.
*/

package com.WarwickWestonWright.HacerGestoV3Demo;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Point;
import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.WarwickWestonWright.HacerGestoV3.HGV3Info;
import com.WarwickWestonWright.HacerGestoV3.HacerGestoV3;
import com.WarwickWestonWright.HacerGestoV3Demo.GestureFragments.DemoFragment;
import com.WarwickWestonWright.HacerGestoV3Demo.GestureFragments.FlingFragment;
import com.WarwickWestonWright.HacerGestoV3Demo.GestureFragments.MoveFragment;
import com.WarwickWestonWright.HacerGestoV3Demo.GestureFragments.RotateFragment;
import com.WarwickWestonWright.HacerGestoV3Demo.GestureFragments.ScaleFragment;
import com.WarwickWestonWright.HacerGestoV3Demo.SettingsFragments.DemoSettingsFragment;
import com.WarwickWestonWright.HacerGestoV3Demo.SettingsFragments.FlingSettingsFragment;
import com.WarwickWestonWright.HacerGestoV3Demo.SettingsFragments.MoveSettingsFragment;
import com.WarwickWestonWright.HacerGestoV3Demo.SettingsFragments.RotateSettingsFragment;
import com.WarwickWestonWright.HacerGestoV3Demo.SettingsFragments.ScaleSettingsFragment;
import com.gargy.floatingkeyboardview.FloatingKeyboardView;

public class MainActivity extends AppCompatActivity implements
	RotateFragment.IRotateFragment,
	ScaleFragment.IScaleFragment,
	MoveFragment.IMoveFragment,
	FlingFragment.IFlingFragment,
	DemoFragment.IDemoFragment,
	RotateSettingsFragment.IRotateSettingsFragment,
	ScaleSettingsFragment.IScaleSettingsFragment,
	MoveSettingsFragment.IMoveSettingsFragment,
	FlingSettingsFragment.IFlingSettingsFragment,
	DemoSettingsFragment.IDemoSettingsFragment,
	HacerGestoV3.IHacerGestoV3 {

	private SharedPreferences sp;
	private float menuItemWidth;//Used for scaling corner images
	private int imageDimension;

	//Widget/Layout/View Declarations
	private ImageView imgULFling;
	private ImageView imgURMove;
	private ImageView imgLLScale;
	private ImageView imgLRRotate;
	private RelativeLayout rLayoutRootView;
	private HacerGestoV3 hgMoveMenu;

	//Fragment Declarations
	private RotateFragment rotateFragment;
	private ScaleFragment scaleFragment;
	private MoveFragment moveFragment;
	private FlingFragment flingFragment;
	private DemoFragment demoFragment;
	private RotateSettingsFragment rotateSettingsFragment;
	private ScaleSettingsFragment scaleSettingsFragment;
	private MoveSettingsFragment moveSettingsFragment;
	private FlingSettingsFragment flingSettingsFragment;
	private DemoSettingsFragment demoSettingsFragment;

	private final Point[] corners = new Point[5];
	private final int[] snapTolerances = new int[5];

	public FloatingKeyboardView floatingKeyboardView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		final HGFlagFullScreen hgFlagFullScreen = new HGFlagFullScreen(getWindow());
		hgFlagFullScreen.flagFullScreen();
		setContentView(R.layout.main_activity);
		floatingKeyboardView = findViewById(R.id.keyboardview);
		floatingKeyboardView.setKeyboard(new Keyboard(this, R.xml.keyboard_number));
		floatingKeyboardView.setPreviewEnabled(false);
		floatingKeyboardView.setAllignBottomCenter(true);

		rLayoutRootView = findViewById(R.id.rLayoutRootView);
		sp = getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE);

		if(sp.getBoolean("arePrefsSetup", false) == false) {

			//Rotate Vars
			sp.edit().putBoolean("usePrecision", false).apply();
			sp.edit().putFloat("precision", 0.5f).apply();
			sp.edit().putBoolean("useVariableDial", false).apply();
			sp.edit().putFloat("innerPrecision", 5.2f).apply();
			sp.edit().putFloat("outerPrecision", 0.2f).apply();
			sp.edit().putBoolean("useVariableCurve", false).apply();
			sp.edit().putBoolean("usePositiveCurve", false).apply();
			sp.edit().putBoolean("cumulativeDial", true).apply();
			sp.edit().putBoolean("isSingleFinger", false).apply();
			sp.edit().putBoolean("useAngleSnap", false).apply();
			sp.edit().putFloat("angleSnap", 0.125f).apply();
			sp.edit().putFloat("snapProximity", 0.03125f).apply();
			sp.edit().putBoolean("useMinMax", false).apply();
			sp.edit().putFloat("minRotation", -2.45f).apply();
			sp.edit().putFloat("maxRotation", 2.88f).apply();
			sp.edit().putBoolean("enableFlingToSpin", false).apply();
			sp.edit().putInt("flingDistanceToSpin", 150).apply();
			sp.edit().putLong("flingTimeToSpin", 200).apply();
			sp.edit().putFloat("spinStartSpeed", 0f).apply();
			sp.edit().putFloat("spinEndSpeed", 0f).apply();
			sp.edit().putLong("spinDuration", 8000).apply();
			sp.edit().putFloat("slowFactor", 0f).apply();
			sp.edit().putBoolean("chkSlowDown", true).apply();
			sp.edit().putFloat("flingAngle", 0f).apply();

			//Scale Vars
			sp.edit().putBoolean("useMinMaxScale", false).apply();
			sp.edit().putFloat("minScale", 0.2f).apply();
			sp.edit().putFloat("maxScale", 1.5f).apply();
			sp.edit().putInt("scaleSnapping", 0).apply();

			//Move Vars
			sp.edit().putBoolean("cumulativeMove", true).apply();
			sp.edit().putBoolean("moveSnapping", false).apply();
			setMoveConstraintsPrefs();
			sp.edit().putBoolean("offEdge", false).apply();
			sp.edit().putBoolean("toEdge", true).apply();
			sp.edit().putBoolean("useMoveConstraint", false).apply();
			sp.edit().putBoolean("zoomCenterCrop", true).apply();
			sp.edit().putBoolean("zoomWithRotation", false).apply();

			//Fling Vars
			sp.edit().putInt("flingToMoveDistance", 150).apply();
			sp.edit().putLong("flingToMoveTime", 250).apply();
			sp.edit().putLong("flingToMoveAnimation", 650).apply();
			sp.edit().putBoolean("dynamicAnimation", false).apply();
			sp.edit().putBoolean("flingOffEdge", true).apply();
			sp.edit().putBoolean("flingToCorner", false).apply();
			sp.edit().putBoolean("flingToEdge", false).apply();
			sp.edit().putBoolean("bounceBack", false).apply();
			sp.edit().putBoolean("flingSlideToCorner", true).apply();

			//Common Properties
			sp.edit().putLong("quickTapTime", 100L).apply();
			sp.edit().putBoolean("enableRotate", true).apply();
			sp.edit().putBoolean("enableScale", true).apply();
			sp.edit().putBoolean("enableMove", true).apply();
			sp.edit().putBoolean("flingToMove", true).apply();
			sp.edit().putBoolean("usePanZoomMode", false).apply();

			sp.edit().putBoolean("arePrefsSetup", true).apply();

		}//End if(sp.getBoolean("arePrefsSetup", false) == false)

		imgULFling = findViewById(R.id.imgULFling);
		imgURMove = findViewById(R.id.imgURMove);
		imgLLScale = findViewById(R.id.imgLLScale);
		imgLRRotate = findViewById(R.id.imgLRRotate);
		hgMoveMenu = findViewById(R.id.hgMoveMenu);
		hgMoveMenu.registerCallback(this);
		hgMoveMenu.setUseGestures(hgMoveMenu.MOVE | hgMoveMenu.FLING);
		hgMoveMenu.setMoveFlingTolerance(100, 250L, 650L);
		hgMoveMenu.setMoveFlingBehaviours(false, true, false);
		hgMoveMenu.setQuickTapTime(sp.getLong("quickTapTime", 100L));

		imgULFling.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(flingFragment == null) {flingFragment = new FlingFragment();}
				getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, flingFragment, "FlingFragment").commit();
			}
		});

		imgURMove.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(moveFragment == null) {moveFragment = new MoveFragment();}
				getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, moveFragment, "MoveFragment").commit();
			}
		});

		imgLLScale.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(scaleFragment == null) {scaleFragment = new ScaleFragment();}
				getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, scaleFragment, "ScaleFragment").commit();
			}
		});

		imgLRRotate.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(rotateFragment == null) {rotateFragment = new RotateFragment();}
				getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, rotateFragment, "RotateFragment").commit();
			}
		});

	}//End protected void onCreate(Bundle savedInstanceState)


	private void setCorners() {

		//Dynamically size the corner images
		if(this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
			imageDimension = (int) ((float) rLayoutRootView.getWidth() * menuItemWidth);
		}
		else if(this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			imageDimension = (int) ((float) rLayoutRootView.getHeight() * menuItemWidth);
		}//End if(this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)

		final RelativeLayout.LayoutParams ulParams = (RelativeLayout.LayoutParams) imgULFling.getLayoutParams();
		ulParams.width = imageDimension;
		ulParams.height = imageDimension;
		imgULFling.setLayoutParams(ulParams);

		final RelativeLayout.LayoutParams urParams = (RelativeLayout.LayoutParams) imgURMove.getLayoutParams();
		urParams.width = imageDimension;
		urParams.height = imageDimension;
		imgURMove.setLayoutParams(urParams);

		final RelativeLayout.LayoutParams llParams = (RelativeLayout.LayoutParams) imgLLScale.getLayoutParams();
		llParams.width = imageDimension;
		llParams.height = imageDimension;
		imgLLScale.setLayoutParams(llParams);

		final RelativeLayout.LayoutParams lrParams = (RelativeLayout.LayoutParams) imgLRRotate.getLayoutParams();
		lrParams.width = imageDimension;
		llParams.height = imageDimension;
		imgLRRotate.setLayoutParams(lrParams);

		//Set Corner positions for move snapping When hgMoveMenu object snaps to corner by dragging or flinging the menu will be selected.
		final int imageDimensionDiv2 = imageDimension / 2;
		final int clientWidthDiv2 = rLayoutRootView.getWidth() / 2;
		final int clientHeightDiv2 = rLayoutRootView.getHeight() / 2;
		//Increment or decrement the snapTolerances to examine behaviour each snap point can be different.
		snapTolerances[0] = 50;
		snapTolerances[1] = 50;
		snapTolerances[2] = 50;
		snapTolerances[3] = 50;
		snapTolerances[4] = 50;
		corners[0] = new Point(-clientWidthDiv2 + imageDimensionDiv2, -clientHeightDiv2 + imageDimensionDiv2);//Upper Left
		corners[1] = new Point(clientWidthDiv2 - imageDimensionDiv2, -clientHeightDiv2 + imageDimensionDiv2);//Upper Right
		corners[2] = new Point(-clientWidthDiv2 + imageDimensionDiv2, clientHeightDiv2 - imageDimensionDiv2);//Lower Left
		corners[3] = new Point(clientWidthDiv2 - imageDimensionDiv2, clientHeightDiv2 - imageDimensionDiv2);//Lower Right
		corners[4] = new Point(0, 0);//Centre
		hgMoveMenu.setMoveSnapping(corners, snapTolerances);

	}//End private void setCorners()


	private void setMoveConstraintsPrefs() {

		rLayoutRootView.post(new Runnable() {
			@Override
			public void run() {

				final int rLayoutXOver2 = rLayoutRootView.getWidth() / 2;
				final int rLayoutYOver2 = rLayoutRootView.getHeight() / 2;
				sp.edit().putInt("l", -rLayoutXOver2).apply();
				sp.edit().putInt("t", -rLayoutYOver2).apply();
				sp.edit().putInt("r", rLayoutXOver2).apply();
				sp.edit().putInt("b", rLayoutYOver2).apply();

			}
		});

	}//End private void setMoveConstraintsPrefs()

	@Override
	public void rotateFragmentCallback() {
		//ToDo
	}

	@Override
	public void scaleFragmentCallback() {
		//ToDo
	}

	@Override
	public void moveFragmentCallback() {
		//ToDo
	}

	@Override
	public void flingFragmentCallback() {
		//ToDo
	}

	@Override
	public void demoFragmentCallback() {
		//ToDo
	}

	@Override
	public void onResume() {
		super.onResume();

		if(getResources().getDisplayMetrics().widthPixels > getResources().getDisplayMetrics().heightPixels) {
			menuItemWidth = (getResources().getDisplayMetrics().heightPixels / 3 / (float) getResources().getDisplayMetrics().heightPixels);
		}
		else if(getResources().getDisplayMetrics().widthPixels <= getResources().getDisplayMetrics().heightPixels) {
			menuItemWidth = (getResources().getDisplayMetrics().widthPixels / 3) / (float) getResources().getDisplayMetrics().widthPixels;
		}//End if(Activity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)

		hgMoveMenu.doManualScale(menuItemWidth);

		rLayoutRootView.post(new Runnable() {
			@Override
			public void run() {
				setCorners();
			}
		});
	}

	@Override
	public void rotateSettingsCallback() {
		rotateSettingsFragment = (RotateSettingsFragment) getSupportFragmentManager().findFragmentByTag("RotateSettingsFragment");
		if(rotateSettingsFragment != null && rotateSettingsFragment.isAdded() == true) {
			getSupportFragmentManager().beginTransaction().remove(rotateSettingsFragment).commit();
			rotateFragment = new RotateFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, rotateFragment, "RotateFragment").commit();
		}
	}

	@Override
	public void scaleSettingsCallback() {
		scaleSettingsFragment = (ScaleSettingsFragment) getSupportFragmentManager().findFragmentByTag("ScaleSettingsFragment");
		if(scaleSettingsFragment != null && scaleSettingsFragment.isAdded() == true) {
			getSupportFragmentManager().beginTransaction().remove(scaleSettingsFragment).commit();
			scaleFragment = new ScaleFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, scaleFragment, "ScaleFragment").commit();
		}
	}

	@Override
	public void moveSettingsCallback() {
		moveSettingsFragment = (MoveSettingsFragment) getSupportFragmentManager().findFragmentByTag("MoveSettingsFragment");
		if(moveSettingsFragment != null && moveSettingsFragment.isAdded() == true) {
			getSupportFragmentManager().beginTransaction().remove(moveSettingsFragment).commit();
			moveFragment = new MoveFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, moveFragment, "MoveFragment").commit();
		}
	}

	@Override
	public void flingSettingsCallback() {
		flingSettingsFragment = (FlingSettingsFragment) getSupportFragmentManager().findFragmentByTag("FlingSettingsFragment");
		if(flingSettingsFragment != null && flingSettingsFragment.isAdded() == true) {
			getSupportFragmentManager().beginTransaction().remove(flingSettingsFragment).commit();
			flingFragment = new FlingFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, flingFragment, "FlingFragment").commit();
		}
	}

	@Override
	public void demoSettingsFragmentCallback() {
		demoSettingsFragment = (DemoSettingsFragment) getSupportFragmentManager().findFragmentByTag("DemoSettingsFragment");
		if(demoSettingsFragment != null && demoSettingsFragment.isAdded() == true) {
			getSupportFragmentManager().beginTransaction().remove(demoSettingsFragment).commit();
			demoFragment = new DemoFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, demoFragment, "DemoFragment").commit();
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		hgMoveMenu.unRegisterCallback();

	}


	@Override
	public void onBackPressed() {

		rotateFragment = (RotateFragment) getSupportFragmentManager().findFragmentByTag("RotateFragment");
		scaleFragment = (ScaleFragment) getSupportFragmentManager().findFragmentByTag("ScaleFragment");
		moveFragment = (MoveFragment) getSupportFragmentManager().findFragmentByTag("MoveFragment");
		flingFragment = (FlingFragment) getSupportFragmentManager().findFragmentByTag("FlingFragment");
		demoFragment = (DemoFragment) getSupportFragmentManager().findFragmentByTag("DemoFragment");
		rotateSettingsFragment = (RotateSettingsFragment) getSupportFragmentManager().findFragmentByTag("RotateSettingsFragment");
		scaleSettingsFragment = (ScaleSettingsFragment) getSupportFragmentManager().findFragmentByTag("ScaleSettingsFragment");
		moveSettingsFragment = (MoveSettingsFragment) getSupportFragmentManager().findFragmentByTag("MoveSettingsFragment");
		flingSettingsFragment = (FlingSettingsFragment) getSupportFragmentManager().findFragmentByTag("FlingSettingsFragment");
		demoSettingsFragment = (DemoSettingsFragment) getSupportFragmentManager().findFragmentByTag("DemoSettingsFragment");

		if(floatingKeyboardView != null && floatingKeyboardView.isShown() == true) {
			floatingKeyboardView.hide();
		}
		else if(rotateFragment != null && rotateFragment.isAdded() == true) {
			getSupportFragmentManager().beginTransaction().remove(rotateFragment).commit();
			restart();
		}
		else if(scaleFragment != null && scaleFragment.isAdded() == true) {
			getSupportFragmentManager().beginTransaction().remove(scaleFragment).commit();
			restart();
		}
		else if(moveFragment != null && moveFragment.isAdded() == true) {
			getSupportFragmentManager().beginTransaction().remove(moveFragment).commit();
			restart();
		}
		else if(flingFragment != null && flingFragment.isAdded() == true) {
			getSupportFragmentManager().beginTransaction().remove(flingFragment).commit();
			restart();
		}
		else if(demoFragment != null && demoFragment.isAdded() == true) {
			getSupportFragmentManager().beginTransaction().remove(demoFragment).commit();
			restart();
		}
		else if(rotateSettingsFragment != null && rotateSettingsFragment.isAdded() == true) {
			rotateFragment = new RotateFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, rotateFragment, "RotateFragment").commit();
		}
		else if(scaleSettingsFragment != null && scaleSettingsFragment.isAdded() == true) {
			scaleFragment = new ScaleFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, scaleFragment, "ScaleFragment").commit();
		}
		else if(moveSettingsFragment != null && moveSettingsFragment.isAdded() == true) {
			moveFragment = new MoveFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, moveFragment, "MoveFragment").commit();
		}
		else if(flingSettingsFragment != null && flingSettingsFragment.isAdded() == true) {
			flingFragment = new FlingFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, flingFragment, "FlingFragment").commit();
		}
		else if(demoSettingsFragment != null && demoSettingsFragment.isAdded() == true) {
			demoFragment = new DemoFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, demoFragment, "DemoFragment").commit();
		}
		else {
			super.onBackPressed();
		}

	}//End public void onBackPressed()

	private void restart() {
		finish();
		overridePendingTransition(0, 0);
		startActivity(getIntent());
		overridePendingTransition(0, 0);
	}

	@Override
	public void onDown(HGV3Info hgv3Info) {}
	@Override
	public void onPointerDown(HGV3Info hgv3Info) {}
	@Override
	public void onMove(HGV3Info hgv3Info) {
		if(hgv3Info.getMoveSnapped() == true) {hgMoveMenu.bringToFront();}
	}
	@Override
	public void onPointerUp(HGV3Info hgv3Info) {}

	@Override
	public void onUp(HGV3Info hgv3Info) {

		if(hgv3Info.getMoveSnapped() == true) {
			final int moveSnapIdx = hgv3Info.getMoveSnappedIdx();

			switch(moveSnapIdx) {
				case 0:
					if(flingFragment == null) {flingFragment = new FlingFragment();}
					getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, flingFragment, "FlingFragment").commit();
					break;

				case 1:
					if(moveFragment == null) {moveFragment = new MoveFragment();}
					getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, moveFragment, "MoveFragment").commit();
					break;

				case 2:
					if(scaleFragment == null) {scaleFragment = new ScaleFragment();}
					getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, scaleFragment, "ScaleFragment").commit();
					break;

				case 3:
					if(rotateFragment == null) {rotateFragment = new RotateFragment();}
					getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, rotateFragment, "RotateFragment").commit();
					break;

				case 4:
					if(demoFragment == null) {demoFragment = new DemoFragment();}
					getSupportFragmentManager().beginTransaction().replace(R.id.lLayoutDemoFragmentContainer, demoFragment, "DemoFragment").commit();
					break;

			}

		}

	}

}