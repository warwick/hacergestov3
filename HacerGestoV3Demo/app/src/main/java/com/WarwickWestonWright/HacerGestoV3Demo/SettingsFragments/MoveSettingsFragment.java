/*
Though the code in the ‘com.WarwickWestonWright.HacerGestoV3’ package is protected by open source, this code (the code in the demo app)
in the ‘com.WarwickWestonWright.HacerGestoV3Demo’ package is free source and can be modified and reused without any obligation to the developer.
*/

package com.WarwickWestonWright.HacerGestoV3Demo.SettingsFragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.WarwickWestonWright.HacerGestoV3Demo.MainActivity;
import com.WarwickWestonWright.HacerGestoV3Demo.R;

public class MoveSettingsFragment extends Fragment implements Animation.AnimationListener {

	public interface IMoveSettingsFragment {void moveSettingsCallback();}

	private IMoveSettingsFragment iMoveSettingsFragment;
	private View rootView;
	private CheckBox chkCumulativeMove;
	private CheckBox chkMoveSnapping;
	private EditText txtTop;
	private EditText txtLeft;
	private EditText txtRight;
	private EditText txtBottom;
	private CheckBox chkUseMoveConstraint;
	private CheckBox chkOffEdge;
	private CheckBox chkToEdge;
	private LinearLayout lLayoutConstraintContainer;

	private Animation moveConstraintFadeIn;
	private Animation moveConstraintFadeOut;

	private Button btnCloseSettings;
	private SharedPreferences sp;
	public MoveSettingsFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sp = getActivity().getSharedPreferences(getString(R.string.shared_pref_filename), Context.MODE_PRIVATE);
		moveConstraintFadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_expand_in);
		moveConstraintFadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_shrink_out);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.move_settings_fragment, container, false);

		moveConstraintFadeIn.setAnimationListener(this);
		moveConstraintFadeOut.setAnimationListener(this);

		chkCumulativeMove = rootView.findViewById(R.id.chkCumulativeMove);
		chkMoveSnapping = rootView.findViewById(R.id.chkMoveSnapping);
		txtTop = rootView.findViewById(R.id.txtTop);
		txtLeft = rootView.findViewById(R.id.txtLeft);
		txtRight = rootView.findViewById(R.id.txtRight);
		txtBottom = rootView.findViewById(R.id.txtBottom);
		chkUseMoveConstraint = rootView.findViewById(R.id.chkUseMoveConstraint);
		chkOffEdge = rootView.findViewById(R.id.chkOffEdge);
		chkToEdge = rootView.findViewById(R.id.chkToEdge);
		lLayoutConstraintContainer = rootView.findViewById(R.id.lLayoutConstraintContainer);

		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtTop);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtLeft);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtRight);
		((MainActivity) getActivity()).floatingKeyboardView.registerEditText(txtBottom);

		chkCumulativeMove.setChecked(sp.getBoolean("cumulativeMove", true));
		chkMoveSnapping.setChecked(sp.getBoolean("moveSnapping", false));
		chkUseMoveConstraint.setChecked(sp.getBoolean("useMoveConstraint", false));
		chkOffEdge.setChecked(sp.getBoolean("offEdge", false));
		chkToEdge.setChecked(sp.getBoolean("toEdge", true));

		if(chkUseMoveConstraint.isChecked() == true) {

			lLayoutConstraintContainer.setVisibility(View.VISIBLE);
			lLayoutConstraintContainer.requestLayout();

		}
		else if(chkUseMoveConstraint.isChecked() == false) {

			lLayoutConstraintContainer.setVisibility(View.GONE);
			lLayoutConstraintContainer.requestLayout();

		}//End if(chkUseMoveConstraint.isChecked() == true)

		txtLeft.setText(Integer.toString(sp.getInt("l", 0)));
		txtTop.setText(Integer.toString(sp.getInt("t", 0)));
		txtRight.setText(Integer.toString(sp.getInt("r", 0)));
		txtBottom.setText(Integer.toString(sp.getInt("b", 0)));
		btnCloseSettings = rootView.findViewById(R.id.btnCloseMoveSettings);

		chkCumulativeMove.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//Empty for haptic Feedback
			}
		});

		chkMoveSnapping.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//Empty for haptic Feedback
			}
		});

		chkUseMoveConstraint.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if(chkUseMoveConstraint.isChecked() == true) {

					lLayoutConstraintContainer.setVisibility(View.VISIBLE);
					lLayoutConstraintContainer.requestLayout();
					lLayoutConstraintContainer.startAnimation(moveConstraintFadeIn);

				}
				else if(chkUseMoveConstraint.isChecked() == false) {

					lLayoutConstraintContainer.startAnimation(moveConstraintFadeOut);

				}//End if(chkUseMoveConstraint.isChecked() == true)

			}
		});

		chkOffEdge.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//Empty for haptic Feedback
			}
		});

		chkToEdge.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//Empty for haptic Feedback
			}
		});

		btnCloseSettings.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setPrefsOnExit();
				iMoveSettingsFragment.moveSettingsCallback();
			}
		});

		return rootView;

	}//End public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)


	private void setPrefsOnExit() {

		sp.edit().putBoolean("cumulativeMove", chkCumulativeMove.isChecked()).commit();
		sp.edit().putBoolean("moveSnapping", chkMoveSnapping.isChecked()).commit();
		sp.edit().putInt("l", Integer.parseInt(txtLeft.getText().toString())).commit();
		sp.edit().putInt("t", Integer.parseInt(txtTop.getText().toString())).commit();
		sp.edit().putInt("r", Integer.parseInt(txtRight.getText().toString())).commit();
		sp.edit().putInt("b", Integer.parseInt(txtBottom.getText().toString())).commit();
		sp.edit().putBoolean("useMoveConstraint", chkUseMoveConstraint.isChecked()).commit();
		sp.edit().putBoolean("offEdge", chkOffEdge.isChecked()).commit();
		sp.edit().putBoolean("toEdge", chkToEdge.isChecked()).commit();

	}//End private void setPrefsOnExit()


	@Override
	public void onAnimationStart(Animation animation) {}
	@Override
	public void onAnimationEnd(Animation animation) {

		if(animation == moveConstraintFadeIn) {

			lLayoutConstraintContainer.setVisibility(View.VISIBLE);
			lLayoutConstraintContainer.requestLayout();

		}
		else if(animation == moveConstraintFadeOut) {

			lLayoutConstraintContainer.setVisibility(View.GONE);
			lLayoutConstraintContainer.requestLayout();

		}

	}
	@Override
	public void onAnimationRepeat(Animation animation) {}


	@Override
	public void onAttach(Context context) {
		super.onAttach(context);

		if(context instanceof IMoveSettingsFragment) {
			iMoveSettingsFragment = (IMoveSettingsFragment) context;
		}
		else {
			throw new RuntimeException(context.toString() + " must implement IMoveSettingsFragment");
		}

	}//End public void onAttach(Context context)


	@Override
	public void onDetach() {
		super.onDetach();
		iMoveSettingsFragment = null;
	}

}