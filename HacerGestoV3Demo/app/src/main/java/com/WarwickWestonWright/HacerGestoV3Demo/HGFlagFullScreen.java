package com.WarwickWestonWright.HacerGestoV3Demo;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class HGFlagFullScreen {

	private final Window window;

	public HGFlagFullScreen(final Window window) {
		this.window = window;
	}

	public void flagFullScreen() {

		if(android.os.Build.VERSION.SDK_INT >= 19) {
			final int uiOptions;
			uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
			window.getDecorView().setSystemUiVisibility(uiOptions);
		}
		else if(android.os.Build.VERSION.SDK_INT >= 16) {
			final int uiOptions;
			uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
			window.getDecorView().setSystemUiVisibility(uiOptions);

		}
		else  {
			window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}

	}

}